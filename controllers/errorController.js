exports.get403Forbidden = (req, res, next) => {    
    try {        
        return res.render("error/403_forbidden", {            
            pageTitle: "403",
            path: "/403-forbidden"
        });
    } catch (err) {
        throw err;
    }
};