const MyCompany = require("../models/myCompany");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

nullCompanyInput = {
    title: {
        short: null,
        full: null,
    },
    address: {
        city: null,
        street: null,
        house: null,
        office: null,
    },
    contacts: {
        phone: null,
        email: null,
        webSite: null,
    },
    apiKeys: {
        telegram: null,
        mailgun: null,
        smsRu: null,
    },
};

exports.getIndex = async (req, res, next) => {
    try {
        const myCompanies = await MyCompany.find();
        return res.render("myCompany/index", {
            myCompanies: myCompanies,
            pageTitle: `Организации | ${res.locals.userCompany.title.short}`,
            path: "/my-companies",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("myCompany/add", {
        pageTitle: `Добавить Организацию | ${res.locals.userCompany.title.short}`,
        path: "my-companies/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: nullCompanyInput,
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("myCompany/add", {
            pageTitle: `Добавить Организацию | ${res.locals.userCompany.title.short}`,
            path: "my-companies/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: {
                    short: req.body.shortTitle,
                    full: req.body.fullTitle,
                },
                address: {
                    city: req.body.city,
                    street: req.body.street,
                    house: req.body.house,
                    office: req.body.office,
                },
                contacts: {
                    phone: req.body.phone,
                    email: req.body.email,
                    webSite: req.body.webSite,
                },
                apiKeys: {
                    telegram: req.body.telegramApi,
                    mailgun: req.body.mailgunApi,
                    smsRu: req.body.smsRuApi,
                },
            },
        });
    }

    try {
        const myCompany = new MyCompany({
            title: {
                short: req.body.shortTitle,
                full: req.body.fullTitle,
            },
            address: {
                city: req.body.city,
                street: req.body.street,
                house: req.body.house,
                office: req.body.office,
            },
            contacts: {
                phone: req.body.phone,
                email: req.body.email,
                webSite: req.body.webSite,
            },
            apiKeys: {
                telegram: req.body.telegramApi,
                mailgun: req.body.mailgunApi,
                smsRu: req.body.smsRuApi,
            },
        });

        await myCompany.save();
        return res.redirect("/my-companies");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const companyId = req.params.companyId;
    try {
        const myCompany = await MyCompany.findById(companyId);
        if (!myCompany) {
            req.flash("error", "Организация не найдена.");
            return res.redirect("/my-companies");
        }
        return res.render("myCompany/edit", {
            pageTitle: `Изменить Организацию | ${res.locals.userCompany.title.short}`,
            path: "my-companies/edit",
            myCompany: myCompany,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: nullCompanyInput
        });
    } catch (err) {
        throw err;
    }
};

exports.getEditMyCompany = async (req, res, next) => {
    try {
        const myCompany = await MyCompany.findById("5f3369790c87e91076c71672");
        if (!myCompany) {
            req.flash("error", "Организация не найдена.");
            return res.redirect("/my-companies");
        }
        return res.render("myCompany/edit", {
            pageTitle: `Изменить Организацию | ${res.locals.userCompany.title.short}`,
            path: "my-companies/edit",
            myCompany: myCompany,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: nullCompanyInput
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const myCompany = await MyCompany.findById(req.body.companyId);
            if (!myCompany) {
                req.flash("error", "Организация не найдена.");
                return res.redirect("/my-companies");
            }
            return res.status(422).render("myCompany/edit", {
                pageTitle: `Изменить Организацию | ${res.locals.userCompany.title.short}`,
                path: "my-companies/edit",
                myCompany: myCompany,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: {
                        short: req.body.shortTitle,
                        full: req.body.fullTitle,
                    },
                    address: {
                        city: req.body.city,
                        street: req.body.street,
                        house: req.body.house,
                        office: req.body.office,
                    },
                    contacts: {
                        phone: req.body.phone,
                        email: req.body.email,
                        webSite: req.body.webSite,
                        telegramId: req.body.telegramId,
                    },
                    apiKeys: {
                        telegram: req.body.telegramApi,
                        mailgun: req.body.mailgunApi,
                        smsRu: req.body.smsRuApi,
                    },
                },
            });
        } catch (err) {
            throw err;
        }
    }

    const myCompanyId = req.body.companyId;

    try {
        const myCompany = await MyCompany.findById(myCompanyId);
        myCompany.title.short = req.body.shortTitle;
        myCompany.title.full = req.body.fullTitle;
        myCompany.address.city = req.body.city;
        myCompany.address.street = req.body.street;
        myCompany.address.house = req.body.house;
        myCompany.address.office = req.body.office;
        myCompany.contacts.phone = req.body.phone;
        myCompany.contacts.email = req.body.email;
        myCompany.contacts.webSite = req.body.webSite;
        myCompany.contacts.telegramId = req.body.telegramId;
        myCompany.apiKeys.telegram = req.body.telegramApi;
        myCompany.apiKeys.mailgun = req.body.mailgunApi;
        myCompany.apiKeys.smsRu = req.body.smsRuApi;
        
        await myCompany.save();
        return res.redirect("/my-companies");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const errors = validationResult(req);
    const companyId = req.body.companyId;
    try {
        if (!errors.isEmpty()) {
            return res.status(422).render("auth/login", {
                path: "/my-companies/delete",
                pageTitle: `Удалить Организацию | ${res.locals.userCompany.title.short}`,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
            });
        }
        await MyCompany.deleteOne({
            _id: companyId,
        });
        return res.redirect("/my-companies");
    } catch (err) {
        throw err;
    }
};
