exports.getIndex = async (req, res, next) => {
    try {
        return res.render("dashboard/index", {
            pageTitle: `Dashboard | ${res.locals.userCompany.title.short}`,
            path: "/dashboard",           
        });
    } catch (err) {
        throw err;
    }
};