const Customer = require("../models/customer");
const Order = require("../models/order");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const customers = await Customer.aggregate([
            {
                $project: {
                    fullName: {
                        $concat: [
                            "$name.last",
                            " ",
                            "$name.first",
                            " ",
                            "$name.middle",
                        ],
                    },
                    address: {
                        $concat: [
                            "$address.city",
                            ", ",
                            "$address.street",
                            ", ",
                            "$address.house",
                        ],
                    },
                    "contacts.phone": 1,
                    "contacts.email": 1,
                    customerType: 1,
                    silentMode: 1,
                },
            },
        ]);
        return res.render("customer/index", {
            customers: customers,
            pageTitle: `Все Физические Лица | ${res.locals.userCompany.title.short}`,
            path: "/customers",
        });
    } catch (err) {
        throw err;
    }
};

exports.getViewCustomer = async (req, res, next) => {
    try {
        const customer = await Customer.findById(req.params.customerId);
        const orders = await Order.find({
            $or: [
                { "device.owner.individual._id": customer._id },
                { "customer.individual._id": customer._id },
            ],
            $and: [
                { state: { $ne: "Устройство выдано" } },
                { state: { $ne: "Выдан акт неремонтопригодности" } },
            ],
        });
        return res.render("customer/view", {
            customer: customer,
            orders: orders,
            pageTitle: `${customer.name.last} | ${res.locals.userCompany.title.short}`,
            path: "/customers/view",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAddCustomer = (req, res, next) => {
    return res.render("customer/add", {
        pageTitle: `Добавить Физическое Лицо | ${res.locals.userCompany.title.short}`,
        path: "customers/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            name: {
                first: null,
                middle: null,
                last: null,
            },
            address: {
                city: null,
                street: null,
                house: null,
            },
            contacts: {
                phone: null,
                email: null,
            },
        },
    });
};

exports.postAddCustomer = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("customer/add", {
            pageTitle: `Добавить Физическое Лицо | ${res.locals.userCompany.title.short}`,
            path: "customers/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: {
                    first: req.body.firstName,
                    middle: req.body.middleName,
                    last: req.body.lastName,
                },
                address: {
                    city: req.body.city,
                    street: req.body.street,
                    house: req.body.house,
                },
                contacts: {
                    phone: req.body.phone,
                    email: req.body.email,
                },
            },
        });
    }

    const silentMode = req.body.silentMode ? true : false;
    try {
        const customer = new Customer({
            name: {
                first: req.body.firstName,
                middle: req.body.middleName,
                last: req.body.lastName,
            },
            address: {
                city: req.body.city,
                street: req.body.street,
                house: req.body.house,
            },
            contacts: {
                phone: req.body.emptyPhone
                    ? "номер отсутствует"
                    : req.body.phone,
                email: req.body.email,
            },
            silentMode: silentMode,
        });
        await customer.save();
        req.body.continueSelector !== "yes"
            ? res.redirect(`/customers/view/${customer._id}`)
            : res.redirect(`/devices/add?customer=${customer._id}`);
    } catch (err) {
        throw err;
    }
};

exports.getEditCustomer = async (req, res, next) => {
    const customerId = req.params.customerId;
    try {
        const customer = await Customer.findById(customerId);
        if (!customer) {
            return res.redirect("/customers");
        }
        return res.render("customer/edit", {
            pageTitle: `Изменить Физическое Лицо | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            customer: customer,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: {
                    first: null,
                    middle: null,
                    last: null,
                },
                address: {
                    city: null,
                    street: null,
                    house: null,
                },
                contacts: {
                    phone: null,
                    email: null,
                },
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEditCustomer = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("customer/edit", {
            pageTitle: `Изменить Физическое Лицо | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            customer: customer,
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: {
                    first: req.body.firstName,
                    middle: req.body.middleName,
                    last: req.body.lastName,
                },
                address: {
                    city: req.body.city,
                    street: req.body.street,
                    house: req.body.house,
                },
                contacts: {
                    phone: req.body.phone,
                    email: req.body.email,
                },
            },
        });
    }

    const silentMode = req.body.silentMode ? true : false;
    try {
        const customer = await Customer.findById(req.body.customerId);
        customer.name.first = req.body.firstName;
        customer.name.middle = req.body.middleName;
        customer.name.last = req.body.lastName;

        customer.address.city = req.body.city;
        customer.address.street = req.body.street;
        customer.address.house = req.body.house;

        (customer.contacts.phone = req.body.emptyPhone
            ? "номер отсутствует"
            : req.body.phone),
            (customer.contacts.email = req.body.email);

        customer.silentMode = silentMode;
        await customer.save();
        return res.redirect("/customers");
    } catch (err) {
        throw err;
    }
};

exports.postDeleteCustomer = async (req, res, next) => {
    const customerId = req.body.customerId;
    try {
        await Customer.deleteOne({
            _id: customerId,
        });
        return res.redirect("/customers");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoCompleteCustomer = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const customersFilter = Customer.find(
        {
            $or: [
                {
                    "name.first": regex,
                },
                {
                    "name.middle": regex,
                },
                {
                    "name.last": regex,
                },
            ],
        },
        {
            "name.first": 1,
            "name.middle": 1,
            "name.last": 1,
            "contacts.phone": 1,
        }
    ).limit(10);
    customersFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((customer) => {
                    let obj = {
                        id: customer._id,
                        label: `${customer.name.last} ${customer.name.first} ${customer.name.middle} (${customer.contacts.phone})`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
