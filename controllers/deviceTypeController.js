const DeviceType = require("../models/deviceType");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const deviceTypes = await DeviceType.find();
        return res.render("deviceType/index", {
            deviceTypes: deviceTypes,
            pageTitle: `Все Типы Устройств | ${res.locals.userCompany.title.short}`,
            path: "/device-types",
        });
    } catch (err) {
        throw err;
    }
};

exports.getView = async (req, res, next) => {
    const deviceTypeId = req.params.deviceTypeId;
    try {
        const deviceType = await DeviceType.findById(deviceTypeId);
        return res.render("deviceType/view", {
            deviceType: deviceType,
            pageTitle: deviceType.title,
            path: "/device-types/view",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("deviceType/add", {
        pageTitle: `Добавить тип устройства | ${res.locals.userCompany.title.short}`,
        path: "device-types/add",
        errorMessage:  alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            name: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    const name = req.body.name;
    const isOfficiallyAuthorized = req.body.isOfficiallyAuthorized;

    const deviceType = new DeviceType({
        name: name,
        isOfficiallyAuthorized: isOfficiallyAuthorized,
    });

    if (!errors.isEmpty()) {
        try {
            return res.status(422).render("deviceType/add", {
                path: "deviceType/add",
                pageTitle: `Добавить тип устройства | ${res.locals.userCompany.title.short}`,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    name: req.body.name,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        await deviceType.save();
        return res.redirect("/device-types");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const deviceTypeId = req.params.deviceTypeId;
    try {
        const deviceType = await DeviceType.findById(deviceTypeId);
        if (!deviceType) {
            return res.redirect("/deviceTypes");
        }
        return res.render("deviceType/edit", {
            pageTitle: `Изменить Тип Устройства | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            deviceType: deviceType,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    const deviceTypeId = req.body.deviceTypeId;
    const name = req.body.name;
    // const isOfficiallyAuthorized = req.body.isOfficiallyAuthorized;

    if (!errors.isEmpty()) {
        try {
            return res.status(422).render("deviceType/edit", {
                path: "deviceType/edit",
                pageTitle: `Изменить Тип Устройства | ${res.locals.userCompany.title.short}`,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    name: req.body.name,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const deviceType = await DeviceType.findById(deviceTypeId);
        deviceType.name = name;
        // deviceType.isOfficiallyAuthorized = isOfficiallyAuthorized;
        await deviceType.save();
        return res.redirect("/device-types");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const deviceTypeId = req.body.deviceTypeId;
    try {
        await DeviceType.deleteOne({ _id: deviceTypeId });
        return res.redirect("/device-types");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
/* exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const deviceTypesFilter = DeviceType.find({ name: regex }, { name: 1 })
        //    .sort({"updated_at": -1})
        //    .sort({created_at: -1})
        .limit(10);
    deviceTypesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((deviceType) => {
                    let obj = {
                        label: `${deviceType.name}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
}; */
