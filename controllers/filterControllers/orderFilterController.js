const Order = require("../../models/order");
const User = require("../../models/user");
const Customer = require("../../models/customer");
const legalEntity = require("../../models/legalEntity");
const Device = require("../../models/device");
const OrderType = require("../../models/orderType");
const DeviceType = require("../../models/deviceType");
const Manufacturer = require("../../models/manufacturer");
const OrderFilter = require("../../models/orderFilter");

const moment = require("moment");
const { validationResult } = require("express-validator");
const { timePassedVerbose } = require("../../middleware/timePassedCalc");

exports.getIndex = async (req, res, next) => {
    try {
        const orderFilters = await OrderFilter.find({
            createdBy: req.session.user._id,
        });
        return res.render("filters/orderFilter/index", {
            orderFilters: orderFilters,
            pageTitle: `Мои фильтры ремонтов | ${res.locals.userCompany.title.short}`,
            path: "/my-order-filters",
        });
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    // flash messages, validation etc.
    let errorMessage = req.flash("error");
    let successMessage = req.flash("success");
    let infoMessage = req.flash("info");
    errorMessage.length > 0
        ? (errorMessage = errorMessage[0])
        : (errorMessage = null);
    successMessage.length > 0
        ? (successMessage = successMessage[0])
        : (successMessage = null);
    infoMessage.length > 0
        ? (infoMessage = infoMessage[0])
        : (infoMessage = null);

    const orderFilterId = req.params.orderFilterId;
    try {
        const orderFilter = await OrderFilter.findById(orderFilterId);
        if (!orderFilter) {
            return res.redirect("/my-order-filters");
        }
        return res.render("filters/orderFilter/edit", {
            pageTitle: `Изменить фильтр ремонтов | ${res.locals.userCompany.title.short}`,
            orderFilter: orderFilter,
            errorMessage: errorMessage,
            successMessage: successMessage,
            infoMessage: infoMessage,
            oldInput: {
                title: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    // flash messages, validation etc.
    let errorMessage = req.flash("error");
    let successMessage = req.flash("success");
    let infoMessage = req.flash("info");
    errorMessage.length > 0
        ? (errorMessage = errorMessage[0])
        : (errorMessage = null);
    successMessage.length > 0
        ? (successMessage = successMessage[0])
        : (successMessage = null);
    infoMessage.length > 0
        ? (infoMessage = infoMessage[0])
        : (infoMessage = null);

    const errors = validationResult(req);

    const orderFilterId = req.body.orderFilterId;
    const title = req.body.title;
    const isShared = req.body.isShared ? true : false;
    const setByDefault = req.body.setByDefault ? true : false;

    if (!errors.isEmpty()) {
        try {
            return res.status(422).render("filters/orderFilter/edit", {
                path: "my-order-filters/edit",
                pageTitle: `Изменить фильтр ремонта | ${res.locals.userCompany.title.short}`,
                errorMessage: errors.array()[0].msg,
                successMessage: successMessage,
                infoMessage: infoMessage,
                oldInput: {
                    title: req.body.title,
                    isShared: isShared,
                    setByDefault: req.body.setByDefault,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const orderFilter = await OrderFilter.findById(orderFilterId);
        orderFilter.title = title;
        orderFilter.isShared = isShared;
        orderFilter.setByDefault = setByDefault;
        await orderFilter.save();
        return res.redirect("/my-order-filters");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const orderFilterId = req.body.orderFilterId;
    try {
        await OrderFilter.deleteOne({ _id: orderFilterId });
        return res.redirect("/my-order-filters");
    } catch (err) {
        throw err;
    }
};

exports.postFilteredContent = async (req, res, next) => {
    try {
        const individualOwner = req.body.individualOwnerId
            ? await Customer.findOne({
                  _id: req.body.individualOwnerId,
              })
            : "";
        const legalEntityOwner = req.body.legalEntityOwnerId
            ? await legalEntity.findOne({
                  _id: req.body.legalEntityOwnerId,
              })
            : "";
        const individualCustomer = req.body.individualCustomerId
            ? await Customer.findOne({
                  _id: req.body.individualCustomerId,
              })
            : "";
        const legalEntityCustomer = req.body.legalEntityCustomerId
            ? await legalEntity.findOne({
                  _id: req.body.legalEntityCustomerId,
              })
            : "";

        const individualOwnerData = !individualOwner
            ? { $exists: true }
            : {
                  individual: {
                      _id: individualOwner._id,
                      firstName: individualOwner.name.first,
                      lastName: individualOwner.name.last,
                      middleName: individualOwner.name.middle,
                      phone: individualOwner.contacts.phone,
                      email: individualOwner.contacts.email,
                  },
                  legalEntity: {},
              };

        const legalEntityOwnerData = !legalEntityOwner
            ? { $exists: true }
            : {
                  individual: {},
                  legalEntity: {
                      _id: legalEntityOwner._id,
                      shortName: legalEntityOwner.shortName,
                      fullName: legalEntityOwner.fullName,
                      phone: legalEntityOwner.phone,
                      email: legalEntityOwner.email,
                  },
              };

        const individualCustomerData = !individualCustomer
            ? { $exists: true }
            : {
                  individual: {
                      _id: individualCustomer._id,
                      firstName: individualCustomer.name.first,
                      lastName: individualCustomer.name.last,
                      middleName: individualCustomer.name.middle,
                      phone: individualCustomer.contacts.phone,
                      email: individualCustomer.contacts.email,
                      address: {
                          city: individualCustomer.address.city,
                          street: individualCustomer.address.street,
                          house: individualCustomer.address.house,
                      },
                  },
              };

        const legalEntityCustomerData = !legalEntityCustomer
            ? { $exists: true }
            : {
                  legalEntity: {
                      _id: legalEntityCustomer._id,
                      shortName: legalEntityCustomer.shortName,
                      fullName: legalEntityCustomer.fullName,
                      phone: legalEntityCustomer.phone,
                      email: legalEntityCustomer.email,
                      isDealer: legalEntityCustomer.isDealer,
                      address: {
                          city: legalEntityCustomer.city,
                          street: legalEntityCustomer.street,
                          house: legalEntityCustomer.house,
                      },
                  },
              };

        const orderTypes = await OrderType.find();
        const users = await User.find({ role: { $ne: "User" } });
        const manufacturers = await Manufacturer.find();
        const deviceTypes = await DeviceType.find();
        const orderStates = await Order.schema.path("state").enumValues;

        const deviceStates = await Device.schema.path("state").enumValues;

        // function for correct variables search in mongodb
        const mongoVariable = (key) => {
            return key
                ? Array.isArray(key)
                    ? { $in: key }
                    : { $in: [key] }
                : { $exists: true };
        };

        const mongoUserVariable = async (keys) => {
            let idsArray = [];
            if (Array.isArray(keys)) {
                let splittedArray = keys.map((key) => {
                    return key.split(" ");
                });
                for (el of splittedArray) {
                    const user = await User.findOne({
                        $and: [{ "name.last": el[1] }, { "name.first": el[0] }],
                    });
                    idsArray.push(user._id);
                }
                return idsArray;
            } else if (keys) {
                const userData = keys.split(" ");
                const user = await User.findOne({
                    $and: [
                        {
                            "name.last": userData[1],
                        },
                        { "name.first": userData[0] },
                    ],
                });
                idsArray.push(user._id);
                return idsArray;
            } else {
                return { $exists: true };
            }
        };

        const mongoDateRange = (key) => {
            return key[0] ? { $gte: key[0], $lte: key[1] } : { $exists: true };
        };

        // search parameters
        const orderState = mongoVariable(req.body.orderState);
        const orderType = mongoVariable(req.body.orderType);
        const providerOrderId = mongoVariable(req.body.providerOrderId);
        const orderNo = mongoVariable(req.body.orderNo);
        const deviceType = mongoVariable(req.body.deviceType);
        const deviceManufacturer = mongoVariable(req.body.deviceManufacturer);
        const deviceModel = mongoVariable(req.body.deviceModel);
        const deviceItemCode = mongoVariable(req.body.deviceItemCode);
        const deviceSN = mongoVariable(req.body.deviceSN);
        const deviceIMEI = mongoVariable(req.body.deviceIMEI);
        const deviceState = mongoVariable(req.body.deviceState);
        const deviceOSVersion = mongoVariable(req.body.deviceOSVersion);

        const createdAtStart = req.body.orderCreatedAtStart
            ? moment(req.body.orderCreatedAtStart, "DD-MM-YYYY").format()
            : "";
        const createdAtEnd = req.body.orderCreatedAtEnd
            ? moment(req.body.orderCreatedAtEnd, "DD-MM-YYYY").format()
            : "";

        const diagnosedAtStart = req.body.orderDiagnosedAtStart
            ? moment(req.body.orderDiagnosedAtStart, "DD-MM-YYYY").format()
            : "";
        const diagnosedAtEnd = req.body.orderDiagnosedAtEnd
            ? moment(req.body.orderDiagnosedAtEnd, "DD-MM-YYYY").format()
            : "";

        const completedAtStart = req.body.orderCompletedAtStart
            ? moment(req.body.orderCompletedAtStart, "DD-MM-YYYY").format()
            : "";
        const completedAtEnd = req.body.orderCompletedAtEnd
            ? moment(req.body.orderCompletedAtEnd, "DD-MM-YYYY").format()
            : "";

        const issuedAtStart = req.body.orderIssuedAtStart
            ? moment(req.body.orderIssuedAtStart, "DD-MM-YYYY").format()
            : "";
        const issuedAtEnd = req.body.orderIssuedAtEnd
            ? moment(req.body.orderIssuedAtEnd, "DD-MM-YYYY").format()
            : "";

        const createdAt = mongoDateRange([createdAtStart, createdAtEnd]);

        const diagnosedAt = mongoDateRange([diagnosedAtStart, diagnosedAtEnd]);

        const completedAt = mongoDateRange([completedAtStart, completedAtEnd]);

        const issuedAt = mongoDateRange([issuedAtStart, issuedAtEnd]);

        const createdBy = await mongoUserVariable(req.body.orderCreatedBy);
        const diagnosedBy = await mongoUserVariable(req.body.orderDiagnosedBy);
        const completedBy = await mongoUserVariable(req.body.orderCompletedBy);
        const issuedBy = await mongoUserVariable(req.body.orderIssuedBy);

        // filtered orders
        const orders = await Order.find({
            $and: [
                { state: orderState },
                { "type.title": orderType },
                { providerOrderId: providerOrderId },
                { orderNo: orderNo },
                { createdAt: createdAt },
                { "createdBy.id": createdBy },
                { diagnosedAt: diagnosedAt },
                { diagnosedBy: diagnosedBy },
                { completedAt: completedAt },
                { completedBy: completedBy },
                { issuedAt: issuedAt },
                { issuedBy: issuedBy },
                { "device.type": deviceType },
                { "device.manufacturer": deviceManufacturer },
                { "device.model": deviceModel },
                { "device.itemCode": deviceItemCode },
                { "device.sn": deviceSN },
                {
                    $or: [
                        { "device.imei1": deviceIMEI },
                        { "device.imei2": deviceIMEI },
                    ],
                },
                { "device.osVersion": deviceOSVersion },
                { "device.state": deviceState },
                { "device.owner": individualOwnerData },
                { "device.owner": legalEntityOwnerData },
                { customer: individualCustomerData },
                { customer: legalEntityCustomerData },
            ],
        });

        if (req.body.saveSelector === "yes") {
            const filter = new OrderFilter({
                title: req.body.filterTitle,
                isShared: req.body.isShared === "on" ? true : false,
                setByDefault: req.body.setByDefault === "on" ? true : false,
                createdBy: req.session.user,
                parameters: {
                    orderState: req.body.orderState,
                    orderType: req.body.orderType,
                    providerOrderId: req.body.providerOrderId,
                    orderNo: req.body.orderNo,
                    orderLegalEntityCustomer: req.body.legalEntityCustomerId,
                    orderIndividualCustomer: req.body.individualCustomerId,
                    orderCreatedAt: {
                        full: req.body.orderCreatedAt,
                        start: req.body.orderCreatedAtStart,
                        end: req.body.orderCreatedAtEnd,
                    },
                    orderDiagnosedAt: {
                        full: req.body.orderDiagnosedAt,
                        start: req.body.orderDiagnosedAtStart,
                        end: req.body.orderDiagnosedAtEnd,
                    },
                    orderCompletedAt: {
                        full: req.body.orderCompletedAt,
                        start: req.body.orderCompletedAtStart,
                        end: req.body.orderCompletedAtEnd,
                    },
                    orderIssuedAt: {
                        full: req.body.orderIssuedAt,
                        start: req.body.orderIssuedAtStart,
                        end: req.body.orderIssuedAtEnd,
                    },
                    orderDiagnosedBy: req.body.orderDiagnosedBy,
                    orderCompletedBy: req.body.orderCompletedBy,
                    orderIssuedBy: req.body.orderIssuedBy,
                    deviceType: req.body.deviceType,
                    deviceManufacturer: req.body.deviceManufacturer,
                    deviceModel: req.body.deviceModel,
                    deviceItemCode: req.body.deviceItemCode,
                    deviceSN: req.body.deviceSN,
                    deviceIMEI: req.body.deviceIMEI,
                    deviceOSVersion: req.body.deviceOSVersion,
                    deviceState: req.body.deviceState,
                    deviceLegalEntityOwner: req.body.legalEntityOwnerId,
                    deviceIndividualOwner: req.body.individualOwnerId,
                },
            });
            await filter.save();
            return res.redirect(`/orders/filtered/${filter._id}`);
            /* if (filter.isShared) {
                for (user of users) {
                    await user.filters.push(filter._id);
                    await user.save();
                }
            } else {
                const user = await User.findById(req.session.user._id);                
                user.filters.push(filter._id);
                await user.save();
            } */
        }

        const orderFilters = await OrderFilter.find({
            $or: [{ isShared: true }, { createdBy: req.session.user._id }],
        });

        return res.render("order/index", {
            pageTitle: `Ремонты | ${res.locals.userCompany.title.short}`,
            path: "/orders",
            orderFilters: orderFilters,
            orderFilterId: "",
            orderState: req.body.orderState || [],
            orderType: req.body.orderType || [],
            orderNo: req.body.orderNo,
            providerOrderId: req.body.providerOrderId,
            individualCustomer: individualCustomer,
            legalEntityCustomer: legalEntityCustomer,
            individualCustomerId: req.body.individualCustomerId,
            legalEntityCustomerId: req.body.legalEntityCustomerId,
            deviceType: req.body.deviceType || [],
            deviceManufacturer: req.body.deviceManufacturer || [],
            deviceModel: req.body.deviceModel,
            deviceItemCode: req.body.deviceItemCode,
            deviceSN: req.body.deviceSN,
            deviceIMEI: req.body.deviceIMEI,
            deviceState: req.body.deviceState || [],
            orderCreatedAt: req.body.orderCreatedAt,
            orderCreatedAtStart: req.body.orderCreatedAtStart,
            orderCreatedAtEnd: req.body.orderCreatedAtEnd,
            orderDiagnosedAt: req.body.orderDiagnosedAt,
            orderDiagnosedAtStart: req.body.orderDiagnosedAtStart,
            orderDiagnosedAtEnd: req.body.orderDiagnosedAtEnd,
            orderCompletedAt: req.body.orderCompletedAt,
            orderCompletedAtStart: req.body.orderCompletedAtStart,
            orderCompletedAtEnd: req.body.orderCompletedAtEnd,
            orderIssuedAt: req.body.orderIssuedAt,
            orderIssuedAtStart: req.body.orderIssuedAtStart,
            orderIssuedAtEnd: req.body.orderIssuedAtEnd,
            orderCreatedBy: createdBy.toString(),
            orderDiagnosedBy: diagnosedBy.toString(),
            orderCompletedBy: completedBy.toString(),
            orderIssuedBy: issuedBy.toString(),
            deviceOSVersion: req.body.deviceOSVersion,
            individualOwner: individualOwner,
            legalEntityOwner: legalEntityOwner,
            individualOwnerId: req.body.individualOwnerId,
            legalEntityOwnderId: req.body.legalEntityOwnerId,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            orderTypes: orderTypes,
            orderStates: orderStates,
            deviceStates: deviceStates,
            orders: orders,
            users: users,
            timePassedVerbose: timePassedVerbose,
            modal: {
                open: false,
                sameCustomerMessage: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.getSavedFilterContent = async (req, res, next) => {
    try {
        // запрашиваем списки для корректной загрузки фильтра ремонтов
        const orderFilters = await OrderFilter.find({
            $or: [{ isShared: true }, { createdBy: req.session.user._id }],
        });
        const orderTypes = await OrderType.find();
        const users = await User.find({ role: { $ne: "User" } });
        const manufacturers = await Manufacturer.find();
        const deviceTypes = await DeviceType.find();
        const orderStates = await Order.schema.path("state").enumValues;
        const deviceStates = await Device.schema.path("state").enumValues;

        // запрашиваем выбранный фильтр
        const filter = await OrderFilter.findById(req.params.filterId);

        // преобразовываем createdBy, diagnosedBy, completedBy, issuedBy в формат для поиска в mongodb
        const mongoUserVariable = async (keys) => {
            let idsArray = [];
            if (Array.isArray(keys)) {
                let splittedArray = keys.map((key) => {
                    return key.split(" ");
                });
                for (el of splittedArray) {
                    const user = await User.findOne({
                        $and: [{ "name.last": el[1] }, { "name.first": el[0] }],
                    });
                    idsArray.push(user._id);
                }
                return idsArray;
            } else if (keys) {
                const userData = keys.split(" ");
                const user = await User.findOne({
                    $and: [
                        {
                            "name.last": userData[1],
                        },
                        { "name.first": userData[0] },
                    ],
                });
                idsArray.push(user._id);
                return idsArray;
            } else {
                return { $exists: true };
            }
        };

        // преобразовываем dateRange в формат для поиска в mongodb
        const mongoDateRange = (key) => {
            return key[0] ? { $gte: key[0], $lte: key[1] } : { $exists: true };
        };

        // function for correct variables search in mongodb
        const mongoVariable = (key) => {
            return key
                ? key.length > 0
                    ? { $in: key }
                    : { $exists: true }
                : { $exists: true };
        };

        //prepairing search parameters
        const orderState = mongoVariable(filter.parameters.orderState);
        const orderType = mongoVariable(filter.parameters.orderType);
        const providerOrderId = mongoVariable(
            filter.parameters.providerOrderId
        );
        const orderNo = mongoVariable(filter.parameters.orderNo);
        const deviceType = mongoVariable(filter.parameters.deviceType);
        const deviceManufacturer = mongoVariable(
            filter.parameters.deviceManufacturer
        );
        const deviceModel = mongoVariable(filter.parameters.deviceModel);
        const deviceItemCode = mongoVariable(filter.parameters.deviceItemCode);
        const deviceSN = mongoVariable(filter.parameters.deviceSN);
        const deviceIMEI = mongoVariable(filter.parameters.deviceIMEI);
        const deviceState = mongoVariable(filter.parameters.deviceState);
        const deviceOSVersion = mongoVariable(
            filter.parameters.deviceOSVersion
        );

        const createdBy = await mongoVariable(filter.parameters.orderCreatedBy);
        const diagnosedBy = await mongoVariable(
            filter.parameters.orderDiagnosedBy
        );
        const completedBy = await mongoVariable(
            filter.parameters.orderCompletedBy
        );
        const issuedBy = await mongoVariable(filter.parameters.orderIssuedBy);

        const createdAt = mongoDateRange([
            filter.parameters.orderCreatedAt.start,
            filter.parameters.orderCreatedAt.end,
        ]);

        const diagnosedAt = mongoDateRange([
            filter.parameters.orderDiagnosedAt.start,
            filter.parameters.orderDiagnosedAt.end,
        ]);

        const completedAt = mongoDateRange([
            filter.parameters.orderCompletedAt.start,
            filter.parameters.orderCompletedAt.end,
        ]);

        const issuedAt = mongoDateRange([
            filter.parameters.orderIssuedAt.start,
            filter.parameters.orderIssuedAt.end,
        ]);

        const individualOwner = filter.parameters.deviceIndividualOwner
            ? await Customer.findById(filter.parameters.deviceIndividualOwner)
            : "";
        const legalEntityOwner = filter.parameters.deviceLegalEntityOwner
            ? await legalEntity.findById(
                  filter.parameters.deviceLegalEntityOwner
              )
            : "";
        const individualCustomer = filter.parameters.orderIndividualCustomer
            ? await Customer.findById(filter.parameters.orderIndividualCustomer)
            : "";
        const legalEntityCustomer = filter.parameters.orderLegalEntityCustomer
            ? await legalEntity.findById(
                  filter.parameters.orderLegalEntityCustomer
              )
            : "";

        const individualOwnerData = !filter.parameters.deviceIndividualOwner
            ? { $exists: true }
            : {
                  individual: {
                      _id: individualOwner._id,
                      firstName: individualOwner.name.first,
                      lastName: individualOwner.name.last,
                      middleName: individualOwner.name.middle,
                      email: individualOwner.contacts.email,
                  },
              };

        const legalEntityOwnerData = !filter.parameters.deviceLegalEntityOwner
            ? { $exists: true }
            : {
                  legalEntity: {
                      _id: legalEntityOwner._id,
                      shortName: legalEntityOwner.shortName,
                      fullName: legalEntityOwner.fullName,
                      phone: legalEntityOwner.phone,
                      email: legalEntityOwner.email,
                  },
              };

        const individualCustomerData = !individualCustomer
            ? { $exists: true }
            : {
                  individual: {
                      _id: individualCustomer._id,
                      firstName: individualCustomer.name.first,
                      lastName: individualCustomer.name.last,
                      middleName: individualCustomer.name.middle,
                      phone: individualCustomer.contacts.phone,
                      email: individualCustomer.contacts.email,
                  },
              };

        const legalEntityCustomerData = !legalEntityCustomer
            ? { $exists: true }
            : {
                  legalEntity: {
                      _id: legalEntityCustomer._id,
                      shortName: legalEntityCustomer.shortName,
                      fullName: legalEntityCustomer.fullName,
                      phone: legalEntityCustomer.phone,
                      email: legalEntityCustomer.email,
                      isDealer: legalEntityCustomer.isDealer,
                      address: {
                          city: legalEntityCustomer.city,
                          street: legalEntityCustomer.street,
                          house: legalEntityCustomer.house,
                      },
                  },
              };

        const orders = await Order.find({
            $and: [
                { state: orderState },
                { "type.title": orderType },
                { providerOrderId: providerOrderId },
                { orderNo: orderNo },
                { createdAt: createdAt },
                { "createdBy.id": createdBy },
                { diagnosedAt: diagnosedAt },
                { diagnosedBy: diagnosedBy },
                { completedAt: completedAt },
                { completedBy: completedBy },
                { issuedAt: issuedAt },
                { issuedBy: issuedBy },
                { customer: individualCustomerData },
                { customer: legalEntityCustomerData },
                { "device.type": deviceType },
                { "device.manufacturer": deviceManufacturer },
                { "device.model": deviceModel },
                { "device.itemCode": deviceItemCode },
                { "device.sn": deviceSN },
                {
                    $or: [
                        { "device.imei1": deviceIMEI },
                        { "device.imei2": deviceIMEI },
                    ],
                },
                { "device.osVersion": deviceOSVersion },
                { "device.state": deviceState },
                { "device.owner": individualOwnerData },
                { "device.owner": legalEntityOwnerData },
            ],
        });

        return res.render("order/index", {
            pageTitle: `Ремонты | ${res.locals.userCompany.title.short}`,
            path: "/orders",
            orderFilterId: filter._id,
            orderFilters: orderFilters,
            orderState: filter.parameters.orderState || [],
            orderType: filter.parameters.orderType || [],
            orderNo: filter.parameters.orderNo,
            providerOrderId: filter.parameters.providerOrderId,
            individualCustomer: individualCustomer,
            legalEntityCustomer: legalEntityCustomer,
            individualCustomerId: filter.parameters.orderIndividualCustomer,
            legalEntityCustomerId: filter.parameters.orderLegalEntityCustomer,
            orderCreatedAt: filter.parameters.orderCreatedAt.full,
            orderCreatedAtStart: filter.parameters.orderCreatedAt.start,
            orderCreatedAtEnd: filter.parameters.orderCreatedAt.end,
            orderDiagnosedAt: filter.parameters.orderDiagnosedAt.full,
            orderDiagnosedAtStart: filter.parameters.orderDiagnosedAt.start,
            orderDiagnosedAtEnd: filter.parameters.orderDiagnosedAt.end,
            orderCompletedAt: filter.parameters.orderCompletedAt.full,
            orderCompletedAtStart: filter.parameters.orderCompletedAt.start,
            orderCompletedAtEnd: filter.parameters.orderCompletedAt.end,
            orderIssuedAt: filter.parameters.orderIssuedAt.full,
            orderIssuedAtStart: filter.parameters.orderIssuedAt.start,
            orderIssuedAtEnd: filter.parameters.orderIssuedAt.end,
            orderCreatedBy: createdBy.toString(),
            orderDiagnosedBy: diagnosedBy.toString(),
            orderCompletedBy: completedBy.toString(),
            orderIssuedBy: issuedBy.toString(),
            deviceType: filter.parameters.deviceType || [],
            deviceManufacturer: filter.parameters.deviceManufacturer || [],
            deviceModel: filter.parameters.deviceModel,
            deviceItemCode: filter.parameters.deviceItemCode,
            deviceSN: filter.parameters.deviceSN,
            deviceIMEI: filter.parameters.deviceIMEI,
            deviceState: filter.parameters.deviceState || [],
            deviceOSVersion: filter.parameters.deviceOSVersion,
            individualOwner: individualOwner,
            legalEntityOwner: legalEntityOwner,
            individualOwnerId: filter.parameters.deviceIndividualOwner,
            legalEntityOwnerId: filter.parameters.legalEntityOwner,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            orderTypes: orderTypes,
            orderStates: orderStates,
            deviceStates: deviceStates,
            orders: orders,
            users: users,
            timePassedVerbose: timePassedVerbose,
            modal: {
                open: false,
                sameCustomerMessage: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postDeleteOrderFilter = async (req, res, next) => {
    try {
        await OrderFilter.deleteOne({ title: req.body.orderFilter });
        return res.redirect("/orders");
    } catch (err) {
        throw err;
    }
};

exports.postFastOrderSearch = async (req, res, next) => {
    try {
        const orderTypes = await OrderType.find();
        const manufacturers = await Manufacturer.find();
        const deviceTypes = await DeviceType.find();
        const users = await User.find({ role: { $ne: "User" } });
        const orderStates = Order.schema.path("state").enumValues;
        const deviceStates = Device.schema.path("state").enumValues;

        const searchParams = new RegExp(req.body.searchParams);

        const orders = await Order.find({
            $or: [
                {
                    orderNo: isNaN(req.body.searchParams)
                        ? null
                        : req.body.searchParams,
                },
                { providerOrderId: searchParams },
                { "device.sn": searchParams },
                { "device.prevSns": searchParams },
            ],
        });

        if (orders.length === 1) {
            return res.redirect(`/orders/view/${orders[0]._id}`);
        }

        return res.render("order/index", {
            orderFilterId: null,
            orderState: req.body.orderState || [],
            orderType: req.body.orderType || [],
            orderNo: req.body.orderNo,
            providerOrderId: req.body.providerOrderId,
            orderCreatedAt: req.body.orderCreatedAt,
            orderCreatedAtStart: req.body.orderCreatedAtStart,
            orderCreatedAtEnd: req.body.orderCreatedAtEnd,
            orderDiagnosedAt: req.body.orderDiagnosedAt,
            orderDiagnosedAtStart: req.body.orderDiagnosedAtStart,
            orderDiagnosedAtEnd: req.body.orderDiagnosedAtEnd,
            orderCompletedAt: req.body.orderCompletedAt,
            orderCompletedAtStart: req.body.orderCompletedAtStart,
            orderCompletedAtEnd: req.body.orderCompletedAtEnd,
            orderIssuedAt: req.body.orderIssuedAt,
            orderIssuedAtStart: req.body.orderIssuedAtStart,
            orderIssuedAtEnd: req.body.orderIssuedAtEnd,
            orderCreatedBy: [],
            orderDiagnosedBy: [],
            orderCompletedBy: [],
            orderIssuedBy: [],
            individualCustomer: req.body.individualCustomer,
            legalEntityCustomer: req.body.legalEntityCustomer,
            individualCustomerId: req.body.individualCustomerId,
            legalEntityCustomerId: req.body.legalEntityCustomerId,
            deviceType: req.body.deviceType || [],
            deviceManufacturer: req.body.deviceManufacturer || [],
            deviceModel: req.body.deviceModel,
            deviceItemCode: req.body.deviceItemCode,
            deviceSN: req.body.deviceSN,
            deviceIMEI: req.body.deviceIMEI,
            deviceState: req.body.deviceState || [],
            deviceOSVersion: req.body.deviceOSVersion,
            individualOwner: req.body.individualOwner,
            legalEntityOwner: req.body.legalEntityOwner,
            individualOwnerId: req.body.individualOwnerId,
            legalEntityOwnderId: req.body.legalEntityOwnerId,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            orderTypes: orderTypes,
            orderStates: orderStates,
            deviceStates: deviceStates,
            orders: orders,
            users: users,
            timePassedVerbose: timePassedVerbose,
            pageTitle: `Все заявки | ${res.locals.userCompany.title.short}`,
            path: "/orders",
            modal: {
                open: false,
                sameCustomerMessage: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoCompleteSearch = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const orderFilter = Order.find(
        {
            $or: [
                { orderNo: parseFloat(regex.toString().slice(1)) },
                { providerOrderId: regex },
                { "device.sn": regex },
                { "device.imei1": regex },
                { "device.imei2": regex },
                { "device.owner.individual.lastName": regex },
                { "device.owner.legelEntity.shortName": regex },
                { "device.owner.legelEntity.fullName": regex },
            ],
        },
        { orderNo: 1, providerOrderId: 1 }
    ).limit(20);
    orderFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((order) => {
                    let obj = {
                        label: `Ремонт: ${order.orderNo}, ${order.providerOrderId}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
