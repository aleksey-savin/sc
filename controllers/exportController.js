const fs = require("fs");
const moment = require("moment");
const ExcelJS = require("exceljs");

const Order = require("../models/order");

exports.postExportLenovoOrders = async (req, res, next) => {
    try {
        const orders = await Order.find({
            "device.manufacturer": "Lenovo",
            "type.warranty": true,
            $and: [
                { providerOrderState: { $ne: "Закрыт" } },
                { providerOrderState: { $ne: "Не требуется" } },
            ],
        });

        const workbook = new ExcelJS.Workbook();
        const sheet = workbook.addWorksheet("Lenovo");
        sheet.addRow([
            "Internal Reference Number",
            "Date In",
            "First Name",
            "Last Name",
            "Telephone",
            "Email",
            "City",
            "Postal Code",
            "Street",
            "House Number",
            "Category",
            "Model",
            "Serial Number",
            "New IMEI 1",
            "New IMEI 2",
            "Problem Description",
            "Condition Code",
            "Symptom Code",
            "Repair Status",
            "Part 1",
            "IRIS 1 Section",
            "IRIS 1 Defect",
            "IRIS 1 Repair",
            "Part 1 Status",
            "Part 2",
            "IRIS 2 Section",
            "IRIS 2 Defect",
            "IRIS 2 Repair",
            "Part 2 Status",
            "Part 3",
            "IRIS 3 Section",
            "IRIS 3 Defect",
            "IRIS 3 Repair",
            "Part 3 Status",
            "Part 4",
            "IRIS 4 Section",
            "IRIS 4 Defect",
            "IRIS 4 Repair",
            "Part 4 Status",
            "Part 5",
            "IRIS 5 Section",
            "IRIS 5 Defect",
            "IRIS 5 Repair",
            "Part 5 Status",
        ]);

        for (order of orders) {
            let customer = order.customer.individual._id
                ? { ...order.customer.individual }
                : { ...order.customer.legalEntity };
            if (customer.isDealer) {
                customer.shortName = `DIL_${customer.shortName}`;
            }

            const deviceCategory = [
                "Ноутбук",
                "Моноблок",
                "Системный блок",
                "Неттоп",
                "Рабочая станция",
            ].includes(order.device.type)
                ? "1 Ноутбуки, моноблоки, системные блоки, рабочие станции"
                : ["Планшет"].includes(order.device.type)
                ? "2 Планшеты"
                : ["Смартфон"].includes(order.device.type)
                ? "3 Смартфоны"
                : ["Монитор"].includes(order.device.type)
                ? "5 Мониторы"
                : "4 Аксессуары";

            repairStatus = [
                "Ремонт окончен",
                "Устройство списано",
                "Устройство выдано",
                "Выдан акт неремонтопригодности",
            ].includes(order.state)
                ? "2 Завершён"
                : "1 Не завершён";

            part1 = {
                partNumber: order.spareParts[0]?.partNumber || "",
                sectionCode: `${order.codes.sectionCode[0]?.code || ""} ${
                    order.codes.sectionCode[0]?.title || ""
                }`.trim(),
                defectCode: `${order.codes.defectCode[0]?.code || ""} ${
                    order.codes.defectCode[0]?.title || ""
                }`.trim(),
                repairCode: `${order.codes.repairCode[0]?.code || ""} ${
                    order.codes.repairCode[0]?.title || ""
                }`.trim(),
                state: !order.spareParts[0]?.partNumber || !order.codes.repairCode[0]
                    ? ""
                    : order.spareParts[0]?.DOA
                    ? "3 DOA"
                    : order.spareParts[0]?.notUsed
                    ? "2 Возвр. не использованной"
                    : "1 Возвр. использованной",
            };

            part2 = {
                partNumber: order.spareParts[1]?.partNumber || "",
                sectionCode: `${order.codes.sectionCode[1]?.code || ""} ${
                    order.codes.sectionCode[1]?.title || ""
                }`.trim(),
                defectCode: `${order.codes.defectCode[1]?.code || ""} ${
                    order.codes.defectCode[1]?.title || ""
                }`.trim(),
                repairCode: `${order.codes.repairCode[1]?.code || ""} ${
                    order.codes.repairCode[1]?.title || ""
                }`.trim(),
                state: !order.spareParts[1]?.partNumber || !order.codes.repairCode[1]
                    ? ""
                    : order.spareParts[1]?.DOA
                    ? "3 DOA"
                    : order.spareParts[1]?.notUsed
                    ? "2 Возвр. не использованной"
                    : "1 Возвр. использованной",
            };

            part3 = {
                partNumber: order.spareParts[2]?.partNumber || "",
                sectionCode: `${order.codes.sectionCode[2]?.code || ""} ${
                    order.codes.sectionCode[2]?.title || ""
                }`.trim(),
                defectCode: `${order.codes.defectCode[2]?.code || ""} ${
                    order.codes.defectCode[2]?.title || ""
                }`.trim(),
                repairCode: `${order.codes.repairCode[2]?.code || ""} ${
                    order.codes.repairCode[2]?.title || ""
                }`.trim(),
                state: !order.spareParts[2]?.partNumber || !order.codes.repairCode[2]
                    ? ""
                    : order.spareParts[2]?.DOA
                    ? "3 DOA"
                    : order.spareParts[2]?.notUsed
                    ? "2 Возвр. не использованной"
                    : "1 Возвр. использованной",
            };

            part4 = {
                partNumber: order.spareParts[3]?.partNumber || "",
                sectionCode: `${order.codes.sectionCode[3]?.code || ""} ${
                    order.codes.sectionCode[3]?.title || ""
                }`.trim(),
                defectCode: `${order.codes.defectCode[3]?.code || ""} ${
                    order.codes.defectCode[3]?.title || ""
                }`.trim(),
                repairCode: `${order.codes.repairCode[3]?.code || ""} ${
                    order.codes.repairCode[3]?.title || ""
                }`.trim(),
                state: !order.spareParts[3]?.partNumber || !order.codes.repairCode[3]
                    ? ""
                    : order.spareParts[3]?.DOA
                    ? "3 DOA"
                    : order.spareParts[3]?.notUsed
                    ? "2 Возвр. не использованной"
                    : "1 Возвр. использованной",
            };

            part5 = {
                partNumber: order.spareParts[4]?.partNumber || "",
                sectionCode: `${order.codes.sectionCode[4]?.code || ""} ${
                    order.codes.sectionCode[4]?.title || ""
                }`.trim(),
                defectCode: `${order.codes.defectCode[4]?.code || ""} ${
                    order.codes.defectCode[4]?.title || ""
                }`.trim(),
                repairCode: `${order.codes.repairCode[4]?.code || ""} ${
                    order.codes.repairCode[4]?.title || ""
                }`.trim(),
                state: !order.spareParts[4]?.partNumber || !order.codes.repairCode[4]
                    ? ""
                    : order.spareParts[4]?.DOA
                    ? "3 DOA"
                    : order.spareParts[4]?.notUsed
                    ? "2 Возвр. не использованной"
                    : "1 Возвр. использованной",
            };

            sheet.addRow([
                order.orderNo,
                order.createdAt,
                customer.firstName || customer.shortName,
                customer.lastName || customer.shortName,
                customer.phone
                    .replace(/[^\w\s]/gi, "")
                    .replace(/^8/, "")
                    .replace(/^7/, ""),
                customer.email,
                customer.address.city,
                "690000",
                customer.address.street,
                customer.address.house,
                deviceCategory,
                order.device.model,
                order.device.sn,
                order.newDeviceIdentity ? order.device.imei1 : "",
                order.newDeviceIdentity ? order.device.imei2 : "",
                order.statedMalfunction,
                `${order.codes.conditionCode?.code || ""} ${
                    order.codes.conditionCode?.title || ""
                }`.trim(),
                `${order.codes.symptomCode?.code || ""} ${
                    order.codes.symptomCode?.title || ""
                }`.trim(),
                repairStatus,
                part1.partNumber,
                part1.sectionCode,
                part1.defectCode,
                part1.repairCode,
                part1.state,
                part2.partNumber,
                part2.sectionCode,
                part2.defectCode,
                part2.repairCode,
                part2.state,
                part3.partNumber,
                part3.sectionCode,
                part3.defectCode,
                part3.repairCode,
                part3.state,
                part4.partNumber,
                part4.sectionCode,
                part4.defectCode,
                part4.repairCode,
                part4.state,
                part5.partNumber,
                part5.sectionCode,
                part5.defectCode,
                part5.repairCode,
                part5.state,
            ]);
        }

        res.setHeader("Content-Type", "application/xlsx");
        res.setHeader(
            "Content-Disposition",
            `inline; filename=lenovo-export-${moment(new Date()).format(
                "DD-MM-YYYY"
            )}.xlsx`
        );

        await workbook.xlsx.write(res);
        res.end();
    } catch (err) {
        throw err;
    }
};

exports.postExportPhilipsAOCOrders = async (req, res, next) => {
    try {
        const orders = await Order.find({
            $or: [
                { "device.manufacturer": "Philips" },
                { "device.manufacturer": "AOC" },
            ],
            "type.warranty": true,
            $and: [
                { providerOrderState: { $ne: "Закрыт" } },
                { providerOrderState: { $ne: "Не требуется" } },
            ],
        });

        const workbook = new ExcelJS.Workbook();
        const sheet = workbook.addWorksheet("Lenovo");
        sheet.addRow([
            "Internal Reference Number",
            "Brand",
            "Date Sold",
            "Date In",
            "Repair Status",
            "Delivery",
            "First Name",
            "Last Name",
            "Telephone",
            "City",
            "Street",
            "House Number",
            "Model",
            "Serial Number",
            "Problem Description",
            "Symptom Code",
            "IRIS Repair",
            "Repair Action",
            "Mainboard FirmWare",
            "IRIS 1 Section",
            "Part 1", // партномер первой запчасти
            "IRIS 1 Defect", // серийный номер дефектной запчасти
            "IRIS 2 Section",
            "Part 2", // партномер первой запчасти
            "IRIS 2 Defect", // серийный номер дефектной запчасти
            "IRIS 3 Section",
            "Part 3", // партномер первой запчасти
            "IRIS 3 Defect", // серийный номер дефектной запчасти
            "IRIS 4 Section",
            "Part 4", // партномер первой запчасти
            "IRIS 4 Defect", // серийный номер дефектной запчасти
            "IRIS 5 Section",
            "Part 5", // партномер первой запчасти
            "IRIS 5 Defect", // серийный номер дефектной запчасти
        ]);

        for (order of orders) {
            let customer = order.customer.individual._id
                ? { ...order.customer.individual }
                : { ...order.customer.legalEntity };
            if (customer.isDealer) {
                customer.shortName = `DIL_${customer.shortName}`;
            }

            const deviceBrand =
                order.device.manufacturer === "Philips" ? "2 TPV" : "1 AOC";

            repairStatus = [
                "Ремонт окончен",
                "Устройство списано",
                "Устройство выдано",
                "Выдан акт неремонтопригодности",
            ].includes(order.state)
                ? "2 Завершён"
                : "1 Не завершён";

            part1 = order.spareParts[0]?.partNumber
                ? {
                      partNumber: order.spareParts[0]?.partNumber || "",
                      sectionCode: `${order.codes.sectionCode[0]?.code || ""} ${
                          order.codes.sectionCode[0]?.title || ""
                      }`.trim(),
                      defectCode: `${order.codes.defectCode[0]?.code || ""} ${
                          order.codes.defectCode[0]?.title || ""
                      }`.trim(),
                      repairCode: `${order.codes.repairCode[0]?.code || ""} ${
                          order.codes.repairCode[0]?.title || ""
                      }`.trim(),
                  }
                : {
                      partNumber: "",
                      sectionCode: "",
                      defectCode: "",
                      repairCode: "",
                  };

            part2 = order.spareParts[1]?.partNumber
                ? {
                      partNumber: order.spareParts[1]?.partNumber || "",
                      sectionCode: `${order.codes.sectionCode[1]?.code || ""} ${
                          order.codes.sectionCode[1]?.title || ""
                      }`.trim(),
                      defectCode: `${order.codes.defectCode[1]?.code || ""} ${
                          order.codes.defectCode[1]?.title || ""
                      }`.trim(),
                      repairCode: `${order.codes.repairCode[1]?.code || ""} ${
                          order.codes.repairCode[1]?.title || ""
                      }`.trim(),
                  }
                : {
                      partNumber: "",
                      sectionCode: "",
                      defectCode: "",
                      repairCode: "",
                  };

            part3 = order.spareParts[2]?.partNumber
                ? {
                      partNumber: order.spareParts[2]?.partNumber || "",
                      sectionCode: `${order.codes.sectionCode[2]?.code || ""} ${
                          order.codes.sectionCode[2]?.title || ""
                      }`.trim(),
                      defectCode: `${order.codes.defectCode[2]?.code || ""} ${
                          order.codes.defectCode[2]?.title || ""
                      }`.trim(),
                      repairCode: `${order.codes.repairCode[2]?.code || ""} ${
                          order.codes.repairCode[2]?.title || ""
                      }`.trim(),
                  }
                : {
                      partNumber: "",
                      sectionCode: "",
                      defectCode: "",
                      repairCode: "",
                  };

            part4 = order.spareParts[3]?.partNumber
                ? {
                      partNumber: order.spareParts[3]?.partNumber || "",
                      sectionCode: `${order.codes.sectionCode[3]?.code || ""} ${
                          order.codes.sectionCode[3]?.title || ""
                      }`.trim(),
                      defectCode: `${order.codes.defectCode[3]?.code || ""} ${
                          order.codes.defectCode[3]?.title || ""
                      }`.trim(),
                      repairCode: `${order.codes.repairCode[3]?.code || ""} ${
                          order.codes.repairCode[3]?.title || ""
                      }`.trim(),
                  }
                : {
                      partNumber: "",
                      sectionCode: "",
                      defectCode: "",
                      repairCode: "",
                  };

            part5 = order.spareParts[4]?.partNumber
                ? {
                      partNumber: order.spareParts[4]?.partNumber || "",
                      sectionCode: `${order.codes.sectionCode[4]?.code || ""} ${
                          order.codes.sectionCode[4]?.title || ""
                      }`.trim(),
                      defectCode: `${order.codes.defectCode[4]?.code || ""} ${
                          order.codes.defectCode[4]?.title || ""
                      }`.trim(),
                      repairCode: `${order.codes.repairCode[4]?.code || ""} ${
                          order.codes.repairCode[4]?.title || ""
                      }`.trim(),
                  }
                : {
                      partNumber: "",
                      sectionCode: "",
                      defectCode: "",
                      repairCode: "",
                  };

            sheet.addRow([
                order.orderNo,
                deviceBrand,
                order.device.dateOfSale,
                order.createdAt,
                repairStatus,
                "1 Ремонт в АСЦ",
                customer.firstName || customer.shortName,
                customer.lastName || customer.shortName,
                customer.phone
                    .replace(/[^\w\s]/gi, "")
                    .replace(/^8/, "")
                    .replace(/^7/, ""),
                customer.address.city,
                customer.address.street,
                customer.address.house,
                order.device.model,
                order.device.sn,
                order.statedMalfunction,
                `${order.codes.symptomCode?.code || ""} ${
                    order.codes.symptomCode?.title || ""
                }`.trim(),
                part1.repairCode,
                "", // описание ремонта
                "", // mainboard firmware
                part1.sectionCode,
                part1.partNumber,
                "", // серийный номер дефектной запчасти
                part1.defectCode,
                part2.sectionCode,
                "", // серийный номер дефектной запчасти
                part2.defectCode,
                part3.sectionCode,
                "", // серийный номер дефектной запчасти
                part3.defectCode,
                part4.sectionCode,
                "", // серийный номер дефектной запчасти
                part4.defectCode,
                part5.sectionCode,
                "", // серийный номер дефектной запчасти
                part5.defectCode,
            ]);
        }

        res.setHeader("Content-Type", "application/xlsx");
        res.setHeader(
            "Content-Disposition",
            `inline; filename=philips-aoc-export-${moment(new Date()).format(
                "DD-MM-YYYY"
            )}.xlsx`
        );

        await workbook.xlsx.write(res);
        res.end();
    } catch (err) {
        throw err;
    }
};
