const OrderType = require("../models/orderType");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const orderTypes = await OrderType.find().sort({ priority: -1 });
        return res.render("orderType/index", {
            orderTypes: orderTypes,
            pageTitle: `Типы ремонта | ${res.locals.userCompany.title.short}`,
            path: "/order-types",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = async (req, res, next) => {
    try {
        return res.render("orderType/add", {
            pageTitle: `Добавление Типа Ремонта | ${res.locals.userCompany.title.short}`,
            path: "order-types/add",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                warranty: false,
                isActive: false,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    const title = req.body.title;
    const isActive = req.body.isActive ? true : false;
    const warranty = req.body.warranty ? true : false;
    if (!errors.isEmpty()) {
        //return res.state(422).render <-- почему-то не работает
        return res.render("orderType/add", {
            path: "order-types/add",
            pageTitle: `Добавление Типа Ремонта | ${res.locals.userCompany.title.short}`,
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: title,
                warranty: warranty,
                isActive: isActive,
            },
        });
    }
    try {
        const orderType = new OrderType({
            title: title,
            warranty: warranty,
            isActive: isActive,
        });

        await orderType.save();
        return res.redirect("/order-types");
    } catch (err) {
        console.log(err);
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const orderTypeId = req.params.orderTypeId;
    try {
        const orderType = await OrderType.findById(orderTypeId);
        if (!orderType) {
            return res.redirect("/order-types");
        }
        return res.render("orderType/edit", {
            pageTitle: `Редактирование Типа Ремонта | ${res.locals.userCompany.title.short}`,
            path: "order-types/edit",
            orderType: orderType,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                warranty: false,
                isActive: false,
                priority: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    const orderTypeId = req.body.orderTypeId;
    const warranty = req.body.warranty ? true : false;
    const isActive = req.body.isActive ? true : false;

    try {
        const orderType = await OrderType.findById(orderTypeId);
        if (!orderType) {
            return res.redirect("/order-types");
        }

        if (!errors.isEmpty()) {
            //return res.state(422).render <-- почему-то не работает
            return res.render("orderType/edit", {
                path: "order-types/edit",
                pageTitle: `Редактирование Типа Ремонта | ${res.locals.userCompany.title.short}`,
                orderType: orderType,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    warranty: warranty,
                    isActive: isActive,
                    priority: req.body.priority,
                },
            });
        }
    } catch (err) {
        throw err;
    }

    try {
        const orderType = await OrderType.findById(orderTypeId);

        orderType.title = req.body.title;
        orderType.warranty = warranty;
        orderType.isActive = isActive;
        orderType.priority = req.body.priority;

        await orderType.save();
        return res.redirect(`/order-types`);
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const orderType = OrderType.findOne({ _id: req.body.orderTypeId });
    try {
        if (!orderType.isSystem) {
            await OrderType.deleteOne({ _id: req.body.orderTypeId });
            return res.redirect("/order-types");
        }
        return res.redirect("/order-types");
    } catch (err) {
        throw err;
    }
};
