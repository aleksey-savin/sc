const RequestState = require("../models/requestState");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const requestStates = await RequestState.find().sort({ priority: -1 });
        return res.render("requestState/index", {
            requestStates: requestStates,
            pageTitle: `Все Статусы Запросов | ${res.locals.userCompany.title.short}`,
            path: "/",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = async (req, res, next) => {
    try {
        return res.render("requestState/add", {
            pageTitle: `Добавление Статуса Запроса | ${res.locals.userCompany.title.short}`,
            path: "request-states/add",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: null,
                isActive: false,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAdd = async (req, res, next) => {
    const name = req.body.name;
    const isActive = req.body.isActive ? true : false;    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.render("requestState/add", {
            path: "request-states/add",
            pageTitle: `Добавление Статуса Запроса | ${res.locals.userCompany.title.short}`,
            errorMessage: errors.array()[0].msg,            
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: name,
                isActive: isActive,
            },
        });
    }

    try {
        const requestState = new RequestState({
            name: name,
            isActive: isActive,
        });

        await requestState.save();
        return res.redirect("/request-states");
    } catch (err) {
        console.log(err);
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const requestStateId = req.params.requestStateId;
    const roles = RequestState.schema.path("alertLevels.low.target").enumValues;
    try {
        const requestState = await RequestState.findById(requestStateId);
        if (!requestState) {
            return res.redirect("/");
        }
        return res.render("requestState/edit", {
            pageTitle: `Редактирование Статуса Запроса | ${res.locals.userCompany.title.short}`,
            path: "request-states/edit",
            requestState: requestState,
            roles: roles,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                name: null,
                alertLevels: {
                    low: {
                        timeLimit: null,
                        target: null,
                    },
                    medium: {
                        timeLimit: null,
                        target: null,
                    },
                    high: {
                        timeLimit: null,
                        target: null,
                    },
                },
                isActive: false,
                priority: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {   
    const errors = validationResult(req);

    const stateId = req.body.requestId;
    const isActive = req.body.isActive ? true : false;
    const priority = req.body.priority;

    const requestStateId = req.body.requestId;
    const roles = RequestState.schema.path("alertLevels.low.target").enumValues;

    if (!errors.isEmpty()) {
        try {
            const requestState = await RequestState.findById(requestStateId);
            if (!requestState) {
                return res.redirect("/");
            }
            return res.render("requestState/edit", {
                pageTitle: `Редактирование Статуса Запроса | ${res.locals.userCompany.title.short}`,
                path: "request-states/edit",
                requestState: requestState,
                roles: roles,
                errorMessage: errors.array()[0].msg,                
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    name: req.body.name,
                    alertLevels: {
                        low: {
                            timeLimit: req.body.lowTimeLimit,
                            target: req.body.lowTarget,
                        },
                        medium: {
                            timeLimit: req.body.lowTimeLimit,
                            target: req.body.lowTarget,
                        },
                        high: {
                            timeLimit: req.body.lowTimeLimit,
                            target: req.body.lowTarget,
                        },
                    },
                    isActive: isActive,
                    priority: req.body.priority,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const requestState = await RequestState.findById(stateId);

        requestState.name = req.body.name;
        requestState.isActive = isActive;
        requestState.priority = priority;

        requestState.alertLevels.low.timeLimit = req.body.lowTimeLimit;
        requestState.alertLevels.low.target = req.body.lowTarget;

        requestState.alertLevels.medium.timeLimit = req.body.mediumTimeLimit;
        requestState.alertLevels.medium.target = req.body.mediumTarget;

        requestState.alertLevels.high.timeLimit = req.body.highTimeLimit;
        requestState.alertLevels.high.target = req.body.highTarget;

        await requestState.save();
        return res.redirect(`/request-states`);
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const state = RequestState.findOne({ _id: req.body.requestStateId });
    try {
        if (!state.isSystem) {
            await RequestState.deleteOne({ _id: req.body.requestStateId });
            return res.redirect("/request-states");
        }
        return res.redirect("/request-states");
    } catch (err) {
        throw err;
    }
};
