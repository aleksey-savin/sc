const Request = require("../models/request");
const RequestState = require("../models/requestState");
const Customer = require("../models/customer");
const Manufacturer = require("../models/manufacturer");
const DeviceType = require("../models/deviceType");
const Comment = require("../models/comment");
const Notificator = require("../middleware/telegramBot");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

const dateOptions = {
    day: "numeric",
    month: "2-digit",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
};

exports.getIndex = async (req, res, next) => {
    try {
        const timePassed = (date) => {
            const today = new Date();
            const getSeconds = Math.floor((today - date) / 1000); // получаем количество секунд
            const getMinutes = Math.floor(getSeconds / 60);
            const getHours = Math.floor(getMinutes / 60);
            const getDays = getHours > 24 ? getHours % 24 : 0;

            return [getDays, getHours, getMinutes, getSeconds];
        };

        const comments = await Comment.find({ "belongsTo.entity": "request" });

        const requests = await Request.find({ requestState: { $ne: "Архив" } });
        return res.render("request/index", {
            requests: requests,
            dateOptions: dateOptions,
            pageTitle: `Все запросы | ${res.locals.userCompany.title.short}`,
            path: "/",
            timePassed: timePassed,
            comments: comments,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.getView = async (req, res, next) => {
    try {
        const requestId = req.params.requestId;
        const request = await Request.findById(requestId);
        return res.render("request/view", {
            request: request,
            dateOptions: dateOptions,
            pageTitle: request.title,
            path: "/requests/view",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = async (req, res, next) => {
    try {
        const customers = await Customer.find();
        const sparePartAvailabilityList = Request.schema.path(
            "repair.sparePartAvailability"
        ).enumValues;
        const requestStates = await RequestState.find({ isActive: true }).sort({
            priority: -1,
        });
        return res.render("request/add", {
            pageTitle: `Добавление Запроса | ${res.locals.userCompany.title.short}`,
            path: "requests/add",
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            customers: customers,
            sparePartAvailabilityList: sparePartAvailabilityList,
            requestStates: requestStates,
            oldInput: {
                client: {
                    firstName: null,
                    lastName: null,
                    middleName: null,
                    phone: null,
                    email: null,
                },
                device: {
                    manufacturer: null,
                    deviceType: null,
                    model: null,
                },
                repair: {
                    statedMalfunction: null,
                    repairCost: null,
                    sparePartAvailability: null,
                    repairTerms: null,
                },
                requestState: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAdd = async (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const middleName = req.body.middleName;
    const phone = req.body.phone;
    const email = req.body.email;
    const manufacturer = await Manufacturer.findOne({
        name: req.body.manufacturer,
    }).populate("_id");
    const deviceType = await DeviceType.findOne({
        name: req.body.deviceType,
    }).populate("_id");
    const requestState = await RequestState.findOne({
        name: req.body.requestState,
    }).populate("_id");
    const model = req.body.model;
    const statedMalfunction = req.body.statedMalfunction;
    const repairCost = req.body.repairCost;
    const sparePartAvailability = req.body.sparePartAvailability;
    const repairTerms = req.body.repairTerms;

    const requestStates = await RequestState.find({ isActive: true }).sort({
        priority: -1,
    });

    const sparePartAvailabilityList = Request.schema.path(
        "repair.sparePartAvailability"
    ).enumValues;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).render("request/add", {
            path: "requests/add",
            pageTitle: `Добавление Запроса | ${res.locals.userCompany.title.short}`,
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            sparePartAvailabilityList: sparePartAvailabilityList,
            requestStates: requestStates,
            oldInput: {
                client: {
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    phone: phone,
                    email: email,
                },
                device: {
                    manufacturer: manufacturer,
                    deviceType: deviceType,
                    model: model,
                },
                repair: {
                    statedMalfunction: statedMalfunction,
                    repairCost: repairCost,
                    sparePartAvailability: sparePartAvailability,
                    repairTerms: repairTerms,
                },
                requestState: requestState,
            },
        });
    }
    try {
        const request = new Request({
            client: {
                firstName: firstName,
                lastName: lastName,
                middleName: middleName,
                phone: phone,
                email: email,
            },
            device: {
                manufacturer: manufacturer.name,
                deviceType: deviceType.name,
                model: model,
            },
            repair: {
                statedMalfunction: statedMalfunction,
                repairCost: repairCost,
                sparePartAvailability: sparePartAvailability,
                repairTerms: repairTerms,
            },
            alertLevels: {
                low: requestState.alertLevels.low.timeLimit,
                medium: requestState.alertLevels.medium.timeLimit,
                high: requestState.alertLevels.high.timeLimit,
            },
            requestState: requestState.name,

            createdBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
            updatedBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
            inStateSince: new Date(),
        });

        Notificator.send(
            `Новый запрос! \n
Телефон: ${phone} \n
Устройство: ${deviceType.name} ${manufacturer.name} ${model} \n
Заявленная неисправность: ${statedMalfunction} \n
<b>Статус запроса: ${requestState.name}</b>`,
            req.session.user
        );

        await request.save();
        return res.redirect("/requests");
    } catch (err) {
        console.log(err);
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const requestId = req.params.requestId;
    const sparePartAvailabilityList = Request.schema.path(
        "repair.sparePartAvailability"
    ).enumValues;
    const requestStates = await RequestState.find({ isActive: true }).sort({
        priority: -1,
    });
    const sparePartAvailability = req.body.sparePartAvailability;
    const comments = await Comment.find({
        "belongsTo.entity": "request",
        "belongsTo.id": requestId,
    });
    try {
        const request = await Request.findById(requestId);
        if (!request) {
            return res.redirect("/");
        }
        return res.render("request/edit", {
            pageTitle: `Редактирование Запроса | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            request: request,
            requestStates: requestStates,
            sparePartAvailabilityList: sparePartAvailabilityList,
            sparePartAvailability: sparePartAvailability,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            dateOptions: dateOptions,
            comments: comments,
            oldInput: {
                client: {
                    firstName: null,
                    lastName: null,
                    middleName: null,
                    phone: null,
                    email: null,
                },
                device: {
                    manufacturer: null,
                    deviceType: null,
                    model: null,
                },
                repair: {
                    statedMalfunction: null,
                    repairCost: null,
                    sparePartAvailability: null,
                    repairTerms: null,
                },
                requestState: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const sparePartAvailabilityList = Request.schema.path(
        "repair.sparePartAvailability"
    ).enumValues;

    const errors = validationResult(req);

    const requestId = req.body.requestId;

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const middleName = req.body.middleName;
    const phone = req.body.phone;
    const email = req.body.email;

    const manufacturer = await Manufacturer.findOne({
        name: req.body.manufacturer,
    }).populate("_id");
    const deviceType = await DeviceType.findOne({
        name: req.body.deviceType,
    }).populate("_id");
    const model = req.body.model;

    const statedMalfunction = req.body.statedMalfunction;
    const repairCost = req.body.repairCost;
    const sparePartAvailability = req.body.sparePartAvailability;
    const repairTerms = req.body.repairTerms;

    const requestState = await RequestState.findOne({
        name: req.body.requestState,
    });

    const requestStates = await RequestState.find({ isActive: true }).sort({
        priority: -1,
    });

    const request = await Request.findById(requestId);

    if (!errors.isEmpty()) {
        return res.status(422).render("request/edit", {
            pageTitle: `Редактирование Запроса | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            request: request,
            requestStates: requestStates,
            sparePartAvailabilityList: sparePartAvailabilityList,
            sparePartAvailability: sparePartAvailability,
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                client: {
                    firstName: firstName,
                    lastName: lastName,
                    middleName: middleName,
                    phone: phone,
                    email: email,
                },
                device: {
                    manufacturer: manufacturer,
                    deviceType: deviceType,
                    model: model,
                },
                repair: {
                    statedMalfunction: statedMalfunction,
                    repairCost: repairCost,
                    sparePartAvailability: sparePartAvailability,
                    repairTerms: repairTerms,
                },
                requestState: requestState,
            },
        });
    }

    try {
        const request = await Request.findById(requestId);
        const inStateSince =
            request.requestState === requestState
                ? request.inStateSince
                : new Date();

        request.client.firstName = firstName;
        request.client.lastName = lastName;
        request.client.middleName = middleName;
        request.client.phone = phone;
        request.client.email = email;

        request.device.manufacturer = manufacturer.name;
        request.device.deviceType = deviceType.name;
        request.device.model = model;

        request.repair.statedMalfunction = statedMalfunction;
        request.repair.repairCost = repairCost;
        request.repair.sparePartAvailability = sparePartAvailability;
        request.repair.repairTerms = repairTerms;

        request.alertLevels.low = requestState.alertLevels.low.timeLimit;
        request.alertLevels.medium = requestState.alertLevels.medium.timeLimit;
        request.alertLevels.high = requestState.alertLevels.high.timeLimit;

        request.requestState = requestState.name;
        request.inStateSince = inStateSince;
        request.updatedBy.id = req.session.user;
        request.updatedBy.name.first = req.session.user.name.first;
        request.updatedBy.name.last = req.session.user.name.last;

        await request.save();        
        return res.redirect(`/requests`);
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const requestId = req.body.requestId;
    try {
        await Request.deleteOne({ _id: requestId });
        return res.redirect("/requests");
    } catch (err) {
        throw err;
    }
};
