const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");
const User = require("../models/user");

// Авторизация
exports.getLogin = (req, res, next) => {
    res.render("auth/login", {
        path: "/login",
        pageTitle: "Вход",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
    });
};

exports.postLogin = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).render("auth/login", {
            path: "/login",
            pageTitle: "Регистрация",
            errorMessage: errors.array()[0].msg,
            successMessage: null,
            infoMessage: null,
        });
    }

    const email = req.body.email;
    const password = req.body.password;

    try {
        const user = await User.findOne({
            email: email,
        });
        if (!user) {
            req.flash("error", "Неверные имя пользователя или пароль.");
            return res.status(422).redirect("/login");
        }
        if (!user.active) {
            req.flash(
                "error",
                "Аккаунт не активен. Проверьте свой почтовый ящик или обратитесь к администратору"
            );
            return res.status(422).redirect("/login");
        }
        const doMatch = await bcrypt.compare(password, user.password);
        if (doMatch) {
            req.session.isLoggedIn = true;
            req.session.user = user;
            await req.session.save();
            return res.redirect("/");
        }
        req.flash("error", "Неверные имя пользователя или пароль.");
        return res.redirect("/login");
    } catch (err) {
        throw err;
    }
};

// Регистрация
exports.getSignup = (req, res, next) => {
    res.render("auth/signup", {
        path: "/signup",
        pageTitle: "Регистрация",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            email: null,
            phone: null,
            password: null,
            confirmPassword: null,
        },
    });
};

exports.postSignup = async (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    const errors = validationResult(req);
    try {
        if (!errors.isEmpty()) {
            return res.status(422).render("auth/signup", {
                path: "/signup",
                pageTitle: "Регистрация",
                errorMessage: errors.array()[0].msg,
                successMessage: null,
                infoMessage: null,
                oldInput: {
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    password: password,
                    confirmPassword: req.body.confirmPassword,
                },
            });
        }
        const hashedPassword = await bcrypt.hash(password, 12);
        crypto.randomBytes(32, async (err, buffer) => {
            const token = buffer.toString("hex");
            const userCount = User.find().count;
            const userRole = userCount === 0 ? "Admin" : "User";
            const user = new User({
                name: {
                    first: firstName,
                    last: lastName,
                },
                email: email,
                password: hashedPassword,
                role: userRole,
                active: false,
                resetToken: token,
                resetTokenExpiration: Date.now() + 3600000,
                verifyCode: Math.floor(Math.random() * 1000),
                verifyCodeExpiration: Date.now() + 3600,
            });
            await user.save();
            /* if (phone) {
                req.flash(
                    "success",
                    "На указанный номер телефона отправлен код подтверждения для активации аккаунта. Код действителен в течение 1 часа."
                );
                await res.redirect("/verifyAccount");
                return smsClient.messages.create({
                    body: `Код подтверждения: ${user.verifyCode}`,
                    from: myPhoneNumber,
                    to: user.phone
                });
            } */

            await res.redirect("/");

            const myCompany = await MyCompany.findById(
                req.session.user.company._id
            );

            //Mailgun API
            const domain = "helpdesk.f1lab.ru";
            const mailgun = require("mailgun-js")({
                domain: domain,
                apiKey: myCompany.apiKeys.mailgun,
            });

            await mailgun
                .messages()
                .send({
                    from: `noreply@${domain}`,
                    to: req.body.email,
                    subject: "Активация аккаунта",
                    html: `
                <p>Активация аккаунта на портале.</p>
                <p>Пройдите по <a href='https://portal.f1-service.pro/activate/${token}'>ссылке</a>, чтобы активировать свой аккаунт.</p>              `,
                })
                .then(() => {
                    req.flash(
                        "success",
                        "На указанный адрес отправлено письмо для активации аккаунта. Ссылка действительна в течение 24 часов."
                    );
                })
                .catch(() => {
                    req.flash("error", "Не удалось отправить письмо."); //NEEDED: think about error message text
                });

            return res.redirect("/");
        });
    } catch (err) {
        throw err;
    }
};

exports.postLogout = (req, res, next) => {
    req.session.destroy((err) => {
        if (err) {
            console.log(err);
        }
        res.redirect("/");
    });
};

exports.getReset = (req, res, next) => {
    res.render("auth/reset", {
        path: "/reset",
        pageTitle: "Сброс пароля",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
    });
};

exports.postReset = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("auth/reset", {
            path: "/reset",
            pageTitle: "Cброс пароля",
            errorMessage: errors.array()[0].msg,
            successMessage: null,
            infoMessage: null,
        });
    }

    try {
        crypto.randomBytes(32, async (err, buffer) => {
            const token = buffer.toString("hex");
            const user = await User.findOne({
                email: req.body.email,
            });
            if (!user) {
                req.flash("error", "Аккаунт с указанным email не найден");
                return res.redirect("/reset");
            }
            user.resetToken = token;
            user.resetTokenExpiration = Date.now() + 3600000;
            await user.save();

            await mailgun
                .messages()
                .send({
                    from: `noreply@${domain}`,
                    to: req.body.email,
                    subject: "Сброс пароля",
                    html: `
                <p>Вы запросили сброс пароля на портале f1-service.pro</p>
                <p>Кликните по <a href='http://portal.f1-service.pro/reset/${token}'>ссылке</a>, чтобы установить новый пароль.</p>
              `,
                })
                .then(() => {
                    req.flash(
                        "success",
                        "На указанный адрес отправлено письмо со ссылкой на восстановление пароля."
                    );
                })
                .catch(() => {
                    req.flash("error", "Не удалось отправить письмо."); //NEEDED: think about error message text
                });

            return res.redirect("/");
        });
    } catch (err) {
        throw err;
    }
};

exports.getNewPassword = async (req, res, next) => {
    const token = req.params.token;
    try {
        const user = await User.findOne({
            resetToken: token,
            resetTokenExpiration: {
                $gt: Date.now(),
            },
        });
        return res.render("auth/new_password", {
            path: `/reset/${token}`,
            pageTitle: "Новый пароль",
            userId: user._id.toString(),
            passwordToken: token,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.postNewPassword = async (req, res, next) => {
    const newPassword = req.body.password;
    const userId = req.body.userId;
    const passwordToken = req.body.passwordToken;
    let resetUser;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("auth/new_password", {
            pageTitle: "Новый пароль",
            userId: userId,
            passwordToken: req.body.passwordToken,
            errorMessage: errors.array()[0].msg,
            successMessage: null,
            infoMessage: null,
        });
    }

    try {
        const user = await User.findOne({
            resetToken: passwordToken,
            resetTokenExpiration: {
                $gt: Date.now(),
            },
            _id: userId,
        });
        resetUser = user;
        const hashedPassword = await bcrypt.hash(newPassword, 12);
        resetUser.password = hashedPassword;
        resetUser.resetToken = undefined;
        resetUser.resetTokenExpiration = undefined;
        await resetUser.save();
        req.flash("success", "Пароль успешно изменён.");
        return res.redirect("/login");
    } catch (err) {
        throw err;
    }
};

exports.getActivateUser = async (req, res, next) => {
    const token = req.params.token;
    try {
        const user = await User.findOne({
            resetToken: token,
            resetTokenExpiration: {
                $gt: Date.now(),
            },
        });
        if (!user) {
            req.flash(
                "error",
                "Скорее всего данный аккаунт уже был активирован ранее, попробуйте авторизоваться."
            );
            return res.redirect("/login");
        }
        user.active = true;
        user.resetToken = undefined;
        user.resetTokenExpiration = undefined;
        await user.save();
        req.flash(
            "success",
            "Вы успешно активировали свой аккаунт, пожалуйста, авторизуйтесь."
        );
        return res.redirect("/login");
    } catch (err) {
        throw err;
    }
};
