const Comment = require('../models/comment');

exports.postAdd = async (req, res, next) => {
    try {
        const entity = req.body.entityName;
        const entityId = req.body.entityId;
        const commentBody = req.body.commentBody;

        const comment = new Comment({
            belongsTo: {
                entity: entity,
                id: entityId,
            },
            commentBody: commentBody,
            createdBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
        });
        await comment.save();
        if (comment.belongsTo.entity === 'order') {
            return res.redirect(`/orders/view/${comment.belongsTo.id}`);
        }
        return res.redirect(`/${entity}s`);
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const entity = req.body.entityName;
    const commentId = req.body.commentId;

    try {
        const comment = await Comment.findOne({ _id: commentId });
        const commentEntity = comment.belongsTo.entity;
        const entityId = comment.belongsTo.id;
        await Comment.deleteOne({ _id: commentId });
        if (commentEntity === 'order') {
            return res.redirect(`/orders/view/${entityId}`);
        }
        return res.redirect(`/${entity}s`);
    } catch (err) {
        throw err;
    }
};
