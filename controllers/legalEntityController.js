const LegalEntity = require("../models/legalEntity");
const Order = require("../models/order");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const legalEntities = await LegalEntity.find();
        return res.render("legalEntity/index", {
            legalEntities: legalEntities,
            pageTitle: `Все Юридические лица | ${res.locals.userCompany.title.short}`,
            path: "/legal-entities",
        });
    } catch (err) {
        throw err;
    }
};

exports.getViewLegalEntity = async (req, res, next) => {
    try {
        const legalEntity = await LegalEntity.findById(req.params.id);
        const orders = await Order.find({
            $or: [
                { "device.owner.legalEntity._id": legalEntity._id },
                { "customer.legalEntity._id": legalEntity._id },
            ],
            $and: [
                { state: { $ne: "Устройство выдано" } },
                { state: { $ne: "Выдан акт неремонтопригодности" } },
            ],
        });
        return res.render("legalEntity/view", {
            legalEntity: legalEntity,
            orders: orders,
            pageTitle: `${legalEntity.shortName} | ${res.locals.userCompany.title.short}`,
            path: "/legal-entities/view",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAddLegalEntity = (req, res, next) => {
    res.render("legalEntity/add", {
        pageTitle: `Добавление Юридического Лица | ${res.locals.userCompany.title.short}`,
        path: "legal-entities/add",        
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            shortName: null,
            fullName: null,
            city: null,
            street: null,
            house: null,
            phone: null,
            email: null,
            isDealer: false,
        },
    });
};

exports.postAddLegalEntity = async (req, res, next) => {
    const errors = validationResult(req);
    const isDealer = req.body.isDealer ? true : false;

    if (!errors.isEmpty()) {
        return res.status(422).render("legalEntity/add", {
            pageTitle: `Добавление Юридического Лица | ${res.locals.userCompany.title.short}`,
            path: "legal-entities/add",           
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                shortName: req.body.shortName,
                fullName: req.body.fullName,
                city: req.body.city,
                street: req.body.street,
                house: req.body.house,
                phone: req.body.phone,
                email: req.body.email,
                isDealer: isDealer,
            },
        });
    }

    try {
        const legalEntity = new LegalEntity({
            shortName: req.body.shortName,
            fullName: req.body.fullName,
            city: req.body.city,
            street: req.body.street,
            house: req.body.house,
            phone: req.body.phone,
            email: req.body.email,
            isDealer: isDealer,
            createdBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
            updatedBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
        });
        await legalEntity.save();
        return res.redirect("/legal-entities");
    } catch (err) {
        throw err;
    }
};

exports.getEditLegalEntity = async (req, res, next) => {
    const id = req.params.id;
    try {
        const legalEntity = await LegalEntity.findById(id);
        if (!legalEntity) {
            return res.redirect("/legal-entities");
        }
        return res.render("legalEntity/edit", {
            pageTitle: `Изменение Юридического Лица | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            legalEntity: legalEntity,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                shortName: null,
                fullName: null,
                city: null,
                street: null,
                house: null,
                phone: null,
                email: null,
                isDealer: false,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEditLegalEntity = async (req, res, next) => {
    const errors = validationResult(req);
    const isDealer = req.body.isDealer ? true : false;
    if (!errors.isEmpty()) {
        try {
            const legalEntity = await LegalEntity.findById(req.body.id);
            if (!legalEntity) {
                return res.redirect("/legal-entities");
            }
            return res.status(422).render("legalEntity/edit", {
                pageTitle: `Изменение Юридического Лица | ${res.locals.userCompany.title.short}`,
                path: "/edit",
                legalEntity: legalEntity,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    shortName: req.body.shortName,
                    fullName: req.body.fullName,
                    city: req.body.city,
                    street: req.body.street,
                    house: req.body.house,
                    phone: req.body.phone,
                    email: req.body.email,
                    isDealer: isDealer,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const legalEntity = await LegalEntity.findById(req.body.id);
        legalEntity.shortName = req.body.shortName;
        legalEntity.fullName = req.body.fullName;
        legalEntity.city = req.body.city;
        legalEntity.street = req.body.street;
        legalEntity.house = req.body.house;
        legalEntity.phone = req.body.phone;
        legalEntity.email = req.body.email;
        legalEntity.isDealer = isDealer;

        legalEntity.updatedBy.id = req.session.user;
        legalEntity.updatedBy.name.first = req.session.user.name.first;
        legalEntity.updatedBy.name.last = req.session.user.name.last;

        await legalEntity.save();
        return res.redirect("/legal-entities");
    } catch (err) {
        throw err;
    }
};

exports.postDeleteLegalEntity = async (req, res, next) => {
    const id = req.body.id;
    try {
        await LegalEntity.deleteOne({
            _id: id,
        });
        return res.redirect("/legal-entities");
    } catch (err) {
        throw err;
    }
};

exports.getAddContact = async (req, res, next) => {
    try {
        const legalEntity = await LegalEntity.findById(req.params.id);
        return res.render("legalEntity/add_сontact", {
            pageTitle: `${legalEntity.shortName} | ${res.locals.userCompany.title.short}`,
            path: "/legal-entities/add-contact",
            legalEntity: legalEntity,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                firstName: "",
                middleName: "",
                lastName: "",
                phone: "",
                email: "",
                position: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAddContact = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const legalEntity = await LegalEntity.findById(req.body.id);

            return res.status(422).render("legalEntity/add_сontact", {
                pageTitle: `${legalEntity.shortName} | ${res.locals.userCompany.title.short}`,
                path: "/legal-entities/add-contact",
                legalEntity: legalEntity,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    firstName: req.body.firstName,
                    middleName: req.body.middleName,
                    lastName: req.body.lastName,
                    phone: req.body.phone,
                    email: req.body.email,
                    position: req.body.position,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const legalEntity = await LegalEntity.findById(req.body.id);
        const contact = {
            name: {
                first: req.body.firstName,
                middle: req.body.middleName,
                last: req.body.lastName,
            },
            contacts: {
                phone: req.body.phone,
                email: req.body.email,
            },
            position: req.body.position,
        };

        await legalEntity.contactList.push(contact);

        await legalEntity.save();
        return res.redirect(`/legal-entities/view/${req.body.id}`);
    } catch (err) {
        throw err;
    }
};

exports.getEditContact = async (req, res, next) => {
    try {
        const legalEntity = await LegalEntity.findById(req.params.id);
        const contact = await legalEntity.contactList[req.params.contactIndex];

        return res.render("legalEntity/edit_сontact", {
            pageTitle: `${legalEntity.shortName} | ${res.locals.userCompany.title.short}`,
            path: "/legal-entities/edit-contact",
            legalEntity: legalEntity,
            contact: contact,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                firstName: "",
                middleName: "",
                lastName: "",
                phone: "",
                email: "",
                position: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEditContact = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const legalEntity = await LegalEntity.findById(req.body.id);
            const contact = await legalEntity.contactList[
                req.body.contactIndex
            ];

            return res.status(422).render("legalEntity/edit_сontact", {
                pageTitle: `${legalEntity.shortName} | ${res.locals.userCompany.title.short}`,
                path: "/legal-entities/edit-contact",
                legalEntity: legalEntity,
                contact: contact,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    firstName: req.body.firstName,
                    middleName: req.body.middleName,
                    lastName: req.body.lastName,
                    phone: req.body.phone,
                    email: req.body.email,
                    position: req.body.position,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const legalEntity = await LegalEntity.findById(req.body.id);
        legalEntity.contactList[req.body.contactIndex] = {
            name: {
                first: req.body.firstName,
                middle: req.body.middleName,
                last: req.body.lastName,
            },
            contacts: {
                phone: req.body.phone,
                email: req.body.email,
            },
            position: req.body.position,
        };

        await legalEntity.save();
        return res.redirect(`/legal-entities/view/${req.body.id}`);
    } catch (err) {
        throw err;
    }
};

exports.postDeleteContact = async (req, res, next) => {    
    const legalEntity = await LegalEntity.findById(req.body.id);
    const contactIndex = req.body.contactIndex;
    try {
        await legalEntity.contactList.splice(contactIndex, 1);
        await legalEntity.save();
        return res.redirect(`/legal-entities/view/${req.body.id}`);
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoCompleteLegalEntity = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const legalEntitiesFilter = LegalEntity.find(
        {
            $or: [
                {
                    shortName: regex,
                },
                {
                    fullName: regex,
                },
            ],
        },
        {
            shortName: 1,
            fullName: 1,
        }
    ).limit(10);
    legalEntitiesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((legalEntity) => {
                    let obj = {
                        id: legalEntity._id,
                        label: `${legalEntity.shortName} (${legalEntity.fullName})`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
