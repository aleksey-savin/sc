const Manufacturer = require("../models/manufacturer");

const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const manufacturers = await Manufacturer.find();
        return res.render("manufacturer/index", {
            manufacturers: manufacturers,
            pageTitle: `Все производители | ${res.locals.userCompany.title.short}`,
            path: "/manufacturers",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("manufacturer/add", {
        pageTitle: `Добавить Производителя | ${res.locals.userCompany.title.short}`,
        path: "manufacturers/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: null,
            code: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const isOfficiallyAuthorized = req.body.isOfficiallyAuthorized
        ? true
        : false;
    const warrantyExtensionProcedure = req.body.warrantyExtensionProcedure
        ? true
        : false;
    const nonRepairableActPrint = req.body.nonRepairableActPrint ? true : false;
    const trackSpareParts = req.body.trackSpareParts ? true : false;
    const manufacturer = new Manufacturer({
        title: req.body.title,
        isOfficiallyAuthorized: isOfficiallyAuthorized,
        nonRepairableActPrint: nonRepairableActPrint,
        warrantyExtensionProcedure: warrantyExtensionProcedure,
        trackSpareParts: trackSpareParts,
    });
    try {
        await manufacturer.save();
        return res.redirect("/manufacturers");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const manufacturerId = req.params.manufacturerId;
    try {
        const manufacturer = await Manufacturer.findById(manufacturerId);
        if (!manufacturer) {
            return res.redirect("/manufacturers");
        }
        return res.render("manufacturer/edit", {
            pageTitle: `Изменить Производителя | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            manufacturer: manufacturer,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const manufacturerId = req.body.manufacturerId;
    const title = req.body.title;
    const isOfficiallyAuthorized = req.body.isOfficiallyAuthorized
        ? true
        : false;
    const warrantyExtensionProcedure = req.body.warrantyExtensionProcedure
        ? true
        : false;
    const nonRepairableActPrint = req.body.nonRepairableActPrint ? true : false;
    const trackSpareParts = req.body.trackSpareParts ? true : false;
    try {
        const manufacturer = await Manufacturer.findById(manufacturerId);
        manufacturer.title = title;
        manufacturer.isOfficiallyAuthorized = isOfficiallyAuthorized;
        manufacturer.nonRepairableActPrint = nonRepairableActPrint;
        manufacturer.warrantyExtensionProcedure = warrantyExtensionProcedure;
        manufacturer.trackSpareParts = trackSpareParts;
        await manufacturer.save();
        return res.redirect("/manufacturers");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const manufacturerId = req.body.manufacturerId;
    try {
        await Manufacturer.deleteOne({ _id: manufacturerId });
        return res.redirect("/manufacturers");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const manufacturersFilter = Manufacturer.find(
        { title: regex },
        { title: 1, isOfficiallyAuthorized: 1 }
    )
        //    .sort({"updated_at": -1})
        //    .sort({created_at: -1})
        .limit(10);
    manufacturersFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((manufacturer) => {
                    let obj = {
                        label: manufacturer.title,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
