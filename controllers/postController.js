const Post = require("../models/post");

const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

const dateOptions = {
    day: "numeric",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
};

const renderParams = (pageTitle, req) => {
    return {
        pageTitle: pageTitle,
        dateOptions: dateOptions,
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: req.body.title || null,
            body: req.body.body || null,
            category: req.body.category || null,
        },
    };
};

exports.getIndex = async (req, res, next) => {
    const categories = Post.schema.path("category").enumValues;
    try {
        const posts = await Post.find().sort({createdAt: 'desc'});
        return res.render("post/index", {
            posts: posts,
            dateOptions: dateOptions,
            categories: categories,
            pageTitle: `Новости | ${res.locals.userCompany.title.short}`,
            path: "/news",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = async (req, res, next) => {
    console.log("Get Add Success!");
    res.redirect("/");
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);

    const post = new Post({
        title: req.body.title,
        body: req.body.body,
        category: req.body.category,
        createdBy: {
            id: req.session.user,
            name: {
                last: req.session.user.name.last,
                first: req.session.user.name.first,
            },
        },
    });

    if (!errors.isEmpty()) {
        try {
            return res
                .status(422)
                .render(
                    "deviceType/add",
                    renderParams(
                        `Добавить тип устройства | ${res.locals.userCompany.title.short}`,
                        req
                    )
                );
        } catch (err) {
            throw err;
        }
    }

    try {
        await post.save();
        return res.redirect("/");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    console.log("Get Edit Success!");
    res.redirect("/");
};

exports.postEdit = async (req, res, next) => {
    console.log("Post Edit Success!");
    res.redirect("/");
};

exports.postDelete = async (req, res, next) => {
    console.log("Post Delete Success!");
    res.redirect("/");
};
