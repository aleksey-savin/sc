const Device = require("../models/device");
const Order = require("../models/order");
const Customer = require("../models/customer");
const LegalEntity = require("../models/legalEntity");
const Manufacturer = require("../models/manufacturer");
const DeviceType = require("../models/deviceType");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

const dateOptions = { year: "numeric", month: "2-digit", day: "numeric" };

const deviceRenderParams = async (pageTitle, req) => {
    // flash messages, validation etc.
    let errorMessage = req.flash("error");
    let successMessage = req.flash("success");
    let infoMessage = req.flash("info");
    errorMessage.length > 0
        ? (errorMessage = errorMessage[0])
        : (errorMessage = null);
    successMessage.length > 0
        ? (successMessage = successMessage[0])
        : (successMessage = null);
    infoMessage.length > 0
        ? (infoMessage = infoMessage[0])
        : (infoMessage = null);

    const errors = validationResult(req);

    const device = req.params.deviceId
        ? await Device.findById(req.params.deviceId)
        : await Device.findById(req.body.deviceId);

    const manufacturers = await Manufacturer.find({}, null, {
        sort: { title: 1 },
    });
    const deviceTypes = await DeviceType.find({}, null, {
        sort: { name: 1 },
    });

    const customer = req.query.customer
        ? await Customer.findById(req.query.customer)
        : "";

    const states = req.params.deviceId
        ? Device.schema.path("state").enumValues
        : ["У Клиента", "На складе СЦ"];

    return {
        pageTitle: pageTitle,
        device: device,
        customer: customer,
        manufacturers: manufacturers,
        deviceTypes: deviceTypes,
        states: states,
        dateOptions: dateOptions,
        errorMessage: errors.array()[0]?.msg || "",
        successMessage: successMessage,
        infoMessage: infoMessage,
        oldInput: {
            islegalEntityOwner:
                req.body.ownerRadios === "individualOwner" ? false : true,
            individualOwner: req.body.individualOwner || "",
            individualOwnerId: req.body.individualOwnerId || "",
            legalEntityOwner: req.body.legalEntityOwner || "",
            legalEntityOwnerId: req.body.legalEntityOwnerId || "",
            manufacturer: req.body.manufacturer || "",
            type: req.body.type || "",
            model: req.body.model || "",
            sn: req.body.sn || "",
            itemCode: req.body.itemCode || "",
            imei1: req.body.imei1 || "",
            imei2: req.body.imei2 || "",
            dateOfSale: req.body.dateOfSale || "",
            state: req.body.state || "",
            osVersion: req.body.osVersion || "",
            orderNo: req.body.orderNo || "",
        },
    };
};

exports.getIndex = async (req, res, next) => {
    try {
        const devices = await Device.find({
            $and: [
                { state: { $ne: "У Клиента" } },
                {
                    state: {
                        $ne: "Отправлено производителю или сервис-провайдеру",
                    },
                },
                { state: { $ne: "Клиент не забирает" } },
                { state: { $ne: "Подменный фонд - устройство на складе" } },
                { state: { $ne: "Подменный фонд - устройство у Клиента" } },
            ],
        });
        return res.render("device/index", {
            devices: devices,
            pageTitle: `Все устройства | ${res.locals.userCompany.title.short}`,
            dateOptions: dateOptions,
        });
    } catch (err) {
        throw err;
    }
};

exports.getViewDevice = async (req, res, next) => {
    try {
        // flash messages, validation etc.
        let errorMessage = req.flash("error");
        let successMessage = req.flash("success");
        let infoMessage = req.flash("info");
        errorMessage.length > 0
            ? (errorMessage = errorMessage[0])
            : (errorMessage = null);
        successMessage.length > 0
            ? (successMessage = successMessage[0])
            : (successMessage = null);
        infoMessage.length > 0
            ? (infoMessage = infoMessage[0])
            : (infoMessage = null);
        const device = await Device.findById(req.params.deviceId);
        const orders = await Order.find({ "device._id": device._id });
        return res.render("device/view", {
            device: device,
            orders: orders,
            pageTitle: `Просмотр устройства | ${res.locals.userCompany.title.short}`,
            path: "/devices/view",
            dateOptions: dateOptions,
            errorMessage: errorMessage,
            successMessage: successMessage,
            infoMessage: infoMessage,
        });
    } catch (err) {
        throw err;
    }
};

exports.getAddDevice = async (req, res, next) => {
    try {
        return res.render(
            "device/add",
            await deviceRenderParams(
                `Добавить Устройство | ${res.locals.userCompany.title.short}`,
                req
            )
        );
    } catch (err) {
        throw err;
    }
};

exports.postAddDevice = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            return res
                .status(422)
                .render(
                    "device/add",
                    await deviceRenderParams(
                        `Добавить Устройство | ${res.locals.userCompany.title.short}`,
                        req
                    )
                );
        } catch (err) {
            throw err;
        }
    }

    try {
        const owner =
            req.body.ownerRadios === "individualOwner"
                ? req.body.individualOwnerId
                    ? await Customer.findOne({
                          _id: req.body.individualOwnerId,
                      }).populate("_id")
                    : await Customer.findOne({
                          _id: req.body.individualOwnerId,
                      }).populate("_id")
                : req.body.legalEntityOwnerId
                ? await LegalEntity.findOne({
                      _id: req.body.legalEntityOwnerId,
                  }).populate("_id")
                : await LegalEntity.findOne({
                      _id: req.body.legalEntityOwnerId,
                  }).populate("_id");

        const manufacturer = await Manufacturer.findOne({
            title: req.body.manufacturer,
        }).populate("_id");
        const type = await DeviceType.findOne({
            name: req.body.type,
        }).populate("_id");

        const ownerData =
            req.body.ownerRadios === "individualOwner"
                ? {
                      individual: {
                          _id: owner._id,
                          firstName: owner.name.first,
                          lastName: owner.name.last,
                          middleName: owner.name.middle,
                          phone: owner.contacts.phone,
                          email: owner.contacts.email,
                      },
                  }
                : {
                      legalEntity: {
                          _id: owner._id,
                          shortName: owner.shortName,
                          fullName: owner.fullName,
                          phone: owner.phone,
                          email: owner.email,
                      },
                  };

        const device = new Device({
            owner: ownerData,
            manufacturer: manufacturer.title,
            type: type.name,
            model: req.body.model,
            itemCode: req.body.itemCode,
            sn: req.body.sn,
            imei1: req.body.imei1,
            imei2: req.body.imei2,
            osVersion: req.body.osVersion || "",
            dateOfSale: req.body.dateOfSale,
            state: req.body.state,
            orderNo: req.body.orderNo,
        });
        await device.save();
        req.body.continue === "continueView"
            ? res.redirect(`/devices/view/${device._id}`)
            : req.body.ownerRadios === "individualOwner"
            ? res.redirect(
                  `/orders/add?customer=${device.owner.individual._id._id}&device=${device._id}`
              )
            : res.redirect(
                  `/orders/add?customer=${device.owner.legalEntity._id._id}&device=${device._id}`
              );
    } catch (err) {
        throw err;
    }
};

exports.getEditDevice = async (req, res, next) => {
    try {
        return res.render(
            "device/edit",
            await deviceRenderParams(
                `Изменить Устройство | ${res.locals.userCompany.title.short}`,
                req
            )
        );
    } catch (err) {
        throw err;
    }
};

exports.postEditDevice = async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        try {
            return res
                .status(422)
                .render(
                    "device/edit",
                    await deviceRenderParams(
                        `Изменить устройство | ${res.locals.userCompany.title.short}`,
                        req
                    )
                );
        } catch (err) {
            throw err;
        }
    }

    try {
        const owner =
            req.body.ownerRadios === "individualOwner"
                ? await Customer.findOne({
                      _id: req.body.individualOwnerId,
                  })
                : await LegalEntity.findOne({
                      _id: req.body.legalEntityOwnerId,
                  });

        const manufacturer = await Manufacturer.findOne({
            title: req.body.manufacturer,
        });
        const type = await DeviceType.findOne({
            name: req.body.type,
        });
        const orderNo =
            req.body.state === "Подменный фонд - устройство у Клиента"
                ? req.body.orderNo
                : null;

        const ownerData =
            req.body.ownerRadios === "individualOwner"
                ? {
                      individual: {
                          _id: owner._id,
                          firstName: owner.name.first,
                          lastName: owner.name.last,
                          middleName: owner.name.middle,
                          phone: owner.contacts.phone,
                          email: owner.contacts.email,
                      },
                  }
                : {
                      legalEntity: {
                          _id: owner._id,
                          shortName: owner.shortName,
                          fullName: owner.fullName,
                          phone: owner.phone,
                          email: owner.email,
                      },
                  };

        const device = await Device.findById(req.body.deviceId);

        device.owner = ownerData;
        device.manufacturer = manufacturer.title;
        device.type = type.name;
        device.model = req.body.model;
        device.itemCode = req.body.itemCode;
        device.sn = req.body.sn;
        device.imei1 = req.body.imei1;
        device.imei2 = req.body.imei2;
        device.osVersion = req.body.osVersion || "";
        device.dateOfSale = req.body.dateOfSale;
        device.state = req.body.state;
        device.orderNo = orderNo;

        await device.save();

        // обновляем все текущие ремонты
        const orders = await Order.find({ "device._id": device._id });
        for (let order of orders) {
            order.device.owner = device.owner;
            order.device.manufacturer = device.manufacturer;
            order.device.type = device.type;
            order.device.manufacturer = device.manufacturer;
            order.device.model = device.model;
            order.device.sn = device.sn;
            order.device.itemCode = device.itemCode;
            order.device.imei1 = device.imei1;
            order.device.imei2 = device.imei2;
            order.device.osVersion = device.osVersion;
            order.device.state = device.state;
            await order.save();
        }

        return res.redirect(`/devices/view/${device._id}`);
    } catch (err) {
        throw err;
    }
};

exports.postDeleteDevice = async (req, res, next) => {
    try {
        await Device.deleteOne({ _id: req.body.deviceId });
        return res.redirect("/devices");
    } catch (err) {
        throw err;
    }
};

exports.getReplacementFund = async (req, res, next) => {
    try {
        const devices = await Device.find({
            $or: [
                { state: "Подменный фонд - устройство на складе" },
                { state: "Подменный фонд - устройство у Клиента" },
            ],
        });
        return res.render("device/replacement_fund", {
            devices: devices,
            pageTitle: `Подменный фонд | ${res.locals.userCompany.title.short}`,
            path: "/replacement-fund",
            dateOptions: dateOptions,
        });
    } catch (err) {
        throw err;
    }
};

exports.getPrintContractReplacementFund = async (req, res, next) => {
    try {
        const device = await Device.findById(req.params.deviceId);
        return res.render("print/templates/contract_replacement_fund", {
            device: device,
            pageTitle: `Печать Расписки Подменный Аппарат | ${res.locals.userCompany.title.short}`,
            path: "/devices/print-contract-replacement-fund",
            dateOptions: dateOptions,
        });
    } catch (err) {
        throw err;
    }
};

exports.postPrintContractReplacementFund = async (req, res, next) => {
    try {
        const device = await Device.findById(req.body.deviceIdForm);
        const setGivenToClient = req.body.setGivenToClient ? true : false;

        if (setGivenToClient) {
            device.state = "Подменный фонд - устройство у Клиента";
            device.orderNo = req.body.orderNo;
            device.save();
        }

        res.render(`print/templates/contract_replacement_fund`, {
            pageTitle: `Печать Расписки | ${res.locals.userCompany.title.short}`,
            path: "/devices/print-contract-replacement-fund/",
            device: device,
            lastName: req.body.lastName,
            firstName: req.body.firstName,
            middleName: req.body.middleName,
            passportSeries: req.body.passportSeries,
            passportNo: req.body.passportNo,
            givenBy: req.body.givenBy,
            passportDate: req.body.passportDate,
            registeredAt: req.body.registeredAt,
            livesAt: req.body.livesAt,
            orderNo: req.body.orderNo,
        });
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoCompleteDevice = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const devicesFilter = Device.find(
        { $or: [{ sn: regex }, { imei1: regex }, { imei2: regex }] },
        { type: 1, manufacturer: 1, model: 1, sn: 1 }
    ).limit(10);
    devicesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((device) => {
                    let obj = {
                        id: device._id,
                        label: `${device.type} ${device.manufacturer} ${device.model}, sn: ${device.sn}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};

exports.postAddLegalEntity = async (req, res, next) => {
    const errors = validationResult(req);
    const manufacturers = await Manufacturer.find();
    const deviceTypes = await DeviceType.find();
    if (!errors.isEmpty()) {
        const states = Device.schema.path("state").enumValues;
        return res.status(422).render("device/add", {
            pageTitle: `Добавить Устройство | ${res.locals.userCompany.title.short}`,
            path: "devices/add",
            states: states,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            dateOptions: dateOptions,
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                islegalEntityOwner: true,
                individualOwner: null,
                individualOwnerId: null,
                legalEntityOwner: null,
                legalEntityOwnerId: null,
                manufacturer: null,
                type: null,
                model: null,
                sn: null,
                itemCode: null,
                imei1: null,
                dateOfSale: null,
                state: null,
                name: {
                    first: null,
                    last: null,
                    middle: null,
                },
                address: {
                    city: null,
                    street: null,
                    house: null,
                },
                contacts: {
                    phone: null,
                    email: null,
                },
                shortName: req.body.shortName,
                fullName: req.body.fullName,
                city: req.body.city,
                street: req.body.street,
                house: req.body.house,
                phone: req.body.phone,
                email: req.body.email,
            },
        });
    }

    const isDealer = req.body.isDealer ? true : false;
    try {
        const legalEntity = new LegalEntity({
            shortName: req.body.shortName,
            fullName: req.body.fullName,
            city: req.body.city,
            street: req.body.street,
            house: req.body.house,
            phone: req.body.phone,
            email: req.body.email,
            isDealer: isDealer,
            createdBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
            updatedBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
        });
        await legalEntity.save();

        const customer = req.query.customer
        ? await Customer.findById(req.query.customer)
        : "";

        const states = Device.schema.path("state").enumValues;
        return res.render("device/add", {
            pageTitle: `Добавить Устройство | ${res.locals.userCompany.title.short}`,
            path: "devices/add",
            states: states,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            customer: customer,
            dateOptions: dateOptions,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                islegalEntityOwner: true,
                individualOwner: null,
                individualOwnerId: null,
                legalEntityOwner: `${legalEntity.shortName} (${legalEntity.fullName})`,
                legalEntityOwnerId: legalEntity._id,
                manufacturer: null,
                type: null,
                model: null,
                sn: null,
                itemCode: null,
                imei1: null,
                dateOfSale: null,
                state: null,
                name: {
                    first: null,
                    last: null,
                    middle: null,
                },
                address: {
                    city: null,
                    street: null,
                    house: null,
                },
                contacts: {
                    phone: null,
                    email: null,
                },
                shortName: null,
                fullName: null,
                city: null,
                street: null,
                house: null,
                phone: null,
                email: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAddCustomer = async (req, res, next) => {
    const errors = validationResult(req);
    const manufacturers = await Manufacturer.find();
    const deviceTypes = await DeviceType.find();
    const customer = req.query.customer
        ? await Customer.findById(req.query.customer)
        : "";
    if (!errors.isEmpty()) {
        const states = Device.schema.path("state").enumValues;

        return res.status(422).render("device/add", {
            pageTitle: `Добавить Устройство | ${res.locals.userCompany.title.short}`,
            path: "devices/add",
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            states: states,
            customer: customer,
            dateOptions: dateOptions,
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                islegalEntityOwner: false,
                individualOwner: null,
                individualOwnerId: null,
                legalEntityOwner: null,
                legalEntityOwnerId: null,
                manufacturer: null,
                type: null,
                model: null,
                sn: null,
                itemCode: null,
                imei1: null,
                dateOfSale: null,
                state: null,
                name: {
                    first: req.body.firstName,
                    last: req.body.lastName,
                    middle: req.body.middleName,
                },
                address: {
                    city: req.body.city,
                    street: req.body.street,
                    house: req.body.house,
                },
                contacts: {
                    phone: req.body.phone,
                    email: req.body.email,
                },
                shortName: null,
                fullName: null,
                city: null,
                street: null,
                house: null,
                phone: null,
                email: null,
            },
        });
    }

    const silentMode = req.body.silentMode ? true : false;
    try {
        const customer = new Customer({
            name: {
                first: req.body.firstName,
                middle: req.body.middleName,
                last: req.body.lastName,
            },
            address: {
                city: req.body.city,
                street: req.body.street,
                house: req.body.house,
            },
            contacts: {
                phone: req.body.emptyPhone
                    ? "номер отсутствует"
                    : req.body.phone,
                email: req.body.email,
            },
            silentMode: silentMode,
        });
        await customer.save();        

        const states = Device.schema.path("state").enumValues;
        return res.render("device/add", {
            pageTitle: `Добавить Устройство | ${res.locals.userCompany.title.short}`,
            path: "devices/add",
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            states: states,
            customer: customer,
            dateOptions: dateOptions,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                islegalEntityOwner: false,
                individualOwner: `${customer.name.last} ${customer.name.first} ${customer.name.middle} (${customer.contacts.phone})`,
                individualOwnerId: customer._id,
                legalEntityOwner: null,
                legalEntityOwnerId: null,
                manufacturer: null,
                type: null,
                model: null,
                sn: null,
                itemCode: null,
                imei1: null,
                dateOfSale: null,
                state: null,
                name: {
                    first: null,
                    last: null,
                    middle: null,
                },
                address: {
                    city: null,
                    street: null,
                    house: null,
                },
                contacts: {
                    phone: null,
                    email: null,
                },
                shortName: null,
                fullName: null,
                city: null,
                street: null,
                house: null,
                phone: null,
                email: null,
            },
        });
    } catch (err) {
        throw err;
    }
};
