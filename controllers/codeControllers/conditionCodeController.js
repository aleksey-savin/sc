const ConditionCode = require("../../models/codes/conditionCode");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const conditionCodes = await ConditionCode.find();
        return res.render("code/conditionCode/index", {
            conditionCodes: conditionCodes,
            pageTitle: `Коды Условия | ${res.locals.userCompany.title.short}`,
            path: "/condition-codes",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("code/conditionCode/add", {
        pageTitle: `Добавить Код Условия | ${res.locals.userCompany.title.short}`,
        path: "/condition-codes/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: null,
            code: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("code/conditionCode/add", {
            pageTitle: `Добавить Код Условия | ${res.locals.userCompany.title.short}`,
            path: "/condition-codes/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: req.body.title,
                code: req.body.code,
            },
        });
    }

    try {
        const conditionCode = new ConditionCode({
            code: req.body.code,
            title: req.body.title,
        });
        await conditionCode.save();
        return res.redirect("/condition-codes");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    try {
        const conditionCode = await ConditionCode.findById(
            req.params.conditionCodeId
        );
        if (!conditionCode) {
            return res.redirect("/condition-codes");
        }
        return res.render("code/conditionCode/edit", {
            pageTitle: `Изменить Код Условия | ${res.locals.userCompany.title.short}`,
            path: "/condition/codes/edit",
            conditionCode: conditionCode,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                code: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const conditionCode = await ConditionCode.findById(
                req.body.conditionCodeId
            );

            if (!conditionCode) {
                return res.redirect("/condition-codes");
            }

            return res.status(422).render("code/conditionCode/edit", {
                pageTitle: `Изменить Код Условия | ${res.locals.userCompany.title.short}`,
                path: "/condition/codes/edit",
                conditionCode: conditionCode,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    code: req.body.code,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const conditionCode = await ConditionCode.findById(
            req.body.conditionCodeId
        );
        conditionCode.code = req.body.code;
        conditionCode.title = req.body.title;
        await conditionCode.save();
        return res.redirect("/condition-codes");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    try {
        await ConditionCode.deleteOne({ _id: req.body.conditionCodeId });
        return res.redirect("/condition-codes");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const conditionCodesFilter = ConditionCode.find(
        { title: regex },
        { title: 1 }
    ).limit(10);
    conditionCodesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((conditionCode) => {
                    let obj = {
                        label: `${conditionCode.title}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
