const SymptomCode = require("../../models/codes/symptomCode");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const symptomCodes = await SymptomCode.find();
        return res.render("code/symptomCode/index", {
            symptomCodes: symptomCodes,
            pageTitle: `Коды Симптома | ${res.locals.userCompany.title.short}`,
            path: "/symptom-codes",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("code/symptomCode/add", {
        pageTitle: `Добавить Код Симптома | ${res.locals.userCompany.title.short}`,
        path: "/symptom-codes/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: null,
            code: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("code/symptomCode/add", {
            pageTitle: `Добавить Код Симптома | ${res.locals.userCompany.title.short}`,
            path: "/symptom-codes/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: req.body.title,
                code: req.body.code,
            },
        });
    }

    try {
        const symptomCode = new SymptomCode({
            code: req.body.code,
            title: req.body.title,
        });

        await symptomCode.save();
        return res.redirect("/symptom-codes");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    try {
        const symptomCode = await SymptomCode.findById(
            req.params.symptomCodeId
        );
        if (!symptomCode) {
            return res.redirect("/symptom-codes");
        }
        return res.render("code/symptomCode/edit", {
            pageTitle: `Изменить Код Симптома | ${res.locals.userCompany.title.short}`,
            path: "/symptom/codes/edit",
            symptomCode: symptomCode,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                code: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const symptomCode = await SymptomCode.findById(
                req.params.symptomCodeId
            );
            if (!symptomCode) {
                return res.redirect("/symptom-codes");
            }

            return res.status(422).render("code/symptomCode/edit", {
                pageTitle: `Изменить Код Симптома | ${res.locals.userCompany.title.short}`,
                path: "/symptom/codes/edit",
                symptomCode: symptomCode,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    code: req.body.code,
                },
            });
        } catch (err) {
            throw err;
        }
    }
    try {
        const symptomCode = await SymptomCode.findById(req.body.symptomCodeId);
        symptomCode.code = req.body.code;
        symptomCode.title = req.body.title;
        await symptomCode.save();
        return res.redirect("/symptom-codes");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    try {
        await SymptomCode.deleteOne({ _id: req.body.symptomCodeId });
        return res.redirect("/symptom-codes");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const symptomCodesFilter = SymptomCode.find(
        { title: regex },
        { title: 1 }
    ).limit(10);
    symptomCodesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((symptomCode) => {
                    let obj = {
                        label: `${symptomCode.title}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
