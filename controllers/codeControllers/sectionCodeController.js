const SectionCode = require("../../models/codes/sectionCode");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const sectionCodes = await SectionCode.find();
        return res.render("code/sectionCode/index", {
            sectionCodes: sectionCodes,
            pageTitle: `Коды Секции | ${res.locals.userCompany.title.short}`,
            path: "/section-codes",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("code/sectionCode/add", {
        pageTitle: `Добавить Код Секции | ${res.locals.userCompany.title.short}`,
        path: "/section-codes/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: null,
            code: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("code/sectionCode/add", {
            pageTitle: `Добавить Код Секции | ${res.locals.userCompany.title.short}`,
            path: "/section-codes/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: req.body.title,
                code: req.body.code,
            },
        });
    }

    try {
        const sectionCode = new SectionCode({
            code: req.body.code,
            title: req.body.title,
        });

        await sectionCode.save();
        return res.redirect("/section-codes");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    try {
        const sectionCode = await SectionCode.findById(
            req.params.sectionCodeId
        );
        if (!sectionCode) {
            return res.redirect("/section-codes");
        }
        return res.render("code/sectionCode/edit", {
            pageTitle: `Изменить Код Секции | ${res.locals.userCompany.title.short}`,
            path: "/section-codes/edit",
            sectionCode: sectionCode,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                code: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const sectionCode = await SectionCode.findById(
                req.params.sectionCodeId
            );
            if (!sectionCode) {
                return res.redirect("/section-codes");
            }

            return res.status(422).render("code/sectionCode/edit", {
                pageTitle: `Изменить Код Секции | ${res.locals.userCompany.title.short}`,
                path: "/section-codes/edit",
                sectionCode: sectionCode,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    code: req.body.code,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const sectionCode = await SectionCode.findById(req.body.sectionCodeId);
        sectionCode.code = req.body.code;
        sectionCode.title = req.body.title;
        await sectionCode.save();
        return res.redirect("/section-codes");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    try {
        await SectionCode.deleteOne({ _id: req.body.sectionCodeId });
        return res.redirect("/section-codes");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const sectionCodesFilter = SectionCode.find(
        { title: regex },
        { title: 1 }
    ).limit(10);
    sectionCodesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((sectionCode) => {
                    let obj = {
                        label: `${sectionCode.title}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
