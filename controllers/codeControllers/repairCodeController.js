const RepairCode = require("../../models/codes/repairCode");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const repairCodes = await RepairCode.find();
        return res.render("code/repairCode/index", {
            repairCodes: repairCodes,
            pageTitle: `Коды Ремонта | ${res.locals.userCompany.title.short}`,
            path: "/repair-codes",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("code/repairCode/add", {
        pageTitle: `Добавить Код Ремонта | ${res.locals.userCompany.title.short}`,
        path: "/repair-codes/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: null,
            code: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("code/repairCode/add", {
            pageTitle: `Добавить Код Ремонта | ${res.locals.userCompany.title.short}`,
            path: "/repair-codes/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: req.body.title,
                code: req.body.code,
            },
        });
    }
    try {
        const repairCode = new RepairCode({
            code: req.body.code,
            title: req.body.title,
        });
        await repairCode.save();
        return res.redirect("/repair-codes");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    try {
        const repairCode = await RepairCode.findById(req.params.repairCodeId);
        if (!repairCode) {
            return res.redirect("/repair-codes");
        }
        return res.render("code/repairCode/edit", {
            pageTitle: `Изменить Код Ремонта | ${res.locals.userCompany.title.short}`,
            path: "/repair/codes/edit",
            repairCode: repairCode,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                code: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const repairCode = await RepairCode.findById(
                req.params.repairCodeId
            );
            if (!repairCode) {
                return res.redirect("/repair-codes");
            }

            return res.status(422).render("code/repairCode/edit", {
                pageTitle: `Изменить Код Ремонта | ${res.locals.userCompany.title.short}`,
                path: "/repair/codes/edit",
                repairCode: repairCode,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    code: req.body.code,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const repairCode = await RepairCode.findById(req.body.repairCodeId);
        repairCode.code = req.body.code;
        repairCode.title = req.body.title;
        await repairCode.save();
        return res.redirect("/repair-codes");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    try {
        await RepairCode.deleteOne({ _id: req.body.repairCodeId });
        return res.redirect("/repair-codes");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const repairCodesFilter = RepairCode.find(
        { title: regex },
        { title: 1 }
    ).limit(10);
    repairCodesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((repairCode) => {
                    let obj = {
                        label: `${repairCode.title}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
