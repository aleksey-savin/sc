const DefectCode = require("../../models/codes/defectCode");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const defectCodes = await DefectCode.find();
        return res.render("code/defectCode/index", {
            defectCodes: defectCodes,
            pageTitle: `Коды Дефекта | ${res.locals.userCompany.title.short}`,
            path: "/defect-codes",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = (req, res, next) => {
    res.render("code/defectCode/add", {
        pageTitle: `Добавить Код Дефекта | ${res.locals.userCompany.title.short}`,
        path: "/defect-codes/add",
        errorMessage: alertMessage(req.flash("error")),
        successMessage: alertMessage(req.flash("success")),
        infoMessage: alertMessage(req.flash("info")),
        oldInput: {
            title: null,
            code: null,
        },
    });
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render("code/defectCode/add", {
            pageTitle: `Добавить Код Дефекта | ${res.locals.userCompany.title.short}`,
            path: "/defect-codes/add",
            errorMessage: errors.array()[0].msg,
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: req.body.title,
                code: req.body.code,
            },
        });
    }

    try {
        const defectCode = new DefectCode({
            code: req.body.code,
            title: req.body.title,
        });

        await defectCode.save();
        return res.redirect("/defect-codes");
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    try {
        const defectCode = await DefectCode.findById(req.params.defectCodeId);
        if (!defectCode) {
            return res.redirect("/defect-codes");
        }
        return res.render("code/defectCode/edit", {
            pageTitle: `Изменить Код Дефекта | ${res.locals.userCompany.title.short}`,
            path: "/defect/codes/edit",
            defectCode: defectCode,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                code: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const defectCode = await DefectCode.findById(
                req.params.defectCodeId
            );

            if (!defectCode) {
                return res.redirect("/defect-codes");
            }

            return res.status(422).render("code/defectCode/edit", {
                pageTitle: `Изменить Код Дефекта | ${res.locals.userCompany.title.short}`,
                path: "/defect/codes/edit",
                defectCode: defectCode,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    code: req.body.code,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const defectCode = await DefectCode.findById(req.body.defectCodeId);
        defectCode.code = req.body.code;
        defectCode.title = req.body.title;
        await defectCode.save();
        return res.redirect("/defect-codes");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    try {
        await DefectCode.deleteOne({ _id: req.body.defectCodeId });
        return res.redirect("/defect-codes");
    } catch (err) {
        throw err;
    }
};

// код какого-то типа с ютубчика, переписать по-человечьи
exports.getAutoComplete = (req, res, next) => {
    const regex = new RegExp(req.query["term"], "i");
    const defectCodesFilter = DefectCode.find(
        { title: regex },
        { title: 1 }
    ).limit(10);
    defectCodesFilter.exec((err, data) => {
        let result = [];
        if (!err) {
            if (data) {
                data.forEach((defectCode) => {
                    let obj = {
                        label: `${defectCode.title}`,
                    };
                    result.push(obj);
                });
            }
            res.jsonp(result);
        }
    });
};
