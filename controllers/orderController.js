const fs = require("fs");
const path = require("path");
const currencyFormatter = require("currency-formatter");

//Mailgun API
const apiKey = "key-e0dbc978a7a302630cc678e1ed5c531d";
const domain = "helpdesk.f1lab.ru";
const mailgun = require("mailgun-js")({ domain: domain, apiKey: apiKey });

//SMSRU API
const { SMSRu } = require("node-sms-ru");

const Order = require("../models/order");
const User = require("../models/user");
const Customer = require("../models/customer");
const LegalEntity = require("../models/legalEntity");
const Device = require("../models/device");
const OrderType = require("../models/orderType");
const ConditionCode = require("../models/codes/conditionCode");
const SymptomCode = require("../models/codes/symptomCode");
const SectionCode = require("../models/codes/sectionCode");
const DefectCode = require("../models/codes/defectCode");
const RepairCode = require("../models/codes/repairCode");
const MyCompany = require("../models/myCompany");
const Manufacturer = require("../models/manufacturer");
const DeviceType = require("../models/deviceType");
const Comment = require("../models/comment");

const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

// Rocket Chat
const RocketChatApi = require("rocketchat-api");
const rocketChatClient = new RocketChatApi(
    "https",
    "rc.f1-service.pro",
    443,
    "f1lab",
    "z123b15!",
    (err, result) => {}
);

const {
    timePassedVerbose,
    timePassedArray,
} = require("../middleware/timePassedCalc");
const myCompany = require("../models/myCompany");

const dateOptions = {
    day: "numeric",
    month: "2-digit",
    year: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
};

exports.getIndex = async (req, res, next) => {
    try {
        const orderTypes = await OrderType.find();
        const manufacturers = await Manufacturer.find();
        const deviceTypes = await DeviceType.find();
        const users = await User.find({ role: { $ne: "User" } });
        const orderStates = Order.schema.path("state").enumValues;
        const deviceStates = Device.schema.path("state").enumValues;

        const orders = await Order.find({
            $and: [
                { state: { $ne: "Устройство выдано" } },
                { state: { $ne: "Выдан акт неремонтопригодности" } },
            ],
        });

        return res.render("order/index", {
            orderFilterId: null,
            orderState: req.body.orderState || [],
            orderType: req.body.orderType || [],
            orderNo: req.body.orderNo,
            providerOrderId: req.body.providerOrderId,
            orderCreatedAt: req.body.orderCreatedAt,
            orderCreatedAtStart: req.body.orderCreatedAtStart,
            orderCreatedAtEnd: req.body.orderCreatedAtEnd,
            orderDiagnosedAt: req.body.orderDiagnosedAt,
            orderDiagnosedAtStart: req.body.orderDiagnosedAtStart,
            orderDiagnosedAtEnd: req.body.orderDiagnosedAtEnd,
            orderCompletedAt: req.body.orderCompletedAt,
            orderCompletedAtStart: req.body.orderCompletedAtStart,
            orderCompletedAtEnd: req.body.orderCompletedAtEnd,
            orderIssuedAt: req.body.orderIssuedAt,
            orderIssuedAtStart: req.body.orderIssuedAtStart,
            orderIssuedAtEnd: req.body.orderIssuedAtEnd,
            orderCreatedBy: [],
            orderDiagnosedBy: [],
            orderCompletedBy: [],
            orderIssuedBy: [],
            individualCustomer: req.body.individualCustomer,
            legalEntityCustomer: req.body.legalEntityCustomer,
            individualCustomerId: req.body.individualCustomerId,
            legalEntityCustomerId: req.body.legalEntityCustomerId,
            deviceType: req.body.deviceType || [],
            deviceManufacturer: req.body.deviceManufacturer || [],
            deviceModel: req.body.deviceModel,
            deviceItemCode: req.body.deviceItemCode,
            deviceSN: req.body.deviceSN,
            deviceIMEI: req.body.deviceIMEI,
            deviceState: req.body.deviceState || [],
            deviceOSVersion: req.body.deviceOSVersion,
            individualOwner: req.body.individualOwner,
            legalEntityOwner: req.body.legalEntityOwner,
            individualOwnerId: req.body.individualOwnerId,
            legalEntityOwnderId: req.body.legalEntityOwnerId,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            orderTypes: orderTypes,
            orderStates: orderStates,
            deviceStates: deviceStates,
            orders: orders,
            users: users,
            timePassedVerbose: timePassedVerbose,
            pageTitle: `Все заявки | ${res.locals.userCompany.title.short}`,
            path: "/orders",
            dateOptions: dateOptions,
            modal: {
                open: false,
                sameCustomerMessage: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.getViewOrder = async (req, res, next) => {
    try {
        const order = await Order.findById(req.params.orderId);
        const customer = order.customer.individual.lastName
            ? {
                  type: `Individual`,
                  fullName: `${order.customer.individual.lastName} ${order.customer.individual.firstName} ${order.customer.individual.middleName}`,
                  address: ` `,
                  phone: order.customer.individual.phone,
                  email: order.customer.individual.email,
              }
            : {
                  type: `Legal Entity`,
                  fullName: `${order.customer.legalEntity.shortName} (${order.customer.legalEntity.fullName})`,
                  address: ` `,
                  phone: order.customer.legalEntity.phone,
                  email: order.customer.legalEntity.email,
              };

        const deviceOwner = order.device.owner.individual.lastName
            ? {
                  type: `Inividual`,
                  fullName: `${order.device.owner.individual.lastName} ${order.device.owner.individual.firstName} ${order.device.owner.individual.middleName}`,
                  address: ` `,
                  phone: order.device.owner.individual.phone,
                  email: order.device.owner.individual.email,
              }
            : {
                  type: `Legal Entity`,
                  fullName: `${order.device.owner.legalEntity.shortName} (${order.device.owner.legalEntity.fullName})`,
                  address: ` `,
                  phone: order.device.owner.legalEntity.phone,
                  email: order.device.owner.legalEntity.email,
              };

        const device = await Device.findById(order.device._id);
        const manufacturer = await Manufacturer.findOne({
            title: order.device.manufacturer,
        });
        const previousOrders =
            (await Order.find({
                $or: [
                    { "device._id": order.device._id },
                    { "device.sn": order.device.sn },
                    { "device.prevSns": order.device.sn },
                ],
            })) || null;
        const comments = await Comment.find({
            $and: [
                { "belongsTo.id": order._id },
                { "belongsTo.entity": "order" },
            ],
        });

        const myCompany = await MyCompany.findById(
            req.session.user.company._id
        );

        const mailgunApi = myCompany.apiKeys.mailgun ? true : false;
        const smsRuApi = myCompany.apiKeys.smsRu ? true : false;

        return res.render("order/view", {
            order: order,
            inRepairSince: timePassedVerbose(order.createdAt, new Date()),
            customer: customer,
            deviceOwner: deviceOwner,
            previousOrders: previousOrders,
            device: device,
            manufacturer: manufacturer,
            comments: comments,
            mailgunApi: mailgunApi,
            smsRuApi: smsRuApi,
            pageTitle: `Ремонт №${order.orderNo} | ${res.locals.userCompany.title.short}`,
            path: "/orders/view",
            dateOptions: dateOptions,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
        });
    } catch (err) {
        throw err;
    }
};

//
exports.getAddOrder = async (req, res, next) => {
    try {
        const customers = await Customer.find();
        const orderTypes = await OrderType.find();
        const states = Order.schema.path("state").enumValues;
        const manufacturers = await Manufacturer.find();
        const deviceTypes = await DeviceType.find();
        const statesModal = ["На складе СЦ", "У Клиента"];
        const device = await Device.findById(req.query.device);
        const conditionCodes = await ConditionCode.find();
        const symptomCodes = await SymptomCode.find();

        return res.render("order/add", {
            pageTitle: `Добавить Заявку | ${res.locals.userCompany.title.short}`,
            path: "orders/add",
            customers: customers,
            states: states,
            statesModal: statesModal,
            manufacturers: manufacturers,
            deviceTypes: deviceTypes,
            orderTypes: orderTypes,
            owner: device ? device.owner : undefined,
            device: device,
            conditionCodes: conditionCodes,
            symptomCodes: symptomCodes,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                individualOwner: "",
                individualOwnerId: "",
                legalEntityOwner: "",
                legalEntityOwnerId: "",
                device: "",
                deviceId: "",
                orderType: "",
                deviceAppearance: "",
                setStandard: "",
                statedMalfunction: "",
                state: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAddOrder = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const customers = await Customer.find();
            const orderTypes = await OrderType.find();
            const states = Order.schema.path("state").enumValues;
            const statesModal = ["На складе СЦ", "У Клиента"];
            const manufacturers = await Manufacturer.find();
            const deviceTypes = await DeviceType.find();
            const device = req.body.deviceId
                ? await Device.findById(req.body.deviceId)
                : undefined;
            const owner = device ? device.owner : undefined;

            return res.status(422).render("order/add", {
                pageTitle: `Добавить Заявку | ${res.locals.userCompany.title.short}`,
                path: "orders/add",
                customers: customers,
                states: states,
                orderTypes: orderTypes,
                owner: owner,
                device: device,
                statesModal: statesModal,
                manufacturers: manufacturers,
                deviceTypes: deviceTypes,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    individualOwner: req.body.individualOwner,
                    individualOwnerId: req.body.individualOwnerId,
                    legalEntityOwner: req.body.legalEntityOwner,
                    legalEntityOwnerId: req.body.legalEntityOwnerId,
                    device: req.body.device,
                    deviceId: req.body.deviceId,
                    orderType: req.body.orderType,
                    deviceAppearance: req.body.deviceAppearance,
                    setStandard: req.body.setStandard,
                    statedMalfunction: req.body.statedMalfunction,
                    state: req.body.state,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const customer =
            req.body.ownerRadios === "individualOwner"
                ? await Customer.findOne({
                      _id: req.body.individualOwnerId,
                  }).populate("_id")
                : await LegalEntity.findOne({
                      _id: req.body.legalEntityOwnerId,
                  }).populate("_id");
        const device = await Device.findOne({
            _id: req.body.deviceId,
        }).populate("_id");
        const orderType = await OrderType.findOne({
            title: req.body.orderType,
        }).populate("_id");

        const customerData =
            req.body.ownerRadios === "individualOwner"
                ? {
                      individual: {
                          _id: customer._id,
                          firstName: customer.name.first,
                          lastName: customer.name.last,
                          middleName: customer.name.middle,
                          phone: customer.contacts.phone,
                          email: customer.contacts.email,
                          address: {
                              city: customer.city,
                              street: customer.street,
                              house: customer.house,
                          },
                      },
                  }
                : {
                      legalEntity: {
                          _id: customer._id,
                          shortName: customer.shortName,
                          fullName: customer.fullName,
                          phone: customer.phone,
                          email: customer.email,
                          isDealer: customer.isDealer,
                          address: {
                              city: customer.city,
                              street: customer.street,
                              house: customer.house,
                          },
                      },
                  };

        const conditionCode = await ConditionCode.find({
            title: req.body.conditionCode,
        });
        const symptomCode = await SymptomCode.find({
            title: req.body.symptomCode,
        });

        const order = new Order({
            customer: customerData,
            device: {
                _id: device._id,
                owner: {
                    individual: {
                        _id: device.owner.individual._id,
                        firstName: device.owner.individual.firstName,
                        lastName: device.owner.individual.lastName,
                        middleName: device.owner.individual.middleName,
                        phone: device.owner.individual.phone,
                        email: device.owner.individual.phone,
                    },
                    legalEntity: {
                        _id: device.owner.legalEntity._id,
                        shortName: device.owner.legalEntity.shortName,
                        fullName: device.owner.legalEntity.fullName,
                        phone: device.owner.legalEntity.phone,
                        email: device.owner.legalEntity.email,
                    },
                },
                manufacturer: device.manufacturer,
                type: device.type,
                model: device.model,
                itemCode: device.itemCode,
                sn: device.sn,
                imei1: device.imei1,
                imei2: device.imei2,
                osVersion: device.osVersion,
                dateOfSale: device.dateOfSale,
                state: device.state,
            },
            type: {
                title: orderType.title,
                warranty: orderType.warranty,
            },
            codes: {
                conditionCode: {
                    title: conditionCode[0]?.title,
                    code: conditionCode[0]?.code,
                },
                symptomCode: {
                    title: symptomCode[0]?.title,
                    code: symptomCode[0]?.code,
                },
            },
            statedMalfunction: req.body.statedMalfunction,
            deviceAppearance: req.body.deviceAppearance,
            setStandard: req.body.setStandard,
            state: req.body.state,
            prevState: req.body.state,
            createdBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
            updatedBy: {
                id: req.session.user,
                name: {
                    last: req.session.user.name.last,
                    first: req.session.user.name.first,
                },
            },
            providerOrderId: "",
            providerOrderState: "Не создан",
            diagnosedAt: null,
            completedAt: null,
            issuedAt: null,
            diagnosedBy: null,
            completedBy: null,
            issuedBy: null,
            history: [
                {
                    begin: new Date(),
                    state: req.body.state,
                    by: {
                        _id: req.session.user._id,
                        firstName: req.session.user.name.first,
                        lastName: req.session.user.name.last,
                    },
                },
            ],
        });

        await order.save();

        /* const newOrder = await Order.findById(order._id);

        const channel = rocketChatClient.channels.create(
            newOrder.orderNo.toString(),
            (err, body) => {
                newOrder.rocketChat.roomId = body.channel._id;
                newOrder.save();
            }
        ); */

        return res.redirect(`/orders/view/${order._id}`);
    } catch (err) {
        throw err;
    }
};

exports.getEditOrder = async (req, res, next) => {
    try {
        const states = Order.schema.path("state").enumValues;
        const providerOrderStates =
            Order.schema.path("providerOrderState").enumValues;
        const types = await OrderType.find();
        const warrantyExtensionStates = Order.schema.path(
            "warrantyExtensionState"
        ).enumValues;
        const negotiationObjectList =
            Order.schema.path("negotiation.object").enumValues;

        const paymentMethodList =
            Order.schema.path("payments.method").enumValues;

        const order = await Order.findById(req.params.orderId);
        if (!order) {
            return res.redirect("/");
        }

        const conditionCodes = await ConditionCode.find({}, null, {
            sort: { title: 1 },
        });
        const symptomCodes = await SymptomCode.find();
        const sectionCodes = await SectionCode.find();
        const defectCodes = await DefectCode.find();
        const repairCodes = await RepairCode.find();

        return res.render("order/edit/index", {
            pageTitle: `Изменить Заявку | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            order: order,
            states: states,
            providerOrderStates: providerOrderStates,
            types: types,
            conditionCode: conditionCodes,
            symptomCodes: symptomCodes,
            sectionCodes: sectionCodes,
            defectCodes: defectCodes,
            repairCodes: repairCodes,
            warrantyExtensionStates: warrantyExtensionStates,
            negotiationObjectList: negotiationObjectList,
            paymentMethodList: paymentMethodList,
            dateOptions: dateOptions,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                islegalEntityOwner: "",
                individualOwner: "",
                individualOwnerId: "",
                legalEntityOwner: "",
                legalEntityOwnerId: "",
                device: "",
                deviceId: "",
                orderType: "",
                deviceAppearance: "",
                setStandard: "",
                statedMalfunction: "",
                state: "",
                issueNote: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEditOrder = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const states = Order.schema.path("state").enumValues;
            const providerOrderStates =
                Order.schema.path("providerOrderState").enumValues;
            const types = await OrderType.find();
            const warrantyExtensionStates = Order.schema.path(
                "warrantyExtensionState"
            ).enumValues;
            const negotiationObjectList =
                Order.schema.path("negotiation.object").enumValues;

            const order = await Order.findById(req.body.orderId);
            if (!order) {
                return res.redirect("/");
            }

            return res.status(422).render("order/edit/index", {
                pageTitle: `Изменить Заявку | ${res.locals.userCompany.title.short}`,
                path: "/edit",
                order: order,
                states: states,
                providerOrderStates: providerOrderStates,
                types: types,
                warrantyExtensionStates: warrantyExtensionStates,
                negotiationObjectList: negotiationObjectList,
                dateOptions: dateOptions,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    islegalEntityOwner:
                        req.body.ownerRadios === "individualOwner"
                            ? false
                            : true,
                    individualOwner: req.body.individualOwner,
                    individualOwnerId: req.body.individualOwnerId,
                    legalEntityOwner: req.body.legalEntityOwner,
                    legalEntityOwnerId: req.body.legalEntityOwnerId,
                    device: req.body.device,
                    deviceId: req.body.deviceId,
                    orderType: req.body.orderType,
                    deviceAppearance: req.body.deviceAppearance,
                    setStandard: req.body.setStandard,
                    statedMalfunction: req.body.statedMalfunction,
                    state: req.body.state,
                    issueNote: req.body.issueNote,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const customer =
            req.body.ownerRadios === "individualOwner"
                ? await Customer.findOne({
                      _id: req.body.individualOwnerId,
                  }).populate("_id")
                : await LegalEntity.findOne({
                      _id: req.body.legalEntityOwnerId,
                  }).populate("_id");
        const device = await Device.findOne({
            _id: req.body.deviceId,
        });

        const customerData =
            req.body.ownerRadios === "individualOwner"
                ? {
                      individual: {
                          _id: customer._id,
                          firstName: customer.name.first,
                          lastName: customer.name.last,
                          middleName: customer.name.middle,
                          phone: customer.contacts.phone,
                          email: customer.contacts.email,
                          address: {
                              city: customer.address.city,
                              street: customer.address.street,
                              house: customer.address.house,
                          },
                      },
                  }
                : {
                      legalEntity: {
                          _id: customer._id,
                          shortName: customer.shortName,
                          fullName: customer.fullName,
                          phone: customer.phone,
                          email: customer.email,
                          isDealer: customer.isDealer,
                          address: {
                              city: customer.city,
                              street: customer.street,
                              house: customer.house,
                          },
                      },
                  };

        const conditionCode = await ConditionCode.findOne({
            title: req.body.conditionCodeTitle,
        });
        const symptomCode = await SymptomCode.findOne({
            title: req.body.symptomCodeTitle,
        });
        const sectionCode = [];
        const defectCode = [];
        const repairCode = [];
        const spareParts = [];

        const paymentAmount = [];
        const paymentMethod = [];
        const paymentDate = [];

        const array = Object.entries(req.body);
        for (const [key, value] of array) {
            if (
                key.substring(0, key.length - 1) === "sectionCodeTitle" &&
                value
            ) {
                const el = await SectionCode.findOne({ title: value });
                const result = {
                    _id: el._id,
                    code: el.code,
                    title: el.title,
                };
                sectionCode.push(result);
            }

            if (
                key.substring(0, key.length - 1) === "defectCodeTitle" &&
                value
            ) {
                const el = await DefectCode.findOne({ title: value });
                const result = {
                    _id: el._id,
                    code: el.code,
                    title: el.title,
                };
                defectCode.push(result);
            }

            if (
                key.substring(0, key.length - 1) === "repairCodeTitle" &&
                value
            ) {
                const el = await RepairCode.findOne({ title: value });
                const result = {
                    _id: el._id,
                    code: el.code,
                    title: el.title,
                };
                repairCode.push(result);
            }

            if (key.substring(0, key.length - 1) === "sparePart") {
                const result = {
                    partNumber: value,
                    primeCost:
                        req.body[`primeCost${key.substring(key.length - 1)}`],
                    repairCost:
                        req.body[`repairCost${key.substring(key.length - 1)}`],
                    terms: req.body[
                        `repairTerms${key.substring(key.length - 1)}`
                    ],
                    DOA: req.body[`DOA${key.substring(key.length - 1)}`]
                        ? true
                        : false,
                    notUsed: req.body[`notUsed${key.substring(key.length - 1)}`]
                        ? true
                        : false,
                };
                spareParts.push(result);
            }

            if (key.substring(0, key.length - 1) === "paymentAmount" && value) {
                paymentAmount[key.substring(key.length - 1, key.length)] =
                    value;
            }

            if (key.substring(0, key.length - 1) === "paymentMethod" && value) {
                paymentMethod[key.substring(key.length - 1, key.length)] =
                    value;
            }

            if (key.substring(0, key.length - 1) === "paymentDate" && value) {
                paymentDate[key.substring(key.length - 1, key.length)] = value;
            }
        }

        const order = await Order.findById(req.body.orderId);
        const deviceEdited = await Device.findById(req.body.deviceId);
        const orderType = await OrderType.findOne({
            title: req.body.orderType,
        });
        const orderTypeData = {
            title: orderType.title,
            warranty: orderType.warranty,
        };

        for (let i = 0; i < paymentAmount.length; i++) {
            order.payments[i] = {};
            order.payments[i].amount = paymentAmount[i];
            order.payments[i].method = paymentMethod[i];
            order.payments[i].date = paymentDate[i];
        }

        if (
            req.body.state === "Устройство выдано" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у клиента)"
        ) {
            deviceEdited.state = "У Клиента";
            device.state = "У Клиента";
            await deviceEdited.save();
        }
        // --------------------- UPDATING ORDER ---------------------
        // updating order history - state
        if (order.state !== req.body.state) {
            order.history.push({
                begin: new Date(),
                state: req.body.state,
                by: {
                    _id: req.session.user._id,
                    firstName: req.session.user.name.first,
                    lastName: req.session.user.name.last,
                },
            });
        }

        // updating order history - providerOrderId & sending notification to Rocket.Chat
        if (order.providerOrderId !== req.body.providerOrderId) {
            rocketChatClient.chat.postMessage({
                roomId: "zLBkqCJo4tm2Txttd",
                text: `Ремонту №${order.orderNo} присвоен RN ${req.body.providerOrderId}. Ссылка: https://portal.f1-service.pro/orders/view/${order._id}`,
            });
            order.history.push({
                parameter: {
                    title: "Номер ремонта в базе Сервис-Провайдера",
                    when: new Date(),
                    action: `изменён на ${req.body.providerOrderId}`,
                    by: {
                        _id: req.session.user._id,
                        firstName: req.session.user.name.first,
                        lastName: req.session.user.name.last,
                    },
                },
            });
        }

        order.codes.conditionCode = conditionCode;
        order.codes.symptomCode = symptomCode;
        order.codes.sectionCode = sectionCode;
        order.codes.defectCode = defectCode;
        order.codes.repairCode = repairCode;

        order.spareParts = spareParts;

        order.device._id = req.body.deviceId;
        order.device.owner = device.owner;
        order.device.manufacturer = device.manufacturer;
        order.device.type = device.type;
        order.device.model = device.model;
        order.device.sn = device.sn;
        order.device.itemCode = device.itemCode;
        order.device.imei1 = device.imei1;
        order.device.imei2 = device.imei2;
        order.device.osVersion = device.osVersion;
        order.device.state = device.state;

        order.customer = customerData;
        order.statedMalfunction = req.body.statedMalfunction;
        order.deviceAppearance = req.body.deviceAppearance;
        order.setStandard = req.body.setStandard;
        order.warrantyExtensionState = req.body.warrantyExtensionState;
        order.statedRepairTime = req.body.statedRepairTime;
        order.providerOrderId = req.body.providerOrderId;
        order.negotiation.theme = req.body.negotiationTheme;
        order.negotiation.object = req.body.negotiationObject;
        order.negotiation.terms = req.body.negotiationTerms;
        order.type = orderTypeData;

        // check if issueNote changed & if so - reset submition
        if (order.issueNote !== req.body.issueNote) {
            order.issueNoteSubmitted = false;
        }

        order.issueNote = req.body.issueNote;

        order.completedAt =
            req.body.state === "Ремонт окончен" ||
            req.body.state === "Устройство выдано" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у нас)" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у клиента)"
                ? order.completedAt
                    ? order.completedAt
                    : new Date()
                : "";
        order.completedBy =
            req.body.state === "Ремонт окончен" ||
            req.body.state === "Устройство выдано" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у нас)" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у клиента)"
                ? order.completedBy
                    ? order.completedBy
                    : req.session.user._id
                : null;
        order.issuedAt =
            req.body.state === "Устройство выдано" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у нас)" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у клиента)"
                ? order.issuedAt
                    ? order.issuedAt
                    : new Date()
                : "";
        order.issuedBy =
            req.body.state === "Устройство выдано" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у нас)" ||
            req.body.state ===
                "Выдан акт неремонтопригодности (устройство у клиента)"
                ? order.issuedBy
                    ? order.issuedBy
                    : req.session.user._id
                : null;
        order.diagnosedAt =
            order.state === "Диагностика" ||
            order.state === "Продление гарантии"
                ? order.state !== req.body.state
                    ? req.body.state !== "Диагностика" &&
                      req.body.state !== "Продление гарантии"
                        ? new Date()
                        : ""
                    : ""
                : req.body.state === "Диагностика" ||
                  req.body.state === "Продление гарантии"
                ? ""
                : order.diagnosedAt;
        order.diagnosedBy =
            order.state === "Диагностика" ||
            order.state === "Продление гарантии"
                ? order.state !== req.body.state
                    ? req.body.state !== "Диагностика" &&
                      req.body.state !== "Продление гарантии"
                        ? req.session.user._id
                        : null
                    : null
                : req.body.state === "Диагностика" ||
                  req.body.state === "Продление гарантии"
                ? null
                : order.diagnosedBy;
        order.state = req.body.state;
        order.providerOrderState = req.body.providerOrderState;
        order.updatedBy.id = req.session.user;
        order.updatedBy.name.last = req.session.user.name.last;
        order.updatedBy.name.first = req.session.user.name.first;

        await order.save();

        /* if (
            ["Устройство выдано", "Выдан акт неремонтопригодности"].includes(
                order.state
            )
        ) {
            rocketChatClient.channels.archive(
                order.rocketChat.roomId,
                (err, body) => {}
            );
        } */

        return res.redirect(`/orders/view/${order._id}`);
    } catch (err) {
        throw err;
    }
};

exports.postNewDeviceIdentity = async (req, res, next) => {
    try {
        const device = await Device.findById(req.body.newIdentityDeviceId);
        const order = await Order.findById(req.body.orderId);

        device.prevSns.push(device.sn);
        device.prevImeis.push(device.imei1);
        device.prevImeis.push(device.imei2);

        order.device.prevSns.push(device.sn);
        order.device.prevImeis.push(device.imei1);
        order.device.prevImeis.push(device.imei2);

        device.sn = req.body.newSn;
        device.imei1 = req.body.newImei1;
        device.imei2 = req.body.newImei2;

        order.device.sn = req.body.newSn;
        order.device.imei1 = req.body.newImei1;
        order.device.imei2 = req.body.newImei2;

        order.newDeviceIdentity = true;

        await device.save();
        await order.save();

        return res.redirect(`/orders/edit/${order._id}?section=repair`);
    } catch (err) {
        throw err;
    }
};

exports.getPrintWorkOrder = async (req, res, next) => {
    try {
        const order = await Order.findById(req.params.orderId);
        const customerName = order.customer.individual._id
            ? `${order.customer.individual.lastName} ${order.customer.individual.firstName} ${order.customer.individual.middleName}`
            : `${order.customer.legalEntity.fullName}`;
        const customerPhone = order.customer.individual._id
            ? `${order.customer.individual.phone}`
            : `${order.customer.legalEntity.phone}`;
        const customerEmail = order.customer.individual._id
            ? `${order.customer.individual.email}`
            : `${order.customer.legalEntity.email}`;
        const company = await MyCompany.findById(res.locals.userCompany._id);
        if (!order) {
            return res.redirect("/");
        }

        return res.render("print/print_single_work_order", {
            pageTitle: `${order.orderNo} | Печать заказ-наряда`,
            path: "/order/work-order",
            order: order,
            company: company,
            customerName: customerName,
            customerPhone: customerPhone,
            customerEmail: customerEmail,
            dateOptions: dateOptions,
        });
    } catch (err) {
        return err;
    }
};

exports.getPrintNRPAct = async (req, res, next) => {
    try {
        const order = await Order.findById(req.params.orderId);
        const customerName = order.customer.individual._id
            ? `${order.customer.individual.lastName} ${order.customer.individual.firstName} ${order.customer.individual.middleName}`
            : `${order.customer.legalEntity.fullName}`;
        const customerPhone = order.customer.individual._id
            ? `${order.customer.individual.phone}`
            : `${order.customer.legalEntity.phone}`;
        const customerEmail = order.customer.individual._id
            ? `${order.customer.individual.email}`
            : `${order.customer.legalEntity.email}`;
        const company = await MyCompany.findById(res.locals.userCompany._id);
        if (!order) {
            return res.redirect("/");
        }

        return res.render("print/templates/nrp_act_huawei_honor", {
            pageTitle: `${order.orderNo} | Печать заказ-наряда`,
            path: "/order/work-order",
            order: order,
            company: company,
            customerName: customerName,
            customerPhone: customerPhone,
            customerEmail: customerEmail,
            dateOptions: dateOptions,
        });
    } catch (err) {
        return err;
    }
};

exports.getPrintActOfAcceptance = async (req, res, next) => {
    try {
        const order = await Order.findById(req.params.orderId);
        const device = await Device.findById(order.device._id);

        let totalRepairCost = 0;
        if (!order.type.warranty) {
            for (part of order.spareParts) {
                totalRepairCost += part.repairCost;
            }
        }

        const customerName = order.customer.individual._id
            ? `${order.customer.individual.lastName} ${order.customer.individual.firstName} ${order.customer.individual.middleName}`
            : `${order.customer.legalEntity.fullName}`;
        const customerPhone = order.customer.individual._id
            ? `${order.customer.individual.phone}`
            : `${order.customer.legalEntity.phone}`;
        const customerEmail = order.customer.individual._id
            ? `${order.customer.individual.email}`
            : `${order.customer.legalEntity.email}`;
        const company = await MyCompany.findById(res.locals.userCompany._id);
        if (!order) {
            return res.redirect("/");
        }

        return res.render("print/print_single_act_of_acceptance", {
            pageTitle: `${order.orderNo} | Печать акта выполненных работ`,
            path: "/order/act-of-acceptance",
            order: order,
            device: device,
            company: company,
            customerName: customerName,
            customerPhone: customerPhone,
            customerEmail: customerEmail,
            dateOptions: dateOptions,
            totalRepairCost:
                currencyFormatter.format(totalRepairCost, { code: "RUB" }) ||
                "",
            createdAt: order.createdAt.toLocaleDateString("ru-RU", dateOptions),
            completedAt: order.completedAt
                ? order.completedAt.toLocaleDateString("ru-RU", dateOptions)
                : "",
            issuedAt: new Date().toLocaleDateString("ru-RU", dateOptions),
        });
    } catch (err) {
        return err;
    }
};

exports.postDeleteOrder = async (req, res, next) => {
    try {
        await Order.deleteOne({ _id: req.body.orderId });
        return res.redirect("/orders");
    } catch (err) {
        throw err;
    }
};

exports.postAddFileToOrder = async (req, res, next) => {
    try {
        const order = await Order.findById(req.body.orderId);
        const files = req.files;
        if (!files.length) {
            const customer = order.customer.individual.lastName
                ? {
                      type: `Individual`,
                      fullName: `${order.customer.individual.lastName} ${order.customer.individual.firstName} ${order.customer.individual.middleName}`,
                      address: ` `,
                      phone: order.customer.individual.phone,
                      email: order.customer.individual.email,
                  }
                : {
                      type: `Legal Entity`,
                      fullName: `${order.customer.legalEntity.shortName} (${order.customer.legalEntity.fullName})`,
                      address: ` `,
                      phone: order.customer.legalEntity.phone,
                      email: order.customer.legalEntity.email,
                  };

            const deviceOwner = order.device.owner.individual.lastName
                ? {
                      type: `Inividual`,
                      fullName: `${order.device.owner.individual.lastName} ${order.device.owner.individual.firstName} ${order.device.owner.individual.middleName}`,
                      address: ` `,
                      phone: order.device.owner.individual.phone,
                      email: order.device.owner.individual.email,
                  }
                : {
                      type: `Legal Entity`,
                      fullName: `${order.device.owner.legalEntity.shortName} (${order.device.owner.legalEntity.fullName})`,
                      address: ` `,
                      phone: order.device.owner.legalEntity.phone,
                      email: order.device.owner.legalEntity.email,
                  };

            const device = await Device.findById(order.device._id);
            const manufacturer = await Manufacturer.findOne({
                title: order.device.manufacturer,
            });
            const previousOrders =
                (await Order.find({ "device._id": device._id })) || null;
            const comments = await Comment.find({
                $and: [
                    { "belongsTo.id": order._id },
                    { "belongsTo.entity": "order" },
                ],
            });

            const myCompany = await MyCompany.findById(
                req.session.user.company._id
            );

            const mailgunApi = myCompany.apiKeys.mailgun ? true : false;
            const smsRuApi = myCompany.apiKeys.smsRu ? true : false;

            return res.render("order/view", {
                order: order,
                inRepairSince: timePassedVerbose(order.createdAt, new Date()),
                customer: customer,
                deviceOwner: deviceOwner,
                previousOrders: previousOrders,
                device: device,
                manufacturer: manufacturer,
                comments: comments,
                mailgunApi: mailgunApi,
                smsRuApi: smsRuApi,
                pageTitle: `Ремонт №${order.orderNo} | ${res.locals.userCompany.title.short}`,
                path: "/orders/view",
                dateOptions: dateOptions,
                errorMessage:
                    "Файл отсутствует или недопустимое расширение файла.",
                successMessage: null,
                infoMessage: null,
            });
        }
        const attachedFiles = files.map((file) => {
            return {
                longTitle: file.filename,
                shortTitle: file.originalname,
                path: file.path,
            };
        });

        const newFilesArray = order.attachedFiles.concat(attachedFiles);

        order.attachedFiles = newFilesArray;

        await order.save();

        return res.redirect(`/orders/view/${order._id}`);
    } catch (err) {
        throw err;
    }
};

exports.getViewAttachement = (req, res, next) => {
    const fileTitle = req.params.fileTitle;
    const filePath = path.join("uploads", "orderFiles", fileTitle);
    fs.readFile(filePath, (err, data) => {
        if (err) {
            return next(err);
        }
        res.setHeader("Content-Disposition", "inline");
        res.send(data);
    });
};

exports.postMassIssue = async (req, res, next) => {
    let items = req.body.items.split("|");
    const customerNames = [];
    const customerPhones = [];
    const customerEmails = [];
    const createdAts = [];
    const completedAts = [];
    const orders = [];

    const company = await MyCompany.findById(res.locals.userCompany._id);

    let lastCustomer;
    let allCustomersEqual = true;

    try {
        for (let i = 0; i < items.length; i++) {
            if (items[i] && items[i] != "undefined") {
                const order = await Order.findById(items[i]);

                if (!order) continue;

                orders.push(order);

                if (lastCustomer && req.body.actOfTransfer) {
                    allCustomersEqual =
                        lastCustomer == order.customer.individual._id;
                } else {
                    lastCustomer = order.customer.individual._id;
                }

                customerNames.push(
                    order.customer.individual._id
                        ? `${order.customer.individual.lastName} ${order.customer.individual.firstName} ${order.customer.individual.middleName}`
                        : `${order.customer.legalEntity.fullName}`
                );

                customerPhones.push(
                    order.customer.individual._id
                        ? `${order.customer.individual.phone}`
                        : `${order.customer.legalEntity.phone}`
                );

                customerEmails.push(
                    order.customer.individual._id
                        ? `${order.customer.individual.email}`
                        : `${order.customer.legalEntity.email}`
                );

                createdAts.push(
                    order.createdAt.toLocaleDateString("ru-RU", dateOptions)
                );
                completedAts.push(
                    order.completedAt
                        ? order.completedAt.toLocaleDateString(
                              "ru-RU",
                              dateOptions
                          )
                        : ""
                );
            }
        }

        if (req.body.status) {
            orders.forEach(async (order) => {
                const device = await Device.findById(order.device._id);
                if (order.state === "Ремонт окончен") {
                    device.state = "У Клиента";
                    order.device.state = "У Клиента";
                    order.state = "Устройство выдано";
                    order.issuedAt = new Date();
                    order.issuedBy = req.session.user._id;
                    // updating order history - state
                    order.history.push({
                        begin: new Date(),
                        state: "Устройство выдано",
                        by: {
                            _id: req.session.user._id,
                            firstName: req.session.user.name.first,
                            lastName: req.session.user.name.last,
                        },
                    });
                } else if (order.state === "Устройство списано") {
                    device.state = "Списано";
                    order.device.state = "Списано";
                    order.state = "Выдан акт неремонтопригодности";
                    order.issuedAt = new Date();
                    order.issuedBy = req.session.user._id;
                    order.issuedAt = new Date();
                    order.issuedBy = req.session.user._id;
                    // updating order history - state
                    order.history.push({
                        begin: new Date(),
                        state: "Выдан акт неремонтопригодности",
                        by: {
                            _id: req.session.user._id,
                            firstName: req.session.user.name.first,
                            lastName: req.session.user.name.last,
                        },
                    });
                }

                await device.save();
                await order.save();
            });
        }
    } catch (err) {
        throw err;
    }

    try {
        if (
            (allCustomersEqual && req.body.actOfTransfer) ||
            !req.body.actOfTransfer ||
            (req.body.transferCustomer &&
                !allCustomersEqual &&
                req.body.actOfTransfer)
        ) {
            return res.render("print/collectors/mass_issue_acts", {
                pageTitle: `Печать актов выполненных работ и накладной`,
                path: "/mass-issue-acts",
                orders: orders,
                company: company,
                totalRepairCost: 0,
                customerNames: customerNames,
                customerPhones: customerPhones,
                customerEmails: customerEmails,
                dateOptions: dateOptions,
                issuedAt: new Date().toLocaleDateString("ru-RU", dateOptions),
                createdAts: createdAts,
                completedAts: completedAts,
                actOfTransfer: req.body.actOfTransfer,
                transferCustomer: req.body.transferCustomer
                    ? req.body.transferCustomer
                    : "",
            });
        } else {
            const orderTypes = await OrderType.find();
            const manufacturers = await Manufacturer.find();
            const deviceTypes = await DeviceType.find();
            const users = await User.find({ role: { $ne: "User" } });
            const orderStates = Order.schema.path("state").enumValues;
            const deviceStates = Device.schema.path("state").enumValues;

            const orders = await Order.find({
                $and: [
                    { state: { $ne: "Устройство выдано" } },
                    { state: { $ne: "Выдан акт неремонтопригодности" } },
                ],
            });
            return res.render("order/index", {
                orderFilterId: null,
                orderState: req.body.orderState || [],
                orderType: req.body.orderType || [],
                orderNo: req.body.orderNo,
                providerOrderId: req.body.providerOrderId,
                orderCreatedAt: req.body.orderCreatedAt,
                orderCreatedAtStart: req.body.orderCreatedAtStart,
                orderCreatedAtEnd: req.body.orderCreatedAtEnd,
                orderDiagnosedAt: req.body.orderDiagnosedAt,
                orderDiagnosedAtStart: req.body.orderDiagnosedAtStart,
                orderDiagnosedAtEnd: req.body.orderDiagnosedAtEnd,
                orderCompletedAt: req.body.orderCompletedAt,
                orderCompletedAtStart: req.body.orderCompletedAtStart,
                orderCompletedAtEnd: req.body.orderCompletedAtEnd,
                orderIssuedAt: req.body.orderIssuedAt,
                orderIssuedAtStart: req.body.orderIssuedAtStart,
                orderIssuedAtEnd: req.body.orderIssuedAtEnd,
                orderCreatedBy: [],
                orderDiagnosedBy: [],
                orderCompletedBy: [],
                orderIssuedBy: [],
                individualCustomer: req.body.individualCustomer,
                legalEntityCustomer: req.body.legalEntityCustomer,
                individualCustomerId: req.body.individualCustomerId,
                legalEntityCustomerId: req.body.legalEntityCustomerId,
                deviceType: req.body.deviceType || [],
                deviceManufacturer: req.body.deviceManufacturer || [],
                deviceModel: req.body.deviceModel,
                deviceItemCode: req.body.deviceItemCode,
                deviceSN: req.body.deviceSN,
                deviceIMEI: req.body.deviceIMEI,
                deviceState: req.body.deviceState || [],
                deviceOSVersion: req.body.deviceOSVersion,
                individualOwner: req.body.individualOwner,
                legalEntityOwner: req.body.legalEntityOwner,
                individualOwnerId: req.body.individualOwnerId,
                legalEntityOwnderId: req.body.legalEntityOwnerId,
                manufacturers: manufacturers,
                deviceTypes: deviceTypes,
                orderTypes: orderTypes,
                orderStates: orderStates,
                deviceStates: deviceStates,
                orders: orders,
                users: users,
                timePassedVerbose: timePassedVerbose,
                pageTitle: `Все заявки | ${res.locals.userCompany.title.short}`,
                path: "/orders",
                dateOptions: dateOptions,
                modal: {
                    open: true,
                    sameCustomerMessage:
                        "Вы выбрали ремонты с разными клиентами, укажите единого получателя",
                    status: req.body.status,
                    actOfTransfer: req.body.actOfTransfer,
                    items: req.body.items,
                },
            });
        }
    } catch (err) {
        throw err;
    }
};

exports.postSendEmail = async (req, res, next) => {
    try {
        const order = await Order.findById(req.body.orderId);

        const myCompany = await MyCompany.findById(
            req.session.user.company._id
        );

        //Mailgun API
        const domain = "helpdesk.f1lab.ru";
        const mailgun = require("mailgun-js")({
            domain: domain,
            apiKey: myCompany.apiKeys.mailgun,
        });

        //NEEDED: think about subject and mail text
        await mailgun
            .messages()
            .send({
                from: `noreply@${domain}`,
                to: req.body.email,
                subject: `Информация по ремонту №${order.orderNo}`,
                text: `Добрый день! Статус Вашего ремонта по заявке №${order.orderNo}: ${order.state} \nС уважением, \nF1-Service`,
            })
            .then(() => {
                req.flash("success", "На указанный адрес отправлено письмо."); //NEEDED: think about success message text
            })
            .catch(() => {
                req.flash("error", "Не удалось отправить письмо."); //NEEDED: think about error message text
            });
        return res.redirect(`/orders/view/${req.body.orderId}`);
    } catch (err) {
        throw err;
    }
};

exports.postSendSMS = async (req, res, next) => {
    try {
        const order = await Order.findById(req.body.orderId);
        const myCompany = await MyCompany.findById(
            req.session.user.company._id
        );

        const smsRu = new SMSRu(myCompany.apiKeys.smsRu);

        msg = `Добрый день! Статус Вашего ремонта №${order.orderNo}: ${order.state} \nС уважением, \nF1-Service`;

        await smsRu
            .sendSms({
                to: req.body.phone,
                msg: msg,
            })
            .then(() => {
                req.flash(
                    "success",
                    "На указанный номер отправлено оповещение."
                ); //NEEDED: think about success message text
            })
            .catch(() => {
                req.flash("error", "Не удалось отправить оповещение."); //NEEDED: think about error message text
            });
        return res.redirect(`/orders/view/${req.body.orderId}`);
    } catch (err) {
        throw err;
    }
};

exports.postIssueNoteSubmit = async (req, res, next) => {
    try {
        const order = await Order.findById(req.body.orderId);
        const user = await User.findById(req.session.user._id);

        order.issueNoteSubmitted = true;
        order.issueNoteSubmittedBy = user;

        order.history.push({
            parameter: {
                title: "Комментарий сотрудника СЦ",
                when: new Date(),
                action: "утверждён",
                by: {
                    _id: req.session.user._id,
                    firstName: req.session.user.name.first,
                    lastName: req.session.user.name.last,
                },
            },
        });

        order.save();

        return res.redirect(`/orders/view/${req.body.orderId}`);
    } catch (err) {
        throw err;
    }
};
