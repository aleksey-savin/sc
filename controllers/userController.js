const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

const User = require("../models/user");
const MyCompany = require("../models/myCompany");

exports.getIndex = async (req, res, next) => {
    try {
        const users = await User.find();
        return res.render("user/index", {
            users: users,
            pageTitle: `Все Пользователи | ${res.locals.userCompany.title.short}`,
            path: "/users",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAdd = async (req, res, next) => {
    const roles = User.schema.path("role").enumValues;    
    try {
        const companies = await MyCompany.find();
        res.render("user/add", {            
            pageTitle: `Добавление Аккаунта | ${res.locals.userCompany.title.short}`,
            path: "users/add",
            roles: roles,            
            companies: companies,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                email: "",
                phone: "",
                name: {
                    first: "",
                    last: "",
                },
                role: "",
                permissions: "",
                company: "",
                password: "",
                confirmPassword: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAdd = async (req, res, next) => {
    const errors = validationResult(req);
    const isActive = req.body.isActive === "on" ? true : false;
    const roles = User.schema.path("role").enumValues;
    const password = req.body.password;
    try {
        const companies = await MyCompany.find();
        if (!errors.isEmpty()) {
            return res.status(422).render("user/add", {
                path: "users/add",
                pageTitle: `Добавить пользователя | ${res.locals.userCompany.title.short}`,
                roles: roles,
                companies: companies,
                errorMessage: errors.array()[0].msg,                
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    email: req.body.email,
                    phone: req.body.phone,
                    name: {
                        first: req.body.firstName,
                        last: req.body.lastName,
                    },
                    role: req.body.role,
                    company: req.body.company,
                    password: req.body.password,
                    confirmPassword: req.body.confirmPassword,
                },
            });
        }
        const company = await MyCompany.findOne({
            "title.full": req.body.company,
        });
        const hashedPassword = await bcrypt.hash(password, 12);
        crypto.randomBytes(32, async (err, buffer) => {
            const user = new User({
                name: {
                    first: req.body.firstName,
                    last: req.body.lastName,
                },
                email: req.body.email,
                phone: req.body.phone,
                password: hashedPassword,
                role: req.body.role,
                company: {
                    _id: company._id,
                    title: {
                        full: req.body.company,
                        short: company.title.short,
                    },
                },
                active: isActive,
            });
            await user.save();

            return res.redirect("/users");
        });
    } catch (err) {
        throw err;
    }
};

exports.getEdit = async (req, res, next) => {
    const userId = req.params.userId;
    const roles = User.schema.path("role").enumValues;
    try {
        const companies = await MyCompany.find();
        const user = await User.findById(userId);
        if (!user) {
            return res.redirect("/users");
        }
        return res.render("user/edit", {
            pageTitle: `Редактирование Пользователя | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            user: user,
            roles: roles,
            companies: companies,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                email: "",
                phone: "",
                name: {
                    first: "",
                    last: "",
                },
                role: "",
                company: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEdit = async (req, res, next) => {    
    const errors = validationResult(req);
    const userId = req.body.userId;
    const isActive = req.body.isActive === "on" ? true : false;
    const roles = User.schema.path("role").enumValues;

    if (!errors.isEmpty()) {
        try {
            const companies = await MyCompany.find();
            const user = await User.findById(userId);
            return res.status(422).render("user/edit", {
                pageTitle: `Редактирование Пользователя | ${res.locals.userCompany.title.short}`,
                path: "/edit",
                user: user,
                roles: roles,
                companies: companies,
                errorMessage: errors.array()[0].msg,               
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    email: req.body.email,
                    phone: req.body.phone,
                    name: {
                        first: req.body.firstName,
                        last: req.body.lastName,
                    },
                    role: req.body.role,
                    company: req.body.company,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const company = await MyCompany.findOne({
            "title.full": req.body.company,
        });
        const user = await User.findById(userId);
        user.email = req.body.email;
        user.phone = req.body.phone;
        user.name.first = req.body.firstName;
        user.name.last = req.body.lastName;
        user.role = req.body.role;
        user.company._id = company._id;
        user.company.title.full = req.body.company;
        user.company.title.short = company.title.short;
        user.active = isActive;
        await user.save();
        return res.redirect("/users");
    } catch (err) {
        throw err;
    }
};

exports.getMyAccount = async (req, res, next) => {
    const userData = req.session.user;
    try {
        const user = await User.findById(userData._id);
        if (!user) {
            return res.redirect("/users");
        }
        return res.render("user/my_account", {
            pageTitle: `Мой Аккаунт | ${res.locals.userCompany.title.short}`,
            path: "/my-account",
            user: user,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                email: "",
                phone: "",
                name: {
                    first: "",
                    last: "",
                },
                darkTheme: "",
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postMyAccount = async (req, res, next) => {
    const errors = validationResult(req);

    const userId = req.body.userId;

    if (!errors.isEmpty()) {
        try {
            const user = await User.findById(userId);

            return res.status(422).render("user/my-account", {
                path: "/my-account",
                pageTitle: `Мой Аккаунт | ${res.locals.userCompany.title.short}`,
                user: user,
                errorMessage: errors.array()[0].msg,               
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    email: req.body.email,
                    phone: req.body.phone,
                    name: {
                        first: req.body.firstName,
                        last: req.body.lastName,
                    },
                    darkTheme: req.body.darkTheme === "on" ? true : false,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const user = await User.findById(userId);
        user.email = req.body.email;
        user.phone = req.body.phone;
        user.name.first = req.body.firstName;
        user.name.last = req.body.lastName;
        user.darkTheme = req.body.darkTheme === "on" ? true : false;
        req.session.user = user;
        await req.session.save();
        await user.save();
        return res.redirect("/users");
    } catch (err) {
        throw err;
    }
};

exports.postDelete = async (req, res, next) => {
    const userId = req.body.userId;
    try {
        await User.deleteOne({
            _id: userId,
        });
        return res.redirect("/users");
    } catch (err) {
        throw err;
    }
};
