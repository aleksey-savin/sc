const SparePart = require("../models/spareParts");
const Manufacturer = require("../models/manufacturer");
const { validationResult } = require("express-validator");
const { alertMessage } = require("../middleware/alertMessage");

exports.getIndex = async (req, res, next) => {
    try {
        const spareParts = await SparePart.find();
        return res.render("sparePart/index", {
            spareParts: spareParts,
            pageTitle: `Все Запчасти | ${res.locals.userCompany.title.short}`,
            path: "/",
        });
    } catch (err) {
        throw err;
    }
};

exports.getAddSparePart = async (req, res, next) => {
    try {
        const manufacturers = await Manufacturer.find();
        return res.render("sparePart/add", {
            pageTitle: `Добавить Запчасть | ${res.locals.userCompany.title.short}`,
            path: "spareParts/add",
            manufacturers: manufacturers,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                partNumber: null,
                serialNumber: null,
                vendor: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postAddSparePart = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        try {
            const manufacturers = await Manufacturer.find();
            return res.status(422).render("sparePart/add", {
                path: "spareParts/add",
                pageTitle: `Добавить Запчасть | ${res.locals.userCompany.title.short}`,
                manufacturers: manufacturers,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    partNumber: req.body.partNumber,
                    serialNumber: req.body.serialNumber,
                    vendor: req.body.vendor,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const sparePart = new SparePart({
            title: req.body.title,
            partNumber: req.body.partNumber,
            serialNumber: req.body.serialNumber,
            vendor: req.body.vendor,
        });
        await sparePart.save();
        return res.redirect(`/spareParts/view/${sparePart._id}`);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

exports.getEditSparePart = async (req, res, next) => {
    const sparePartId = req.params.sparePartId;
    try {
        const manufacturers = await Manufacturer.find();
        const sparePart = await SparePart.findById(sparePartId);
        if (!sparePart) {
            return res.redirect("/spareParts");
        }
        return res.render("sparePart/edit", {
            pageTitle: `Редактирование Запчасти | ${res.locals.userCompany.title.short}`,
            path: "/edit",
            sparePart: sparePart,
            manufacturers: manufacturers,
            errorMessage: alertMessage(req.flash("error")),
            successMessage: alertMessage(req.flash("success")),
            infoMessage: alertMessage(req.flash("info")),
            oldInput: {
                title: null,
                partNumber: null,
                serialNumber: null,
                vendor: null,
            },
        });
    } catch (err) {
        throw err;
    }
};

exports.postEditSparePart = async (req, res, next) => {
    const errors = validationResult(req);

    const sparePartId = req.body.sparePartId;

    if (!errors.isEmpty()) {
        try {
            const manufacturers = await Manufacturer.find();
            const sparePart = await SparePart.findById(sparePartId);
            return res.status(422).render("sparePart/edit", {
                path: "spareParts/edit",
                pageTitle: `Редактирование Запчасти | ${res.locals.userCompany.title.short}`,
                manufacturers: manufacturers,
                sparePart: sparePart,
                errorMessage: errors.array()[0].msg,
                successMessage: alertMessage(req.flash("success")),
                infoMessage: alertMessage(req.flash("info")),
                oldInput: {
                    title: req.body.title,
                    partNumber: req.body.partNumber,
                    serialNumber: req.body.serialNumber,
                    vendor: req.body.vendor,
                },
            });
        } catch (err) {
            throw err;
        }
    }

    try {
        const sparePart = await SparePart.findById(sparePartId);
        sparePart.title = req.body.title;
        sparePart.partNumber = req.body.partNumber;
        sparePart.serialNumber = req.body.serialNumber;
        sparePart.vendor = req.body.vendor;
        await sparePart.save();
        return res.redirect("/spareParts");
    } catch (err) {
        throw err;
    }
};

exports.getViewSparePart = async (req, res, next) => {
    try {
        const sparePart = await SparePart.findById(req.params.sparePartId);
        return res.render("sparePart/view", {
            sparePart: sparePart,
            pageTitle: `Просмотр Запчасти | ${res.locals.userCompany.title.short}`,
            path: "/spareParts/view",
        });
    } catch (err) {
        throw err;
    }
};

exports.postDeleteSparePart = async (req, res, next) => {
    const sparePartId = req.body.sparePartId;
    try {
        await SparePart.deleteOne({
            _id: sparePartId,
        });
        return res.redirect("/spareParts");
    } catch (err) {
        throw err;
    }
};
