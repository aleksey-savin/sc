const mongoose = require("mongoose");
const mongooseIncrement = require("mongoose-increment");
const increment = mongooseIncrement(mongoose);

const Schema = mongoose.Schema;

const orderSchema = new Schema(
    {
        customer: {
            individual: {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "Customer",
                    required: false,
                },
                firstName: {
                    type: String,
                    required: false,
                },
                lastName: {
                    type: String,
                    required: false,
                },
                middleName: {
                    type: String,
                    required: false,
                },
                phone: {
                    type: String,
                    required: false,
                },
                email: {
                    type: String,
                    required: false,
                },
                address: {
                    city: {
                        type: String,
                    },
                    street: {
                        type: String,
                    },
                    house: {
                        type: String,
                    },
                },
            },
            legalEntity: {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "LegalEntity",
                    required: false,
                },
                shortName: {
                    type: String,
                    required: false,
                },
                fullName: {
                    type: String,
                    required: false,
                },
                phone: {
                    type: String,
                    required: false,
                },
                email: {
                    type: String,
                    required: false,
                },
                address: {
                    city: {
                        type: String,
                    },
                    street: {
                        type: String,
                    },
                    house: {
                        type: String,
                    },
                },
                isDealer: Boolean,
            },
        },
        device: {
            _id: {
                type: Schema.Types.ObjectId,
                ref: "Device",
            },
            owner: {
                individual: {
                    _id: {
                        type: Schema.Types.ObjectId,
                        ref: "Customer",
                        required: false,
                    },
                    firstName: {
                        type: String,
                        required: false,
                    },
                    lastName: {
                        type: String,
                        required: false,
                    },
                    middleName: {
                        type: String,
                        required: false,
                    },
                    phone: {
                        type: String,
                        required: false,
                    },
                    email: {
                        type: String,
                        required: false,
                    },
                    address: {
                        type: String,
                        required: false,
                    },
                },
                legalEntity: {
                    _id: {
                        type: Schema.Types.ObjectId,
                        ref: "LegalEntity",
                        required: false,
                    },
                    shortName: {
                        type: String,
                        required: false,
                    },
                    fullName: {
                        type: String,
                        required: false,
                    },
                    phone: {
                        type: String,
                        required: false,
                    },
                    email: {
                        type: String,
                        required: false,
                    },
                    address: {
                        type: String,
                        required: false,
                    },
                },
            },
            type: {
                type: String,
                required: false,
            },
            manufacturer: {
                type: String,
                required: false,
            },
            model: {
                type: String,
                required: false,
            },
            itemCode: {
                type: String,
                required: false,
            },
            sn: {
                type: String,
                required: false,
            },
            prevSns: [{ type: String }],
            imei1: {
                type: String,
                required: false,
            },
            imei2: {
                type: String,
                required: false,
            },
            prevImeis: [{ type: String }],
            osVersion: {
                type: String,
                required: false,
            },
            dateOfSale: {
                type: Date,
                required: false,
            },
            state: {
                type: String,
                required: false,
            },
        },
        newDeviceIdentity: Boolean,
        type: {
            title: {
                type: String,
                required: false,
            },
            warranty: {
                type: Boolean,
                required: false,
            },
        },
        statedMalfunction: {
            type: String,
            required: true,
        },
        setStandard: {
            type: String,
            required: false,
        },
        deviceAppearance: {
            type: String,
            required: true,
        },
        providerOrderId: {
            type: String,
            required: false,
        },
        providerOrderState: {
            type: String,
            enum: ["Не создан", "Открыт", "Закрыт", "Не требуется"],
            required: false,
        },
        statedRepairTime: {
            type: String,
            required: false,
        },
        warrantyExtensionState: {
            type: String,
            enum: [
                "Не требуется",
                "Запросить документы",
                "Ждём документы",
                "Отправить запрос",
                "Ждём продления",
                "Продлена",
            ],
            required: false,
        },
        codes: {
            conditionCode: {
                code: {
                    type: String,
                    required: false,
                },
                title: {
                    type: String,
                    required: false,
                },
            },
            symptomCode: {
                code: {
                    type: String,
                    required: false,
                },
                title: {
                    type: String,
                    required: false,
                },
            },
            sectionCode: [
                {
                    code: {
                        type: String,
                        required: false,
                    },
                    title: {
                        type: String,
                        required: false,
                    },
                },
            ],
            defectCode: [
                {
                    code: {
                        type: String,
                        required: false,
                    },
                    title: {
                        type: String,
                        required: false,
                    },
                },
            ],
            repairCode: [
                {
                    code: {
                        type: String,
                        required: false,
                    },
                    title: {
                        type: String,
                        required: false,
                    },
                },
            ],
        },
        spareParts: [
            //Не сделано, пока не нужно
            {
                title: String,
                partNumber: {
                    type: String,
                    required: false,
                },
                state: {
                    type: String,
                    enum: [
                        "Не заказана",
                        "Заказана",
                        "На складе",
                        "Заменена",
                        "Не требуется",
                        "Прислана неисправной",
                        "Не использовалась",
                    ],
                    required: false,
                },
                repairCost: {
                    type: Number,
                    required: false,
                },
                primeCost: {
                    type: Number,
                    required: false,
                },
                terms: {
                    type: Date,
                    required: false,
                },
                DOA: {
                    type: Boolean,
                    required: false,
                },
                notUsed: {
                    type: Boolean,
                    required: false,
                },
            },
        ],
        payments: [
            //Нужно сделать как диагностику
            {
                amount: {
                    type: Number,
                    required: false,
                },
                method: {
                    type: String,
                    enum: [
                        "Банковская карта",
                        "Банковский перевод для юридических лиц",
                        "Наличные",
                        "Электронный платеж",
                    ],
                    required: false,
                },
                date: {
                    type: Date,
                    required: false,
                },
            },
        ],
        negotiation: {
            theme: {
                type: String,
                requried: false,
            },
            object: {
                type: String,
                enum: [
                    "",
                    "Конечный пользователь",
                    "Продавец",
                    "Производитель",
                    "Сервис-провайдер",
                ],
                required: false,
            },
            terms: {
                type: Date,
                required: false,
            },
        },
        issueNote: {
            //Комментарий от инженера при выдачи (текстовое поле ниже типа)
            type: String,
            required: false,
        },
        issueNoteSubmitted: Boolean,
        issueNoteSubmittedBy: {
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        state: {
            type: String,
            enum: [
                "Продление гарантии",
                "Диагностика",
                "Согласование",
                "Заказ запчастей",
                "Ожидание запчастей",
                "Выполнение ремонта",
                "В процессе списания",
                "Отказ в гарантии",
                // статусы для устройств на выдаче
                "Устройство списано",
                "Ремонт окончен",
                // Статусы для выданных устройств
                "Устройство выдано",
                "Выдан акт неремонтопригодности",
            ],
            required: true,
        },
        // Time & User stamps
        createdBy: {
            //Проверить работает ли
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
        updatedBy: {
            //Проверить работает ли
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
        diagnosedAt: {
            //Заполняется, когда несовпадение отправленного статуса со статусом из БД
            type: Date,
            required: false,
        },
        diagnosedBy: {
            //Заполняется, когда несовпадение отправленного статуса со статусом из БД
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        sentOffAt: {
            //Не сделано, пока не нужно
            type: Date,
            required: false,
        },
        sentOffBy: {
            //Не сделано, пока не нужно
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        completedAt: {
            //Заполняется, когда ремонт окончен
            type: Date,
            required: false,
        },
        completedBy: {
            //Заполняется, когда ремонт окончен
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        completionNotifiedAt: {
            //Не сделано, пока не нужно
            type: Date,
            required: false,
        },
        completionNotifiedBy: {
            //Не сделано, пока не нужно
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        issuedAt: {
            //Заполняется, когда устройство выдано или выдан акт неремонтопригодности
            //Когда устройство выдано, меняем статус устройства на "у клиента"
            type: Date,
            required: false,
        },
        issuedBy: {
            //Заполняется, когда устройство выдано или выдан акт неремонтопригодности
            //Когда устройство выдано, меняем статус устройства на "у клиента"
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        attachedFiles: [
            {
                longTitle: String,
                shortTitle: String,
                path: String,
            },
        ],
        rocketChat: {
            roomId: String,
        },
        history: [
            {
                begin: Date,
                state: String,
                by: {
                    _id: {
                        type: Schema.Types.ObjectId,
                        ref: "User",
                    },
                    firstName: String,
                    lastName: String,
                },
                parameter: {
                    title: String,
                    when: Date,
                    action: String,
                    by: {
                        _id: {
                            type: Schema.Types.ObjectId,
                            ref: "User",
                        },
                        firstName: String,
                        lastName: String,
                    },
                },
            },
        ],
    },
    {
        timestamps: true,
    }
);

orderSchema.plugin(increment, {
    modelName: "Order",
    fieldName: "orderNo",
    start: 32000,
    increment: 1,
});

module.exports = mongoose.model("Order", orderSchema);
