const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const manufacturerSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    isOfficiallyAuthorized: Boolean,
    nonRepairableActPrint: Boolean,
    warrantyExtensionProcedure: Boolean,
    trackSpareParts: Boolean,
});

module.exports = mongoose.model("Manufacturer", manufacturerSchema);
