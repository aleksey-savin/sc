const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderTypeSchema = new Schema({
    title: {
        type: String,
        unique: true,
        required: true,
    },
    warranty: {
        type: Boolean,
        required: false,
    },
    isActive: {
        type: Boolean,
        required: false,
    },
    isSystem: {
        type: Boolean,
        required: false,
    },
    priority: {
        type: Number,
        required: false
    }
});

module.exports = mongoose.model("OrderType", orderTypeSchema);
