const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const requestStateSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true,
    },
    isActive: {
        type: Boolean,
        required: false,
    },
    isSystem: {
        type: Boolean,
        required: false,
    },
    priority: {
        type: Number,
        required: false
    },
    alertLevels: {
        low: {            
            timeLimit: {
                type: Number,
                required: false,
            },
            target: {
                type: String,
                enum: ["Admin", "Manager", "Engineer", "User"],
                required: false,
            },
        },
        medium: {            
            timeLimit: {
                type: Number,
                required: false,
            },
            target: {
                type: String,
                enum: ["Admin", "Manager", "Engineer", "User"],
                required: false,
            },
        },
        high: {           
            timeLimit: {
                type: Number,
                required: false,
            },
            target: {
                type: String,
                enum: ["Admin", "Manager", "Engineer", "User"],
                required: false,
            },
        },
    },
});

module.exports = mongoose.model("RequestState", requestStateSchema);
