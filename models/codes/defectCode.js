const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const defectCodeSchema = new Schema({
    code: {
        type: String,
        required: false,
    },
    title: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model("DefectCode", defectCodeSchema);