const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const conditionCodeSchema = new Schema({
    code: {
        type: String,
        required: false,
    },
    title: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model("ConditionCode", conditionCodeSchema);
