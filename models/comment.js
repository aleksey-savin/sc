const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const commentSchema = new Schema(
    {
        belongsTo: {
            entity: {
                type: String,
                required: true,
            },
            id: {
                type: Schema.Types.ObjectId,
                required: true,
            },
        },
        commentBody: {
            type: String,
            required: true,
        },
        createdBy: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Comment", commentSchema);
