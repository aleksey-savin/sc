const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const legalEntitySchema = new Schema(
    {
        shortName: {
            type: String,
            required: true,
        },
        fullName: {
            type: String,
            required: false,
        },
        city: {
            type: String,
            required: false,
        },
        street: {
            type: String,
            required: false,
        },
        house: {
            type: String,
            required: false,
        },
        phone: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: false,
        },
        isDealer: {
            type: Boolean,
            required: false,
        },
        contactList: [
            {
                name: {
                    first: {
                        type: String,
                        required: true,
                    },
                    middle: {
                        type: String,
                        required: false,
                    },
                    last: {
                        type: String,
                        required: true,
                    },
                },
                contacts: {
                    phone: {
                        type: String,
                        required: true,
                    },
                    email: {
                        type: String,
                        required: false,
                    },
                },
                position: {
                    type: String,
                    required: true,
                },
            },
        ],
        // User stamps
        createdBy: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
        updatedBy: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("LegalEntity", legalEntitySchema);
