const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const sparePartsSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    partNumber: {
        type: String,
        required: false,
    },
    serialNumber: {
        type: String,
        required: false,
    },
    vendor: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model("SpareParts", sparePartsSchema);
