const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderFilterSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    isShared: {
        type: Boolean,
        required: false,
    },
    setByDefault: {
        type: Boolean,
        required: false,
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: false,
    },
    parameters: {
        orderState: [String],
        orderType: [String],
        providerOrderId: String,
        orderNo: Number,
        orderLegalEntityCustomer: String,
        orderIndividualCustomer: String,
        orderCreatedAt: {
            full: String,
            start: String,
            end: String,
        },
        orderDiagnosedAt: {
            full: String,
            start: String,
            end: String,
        },
        orderCompletedAt: {
            full: String,
            start: String,
            end: String,
        },
        orderIssuedAt: {
            full: String,
            start: String,
            end: String,
        },
        orderCreatedBy: [String],
        orderDiagnosedBy: [String],
        orderCompletedBy: [String],
        orderIssuedBy: [String],
        deviceType: [String],
        deviceManufacturer: [String],
        deviceModel: String,
        deviceItemCode: String,
        deviceSN: String,
        deviceIMEI: String,
        deviceOSVersion: String,
        deviceState: [String],
        deviceLegalEntityOwner: String,
        deviceIndividualOwner: String,
    },
});

module.exports = mongoose.model("OrderFilter", orderFilterSchema);
