const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const myCompanySchema = new Schema({
    title: {
        short: {
            type: String,
            required: true,
        },
        full: {
            type: String,
            required: true,
        },
    },
    address: {
        city: {
            type: String,
            required: true,
        },
        street: {
            type: String,
            required: true,
        },
        house: {
            type: String,
            required: true,
        },
        office: {
            type: String,
            required: false,
        },
    },
    contacts: {
        phone: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        webSite: {
            type: String,
            required: true,
        },
        telegramId: {
            type: String,
            required: false,
        },
    },
    apiKeys: {
        mailgun: String,
        smsRu: String,
        telegram: String,
    },
});

module.exports = mongoose.model("MyCompany", myCompanySchema);
