const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const customerSchema = new Schema({
    name: {
        first: {
            type: String,
            required: true
        },
        middle: {
            type: String,
            required: false
        },
        last: {
            type: String,
            required: true
        }
    },
    address: {
        city: {
            type: String,
            required: false
        },
        street: {
            type: String,
            required: false
        },
        house: {
            type: String,
            required: false
        }
    },
    contacts: {
        phone: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: false
        }
    },
    silentMode: {
        type: Boolean,
        required: false
    },
    legalEntityId: {
        type: Schema.Types.ObjectId,
        ref: "LegalEntity",
        required: false
    },

    // Time & User stamps
    createdAt: {
        type: Date,
        required: false
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: false
    },
    updatedAt: {
        type: Date,
        required: false
    },
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: false
    }
});

module.exports = mongoose.model("Customer", customerSchema);
