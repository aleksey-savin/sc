const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: false,
    },
    password: {
        //user.company.title.full
        type: String,
        required: true,
    },
    name: {
        first: {
            type: String,
            required: false,
        },
        last: {
            type: String,
            required: false,
        },
    },
    role: {
        type: String,
        enum: ["Admin", "Manager", "Engineer", "User"],
        required: false,
    },
    company: {
        _id: {
            type: Schema.Types.ObjectId,
            ref: "MyCompany",
            required: false,
        },
        title: {
            full: {
                type: String,
                required: false,
            },
            short: {
                type: String,
                required: false,
            },
        },
    },
    darkTheme: {
        type: Boolean,
        required: false,
    },
    active: Boolean,
    verifyCode: Number,
    verifyCodeExpiration: Date,
    resetToken: String,
    resetTokenExpiration: Date,
});

module.exports = mongoose.model("User", userSchema);
