const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const deviceSchema = new Schema(
    {
        owner: {
            individual: {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "Customer",
                    required: false,
                },
                firstName: {
                    type: String,
                    required: false,
                },
                lastName: {
                    type: String,
                    required: false,
                },
                middleName: {
                    type: String,
                    required: false,
                },
                phone: {
                    type: String,
                    required: false,
                },
                email: {
                    type: String,
                    required: false,
                },
            },
            legalEntity: {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "LegalEntity",
                    required: false,
                },
                shortName: {
                    type: String,
                    required: false,
                },
                fullName: {
                    type: String,
                    required: false,
                },
                phone: {
                    type: String,
                    required: false,
                },
                email: {
                    type: String,
                    required: false,
                },
            },
        },
        manufacturer: {
            type: String,
            required: false,
        },
        type: {
            type: String,
            required: true,
        },
        model: {
            type: String,
            required: true,
        },
        itemCode: {
            type: String,
            required: false,
        },
        sn: {
            type: String,
            required: true,
        },
        imei1: {
            type: String,
            required: false,
        },
        imei2: {
            type: String,
            required: false,
        },
        osVersion: {
            type: String,
            required: false,
        },
        dateOfSale: {
            type: Date,
            required: false,
        },
        prevItemCodes: [
            {
                type: String,
                required: false,
            },
        ],
        prevSns: [
            {
                type: String,
                required: false,
            },
        ],
        prevImeis: [ 
            {
                type: String,
                required: false,
            },
        ],
        prevOSVersions: [
            {
                type: String,
                required: false,
            },
        ],
        orderNo: {
            type: String,
            required: false
        },
        state: {
            type: String,
            enum: [
                "Подменный фонд - устройство на складе",
                "Подменный фонд - устройство у Клиента",
                "У Клиента",
                "На складе СЦ",  
                "Отправлено производителю или сервис-провайдеру",              
                "Готово к выдаче",          
                "Списано",                
                "Клиент не забирает",                
            ],
            required: true,
        },
        // User stamps
        createdBy: {
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
        updatedBy: {
            type: Schema.Types.ObjectId,
            ref: "User",
            required: false,
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Device", deviceSchema);
