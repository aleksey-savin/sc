const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const requestSchema = new Schema(
    {
        client: {
            firstName: {
                type: String,
                required: false,
            },
            lastName: {
                type: String,
                required: false,
            },
            middleName: {
                type: String,
                required: false,
            },
            phone: {
                type: String,
                required: true,
            },
            email: {
                type: String,
                required: false,
            },
        },
        device: {
            manufacturer: {
                type: String,
                required: false,
            },
            deviceType: {
                type: String,
                required: true,
            },
            model: {
                type: String,
                required: true,
            },
        },
        repair: {
            statedMalfunction: {
                type: String,
                required: true,
            },
            repairCost: {
                type: String,
                required: false,
            },
            sparePartAvailability: {
                type: String,
                enum: [
                    "Не требуется",
                    "На складе",
                    "Под заказ у оф. поставщика",
                    "GSM",
                    "Aliexpress",
                    "Другое",
                ],
                required: false,
            },
            repairTerms: {
                type: String,
                required: false,
            },
        },
        requestState: {
            type: String,
            required: true,
        },
        alertLevels: {
            low: {
                type: Number, 
            },
            medium: {
                type: Number,
            },
            high: {
                type: Number,
            },
        },
        createdBy: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
        updatedBy: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
        inStateSince: {
            type: Date,
            required: true,
        },        
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Request", requestSchema);
