const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const postsSchema = new Schema(
    {
        title: {
            type: String,
            required: true,
        },
        body: String,
        category: {
            type: String,
            enum: [
                "Портал | Новые фичи",
                "Портал | Исправление ошибок",
                "Новости компании",
                "Новости Huawei | Honor",
                "Новости Lenovo",
                "Новости AOC | Philips",
            ],
        },
        createdBy: {
            id: {
                type: Schema.Types.ObjectId,
                ref: "User",
                required: false,
            },
            name: {
                first: {
                    type: String,
                    required: false,
                },
                last: {
                    type: String,
                    required: false,
                },
            },
        },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Post", postsSchema);
