const path = ".modal-body > form > ";

// Автозаполнение поля Физическое лицо. Код какого-то типа с ютубчика, переписать по-человечьи на js
$(function () {
    $(path + "div > div > #individualOwner").autocomplete({
        source: function (req, res, next) {
            $.ajax({
                url: "/customers/autocomplete",
                dataType: "jsonp",
                type: "GET",
                data: req,
                success: function (data) {
                    res(data);
                },
                error: function (err) {
                    console.log(err.status);
                },
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item) {
                $(path + "div > div > #individualOwner").text(ui.item.label);
                $(path + "div > div > #individualOwnerId").val(ui.item.id);
            }
        },
    });
});

// Автозаполнение поля Юридическое лицо. Код какого-то типа с ютубчика, переписать по-человечьи на js
$(function () {
    $(path + "div > div > #legalEntityOwner").autocomplete({
        source: function (req, res, next) {
            $.ajax({
                url: "/legal-entities/autocomplete",
                dataType: "jsonp",
                type: "GET",
                data: req,
                success: function (data) {
                    res(data);
                },
                error: function (err) {
                    console.log(err.status);
                },
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item) {
                $(path + "div > div > #legalEntityOwner").text(ui.item.label);
                $(path + "div > div > #legalEntityOwnerId").val(ui.item.id);
            }
        },
    });
});

// Автозаполнение поля Производитель. Код какого-то типа с ютубчика, переписать по-человечьи на js
$(function () {
    $(path + "div > div > #manufacturer").autocomplete({
        source: function (req, res, next) {
            $.ajax({
                url: "/manufacturers/autocomplete",
                dataType: "jsonp",
                type: "GET",
                data: req,
                success: function (data) {
                    res(data);
                },
                error: function (err) {
                    console.log(err.status);
                },
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item) {
                $(path + "div > div > #manufacturer").text(ui.item.label);
                $(path + "div > div > #manufacturer").val(ui.item.label);
            }
        },
    });
});

// autocomplete1($(path + "div > div > #manufacturer"), "manufacturers");

// Автозаполнение поля Тип Устройства. Код какого-то типа с ютубчика, переписать по-человечьи на js
$(function () {
    $(path + "div > div > #type").autocomplete({
        source: function (req, res, next) {
            $.ajax({
                url: "/device-types/autocomplete",
                dataType: "jsonp",
                type: "GET",
                data: req,
                success: function (data) {
                    res(data);
                },
                error: function (err) {
                    console.log(err.status);
                },
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item) {
                $(path + "div > div > #type").text(ui.item.label);
                $(path + "div > div > #type").val(ui.item.label);
            }
        },
    });
});