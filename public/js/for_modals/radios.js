// Корректное переключение радио ownerRadios
if (document.querySelector(path + "div > div > #legalEntityOwner")) {
    const legalEntityOwnerRadioModal = document.querySelector(
        path + "div > div > div > #legalEntityOwnerRadioModal"
    );
    const individualOwnerRadioModal = document.querySelector(
        path + "div > div > div > #individualOwnerRadioModal"
    );
    const individualOwnerFormModal = document.querySelector(
        path + "#individualOwnerForm"
    );
    const legalEntityOwnerFormModal = document.querySelector(
        path + "#legalEntityOwnerForm"
    );
    legalEntityOwnerRadioModal.addEventListener("click", () => {
        legalEntityOwnerRadioModal.setAttribute("checked", "");
        individualOwnerRadioModal.removeAttribute("checked", "");
        individualOwnerFormModal.setAttribute("hidden", "");
        legalEntityOwnerFormModal.removeAttribute("hidden", "");
    });
    individualOwnerRadioModal.addEventListener("click", () => {
        individualOwnerRadioModal.setAttribute("checked", "");
        legalEntityOwnerRadioModal.removeAttribute("checked", "");

        legalEntityOwnerFormModal.setAttribute("hidden", "");
        individualOwnerFormModal.removeAttribute("hidden", "");
    });
}