const dropArea = {
    dropElement: document.querySelector(".dropArea > div > div"),
    inputElement: document.querySelector(".dropArea > div > div > input"),
    oldFiles: [],
    convertSize: (bytes) => {
        if (!bytes) return '0 Bytes';

        const k = 1024;
        const sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    },
    initialize: () => {
        //VISUAL
        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropArea.dropElement.addEventListener(eventName, e => {
                e.preventDefault();
                e.stopPropagation();
            });
        });

        ['dragenter', 'dragover'].forEach(eventName => {
            dropArea.dropElement.addEventListener(eventName, () => dropArea.dropElement.classList.add('highlight'));
        });

        ['dragleave', 'drop'].forEach(eventName => {
            dropArea.dropElement.addEventListener(eventName, e => dropArea.dropElement.classList.remove('highlight'));
        });

        //FUNCTIONALITY
        dropArea.dropElement.addEventListener("click", () => dropArea.inputElement.click());
        dropArea.dropElement.addEventListener("drop", e => dropArea.handleFiles(e.dataTransfer));
        dropArea.inputElement.addEventListener("change", () => dropArea.handleFiles(dropArea.inputElement));
    },
    handleFiles: source => {
        let newFiles = source.files;

        let newData = new DataTransfer();

        ([...dropArea.oldFiles]).forEach(file => newData.items.add(file));
        ([...newFiles]).forEach(file => {
            newData.items.add(file);

            document.querySelector(".dropArea > div > div > p").className = "dissapeared";

            const fileElement = document.createElement("div");
            fileElement.className = "uploadedFile";

            const sizeInfo = document.createElement("p");
            const nameInfo = document.createElement("p");
            const deleteElement = document.createElement("div");
            sizeInfo.textContent = dropArea.convertSize(file.size);
            nameInfo.textContent = file.name;
            deleteElement.className = "deleteFile";
            deleteElement.textContent = "delete";
            deleteElement.addEventListener("click", e => dropArea.deleteFile(fileElement, e));
            
            fileElement.append(nameInfo);
            fileElement.append(sizeInfo);
            fileElement.append(deleteElement);
            dropArea.dropElement.append(fileElement);
        });

        dropArea.oldFiles = newData.files;
        dropArea.inputElement.files = newData.files;
    },
    deleteFile: (fileElement, e) => {
        e.preventDefault();
        e.stopPropagation();

        let number;

        const fileElements = document.querySelectorAll(".dropArea > div > div > div.uploadedFile");
        for (let i = 0; i < fileElements.length; i++) {
            if (fileElement == fileElements[i]) {
                fileElement.parentNode.removeChild(fileElement);
                number = i;
            }
        }
        
        let newData = new DataTransfer();
        for (let i = 0; i < dropArea.oldFiles.length; i++) {
            if (i == number) continue;
            newData.items.add([...dropArea.oldFiles][i]);
        }

        dropArea.oldFiles = newData.files;
        dropArea.inputElement.files = newData.files;

        if(dropArea.oldFiles.length == 0) document.querySelector(".dropArea > div > div > p").className = "";
    },
};

dropArea.initialize();