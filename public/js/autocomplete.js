// Автозаполнение поля Физическое лицо -------------------------
autocomplete(
    $("#individualOwner"),
    "customers",
    $("#individualOwner"),
    $("#individualOwnerId"),
    true
);

// Автозаполнение поля Юридическое лицо -------------------------
autocomplete(
    $("#legalEntityOwner"),
    "legal-entities",
    $("#legalEntityOwner"),
    $("#legalEntityOwnerId"),
    true
);

// Автозаполнение поля Физическое лицо, сдавшее устройство в ремонт -------------------------
autocomplete(
    $("#individualCustomer"),
    "customers",
    $("#individualCustomer"),
    $("#individualCustomerId"),
    true
);

// Автозаполнение поля Юридическое лицо, сдавшее устройство в ремонт -------------------------
autocomplete(
    $("#legalEntityCustomer"),
    "legal-entities",
    $("#legalEntityCustomer"),
    $("#legalEntityCustomerId"),
    true
);

// Автозаполнение поля Производитель
autocomplete($("#manufacturer"), "manufacturers");

// Автозаполнение поля Устройство -------------------------
autocomplete($("#device"), "devices", $("#device"), $("#deviceId"), true);

// Автозаполнение поля Тип Устройства
autocomplete($("#deviceType"), "device-types", $("#type"), $("#type"));

// Автозаполнение поля Кода Условия
autocomplete($("#conditionCodeTitle"), "condition-codes");

// Автозаполнение поля Кода Ремонта
for (let i = 0; i < 11; i++)
    autocomplete($(`#repairCodeTitle${i}`), "repair-codes");

// Автозаполнение поля Кода Секции
autocomplete($("#sectionCodeTitle0"), "section-codes");

// Автозаполнение поля Кода Симптома
autocomplete($("#symptomCodeTitle"), "symptom-codes");

// Автозаполнение поля Кода Дефекта
autocomplete($("#defectCodeTitle0"), "defect-codes");

// Быстрый поиск
// autocomplete($("#searchParams"), "filtered");

// // Автозаполнение единого клиента в массовой выдачи. Код какого-то типа с ютубчика, переписать по-человечьи на js
// $(() => {
//     $("#transferCustomer").autocomplete({
//         source: (req, res, next) => {
//             $.ajax({
//                 url: "/customers/autocomplete",
//                 dataType: "jsonp",
//                 type: "GET",
//                 data: req,
//                 success: (data) => {
//                     res(data);
//                 },
//                 error: (err) => {
//                     console.log(err.status);
//                 },
//             });
//         },
//         minLength: 1,
//         select: (event, ui) => {
//             if (ui.item) {
//                 let el = document.getElementById("#transferCustomer");
//                 el.textContent = ui.item.label;
//             }
//         },
//     });
// });

// Код какого-то типа с ютубчика, переписать по-человечьи на js
function autocomplete(el, link, elForText = el, elForVal = el, forId = false) {
    el.autocomplete({
        source: (req, res, next) => {
            $.ajax({
                url: `/${link}/autocomplete`,
                dataType: "jsonp",
                type: "GET",
                data: req,
                success: function (data) {
                    res(data);
                },
                error: function (err) {
                    console.log(err.status);
                },
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item) {
                elForText.text(ui.item.label);

                if (forId)
                    elForVal.val(ui.item.id);
                else
                    elForVal.val(ui.item.label);
            }
        },
    });
}
