// Корректное переключение чекбокса saveSelector (указатель на то, что надо сохранить фильтр по ремонтам)
if (document.querySelector("#saveOrderFilter")) {
    const saveSelector = document.querySelector("#saveSelector");
    const saveOrderFilter = document.querySelector("#saveOrderFilter");
    const notSaveOrderFilter = document.querySelector("#notSaveOrderFilter");
    const filterTitle = document.querySelector("#filterTitle");
    saveOrderFilter.addEventListener("click", () => {
        saveSelector.value = "yes";
        filterTitle.required = true;
    });
    notSaveOrderFilter.addEventListener("click", () => {
        saveSelector.value = "no";
        filterTitle.required = false;
    });
}

// Подключаем daterangepicker для возможности выбора периодов
$(function () {
    $('input[name="orderCreatedAt"]').daterangepicker({ 
        autoUpdateInput: false,
        locale: {
            cancelLabel: "Clear",
        },
    });
    $('input[name="orderCreatedAt"]').on("apply.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val(
            picker.startDate.format("DD-MM-YYYY") +
                " - " +
                picker.endDate.format("DD-MM-YYYY")
        );
    });
    $('input[name="orderCreatedAt"]').on("cancel.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val("");
    });
    $('input[name="orderDiagnosedAt"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: "Clear",
        },
    });
    $('input[name="orderDiagnosedAt"]').on("apply.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val(
            picker.startDate.format("DD-MM-YYYY") +
                " - " +
                picker.endDate.format("DD-MM-YYYY")
        );
    });
    $('input[name="orderDiagnosedAt"]').on("cancel.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val("");
    });
    $('input[name="orderCompletedAt"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: "Clear",
        },
    });
    $('input[name="orderCompletedAt"]').on("apply.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val(
            picker.startDate.format("DD-MM-YYYY") +
                " - " +
                picker.endDate.format("DD-MM-YYYY")
        );
    });
    $('input[name="orderCompletedAt"]').on("cancel.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val("");
    });
    $('input[name="orderIssuedAt"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: "Clear",
        },
    });
    $('input[name="orderIssuedAt"]').on("apply.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val(
            picker.startDate.format("DD-MM-YYYY") +
                " - " +
                picker.endDate.format("DD-MM-YYYY")
        );
    });
    $('input[name="orderIssuedAt"]').on("cancel.daterangepicker", function (
        ev,
        picker
    ) {
        $(this).val("");
    });
});

$("#orderCreatedAt").on("apply.daterangepicker", (ev, picker) => {
    const orderCreatedAtStart = document.querySelector("#orderCreatedAtStart");
    const orderCreatedAtEnd = document.querySelector("#orderCreatedAtEnd");
    orderCreatedAtStart.setAttribute(
        "value",
        picker.startDate.format("DD-MM-YYYY")
    );
    orderCreatedAtEnd.setAttribute(
        "value",
        picker.endDate.format("DD-MM-YYYY")
    );
});

$("#orderDiagnosedAt").on("apply.daterangepicker", function (ev, picker) {
    const orderDiagnosedAtStart = document.querySelector(
        "#orderDiagnosedAtStart"
    );
    const orderDiagnosedAtEnd = document.querySelector("#orderDiagnosedAtEnd");
    orderDiagnosedAtStart.setAttribute(
        "value",
        picker.startDate.format("DD-MM-YYYY")
    );
    orderDiagnosedAtEnd.setAttribute(
        "value",
        picker.endDate.format("DD-MM-YYYY")
    );
});

$("#orderCompletedAt").on("apply.daterangepicker", function (ev, picker) {
    const orderCompletedAtStart = document.querySelector(
        "#orderCompletedAtStart"
    );
    const orderCompletedAtEnd = document.querySelector("#orderCompletedAtEnd");
    orderCompletedAtStart.setAttribute(
        "value",
        picker.startDate.format("DD-MM-YYYY")
    );
    orderCompletedAtEnd.setAttribute(
        "value",
        picker.endDate.format("DD-MM-YYYY")
    );
});

$("#orderIssuedAt").on("apply.daterangepicker", function (ev, picker) {
    const orderIssuedAtStart = document.querySelector("#orderIssuedAtStart");
    const orderIssuedAtEnd = document.querySelector("#orderIssuedAtEnd");
    orderIssuedAtStart.setAttribute(
        "value",
        picker.startDate.format("DD-MM-YYYY")
    );
    orderIssuedAtEnd.setAttribute("value", picker.endDate.format("DD-MM-YYYY"));
});

// очистка всех полей фильтра
const clearAll = document.querySelector("#clearAll");
clearAll.addEventListener("click", () => {
    document.querySelector("#deviceModel").value = "";
    document.querySelector("#deviceItemCode").value = "";
    document.querySelector("#deviceSN").value = "";
    document.querySelector("#deviceIMEI").value = "";
    document.querySelector("#deviceOSVersion").value = "";
    document.querySelector("#providerOrderId").value = "";
    document.querySelector("#orderNo").value = "";
    document.querySelector("#orderCreatedAt").value = "";
    document.querySelector("#orderCreatedAtStart").value = "";
    document.querySelector("#orderCreatedAtEnd").value = "";
    document.querySelector("#orderDiagnosedAt").value = "";
    document.querySelector("#orderDiagnosedAtStart").value = "";
    document.querySelector("#orderDiagnosedAtEnd").value = "";
    document.querySelector("#orderCompletedAt").value = "";
    document.querySelector("#orderCompletedAtStart").value = "";
    document.querySelector("#orderCompletedAtEnd").value = "";
    document.querySelector("#orderIssuedAt").value = "";
    document.querySelector("#orderIssuedAtStart").value = "";
    document.querySelector("#orderIssuedAtEnd").value = "";
    document.querySelector("#orderCreatedBy").value = "";
    document.querySelector("#orderDiagnosedBy").value = "";
    document.querySelector("#orderCompletedBy").value = "";
    document.querySelector("#orderIssuedBy").value = "";
    // deselect all selectpickers
    $(".selectpicker").selectpicker("deselectAll");
    document.querySelector("#individualOwner").value = "";
    document.querySelector("#individualOwnerId").value = "";
    document.querySelector("#legalEntityOwner").value = "";
    document.querySelector("#legalEntityOwnerId").value = "";
    document.querySelector("#individualCustomer").value = "";
    document.querySelector("#individualCustomerId").value = "";
    document.querySelector("#legalEntityCustomer").value = "";
    document.querySelector("#legalEntityCustomerId").value = "";
});

// Корректное переключение радио ownerRadios
if (document.querySelector("#legalEntityOwner")) {
    const legalEntityOwnerRadio = document.querySelector(
        "#legalEntityOwnerRadio"
    );
    const individualOwnerRadio = document.querySelector(
        "#individualOwnerRadio"
    );
    const individualOwnerForm = document.querySelector("#individualOwnerForm");
    const individualOwner = document.querySelector("#individualOwner");
    const individualOwnerId = document.querySelector("#individualOwnerId");
    const legalEntityOwner = document.querySelector("#legalEntityOwner");
    const legalEntityOwnerId = document.querySelector("#legalEntityOwnerId");
    const legalEntityOwnerForm = document.querySelector(
        "#legalEntityOwnerForm"
    );
    legalEntityOwnerRadio.addEventListener("click", () => {
        legalEntityOwnerRadio.setAttribute("checked", "");
        individualOwnerRadio.removeAttribute("checked", "");
        individualOwner.value = "";
        individualOwnerId.value = "";
        individualOwnerForm.setAttribute("hidden", "");
        legalEntityOwnerForm.removeAttribute("hidden", "");
    });
    individualOwnerRadio.addEventListener("click", () => {
        individualOwnerRadio.setAttribute("checked", "");
        legalEntityOwnerRadio.removeAttribute("checked", "");
        legalEntityOwner.value = "";
        legalEntityOwnerId.value = "";
        legalEntityOwnerForm.setAttribute("hidden", "");
        individualOwnerForm.removeAttribute("hidden", "");
    });
}

// Корректное переключение радио customerRadios
if (document.querySelector("#legalEntityCustomer")) {
    const legalEntityRadio = document.querySelector(
        "#legalEntityCustomerRadio"
    );
    const individualRadio = document.querySelector("#individualCustomerRadio");
    const individualCustomerForm = document.querySelector(
        "#individualCustomerForm"
    );
    const individualCustomer = document.querySelector("#individualCustomer");
    const individualCustomerId = document.querySelector(
        "#individualCustomerId"
    );
    const legalEntityCustomer = document.querySelector("#legalEntityCustomer");
    const legalEntityCustomerId = document.querySelector(
        "#legalEntityCustomerId"
    );
    const legalEntityCustomerForm = document.querySelector(
        "#legalEntityCustomerForm"
    );
    legalEntityRadio.addEventListener("click", () => {
        legalEntityRadio.setAttribute("checked", "");
        individualRadio.removeAttribute("checked", "");
        individualCustomer.value = "";
        individualCustomerId.value = "";
        individualCustomerForm.setAttribute("hidden", "");
        legalEntityCustomerForm.removeAttribute("hidden", "");
    });
    individualRadio.addEventListener("click", () => {
        individualRadio.setAttribute("checked", "");
        legalEntityRadio.removeAttribute("checked", "");
        legalEntityCustomer.value = "";
        legalEntityCustomerId.value = "";
        legalEntityCustomerForm.setAttribute("hidden", "");
        individualCustomerForm.removeAttribute("hidden", "");
    });
}

// Корректное переключение чекбокса isShared
if (document.querySelector("#isShared")) {
    const isShared = document.querySelector("#isShared");
    isShared.addEventListener("click", () => {
        isShared.value === "off"
            ? (isShared.value = "on")
            : (isShared.value = "off");
    });
}

// Корректное переключение чекбокса setByDefault
if (document.querySelector("#setByDefault")) {
    const setByDefault = document.querySelector("#setByDefault");
    setByDefault.addEventListener("click", () => {
        setByDefault.value === "off"
            ? (setByDefault.value = "on")
            : (setByDefault.value = "off");
    });
}

//подставляем id в ссылку
if (document.querySelector("#orderFilter")) {
    const selectedSavedFilter = document.querySelector("#orderFilter");
    const link = document.querySelector("#applySavedFilter");
    link.addEventListener("click", () => {
        link.getAttribute("href");
        link.setAttribute(
            "href",
            `/orders/filtered/${selectedSavedFilter.value}`
        );
    });
}
