document.querySelector("input[id='emptyPhone']").addEventListener("click", () => {
    const el = document.querySelector("input[id='phone']");

    el.required = !el.required;
    el.readOnly = !el.readOnly;
});