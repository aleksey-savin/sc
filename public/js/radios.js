// Корректное переключение радио ownerRadios
if (document.querySelector("#legalEntityOwner")) {
    const legalEntityOwnerRadio = document.querySelector(
        "#legalEntityOwnerRadio"
    );
    const individualOwnerRadio = document.querySelector(
        "#individualOwnerRadio"
    );
    const individualOwnerForm = document.querySelector("#individualOwnerForm");
    const legalEntityOwnerForm = document.querySelector(
        "#legalEntityOwnerForm"
    );
    const individualOwner = document.querySelector("#individualOwner");
    const legalEntityOwner = document.querySelector("#legalEntityOwner");

    legalEntityOwnerRadio.addEventListener("click", () => {
        legalEntityOwnerRadio.setAttribute("checked", "");
        individualOwnerRadio.removeAttribute("checked", "");
        individualOwnerForm.setAttribute("hidden", "");        
        individualOwner.removeAttribute("required", "");
        legalEntityOwnerForm.removeAttribute("hidden", "");
        legalEntityOwner.setAttribute("required", "");

    });
    individualOwnerRadio.addEventListener("click", () => {
        individualOwnerRadio.setAttribute("checked", "");
        legalEntityOwnerRadio.removeAttribute("checked", "");
        legalEntityOwnerForm.setAttribute("hidden", "");
        legalEntityOwner.removeAttribute("required", "");
        individualOwnerForm.removeAttribute("hidden", "");
        individualOwner.setAttribute("required", "");
    });
}

// Корректное переключение радио customerRadios
if (document.querySelector("#legalEntityCustomer")) {
    const legalEntityRadio = document.querySelector(
        "#legalEntityCustomerRadio"
    );
    const individualRadio = document.querySelector("#individualCustomerRadio");
    const individualCustomerForm = document.querySelector(
        "#individualCustomerForm"
    );
    const legalEntityCustomerForm = document.querySelector(
        "#legalEntityCustomerForm"
    );
    legalEntityRadio.addEventListener("click", () => {
        legalEntityRadio.setAttribute("checked", "");
        individualRadio.removeAttribute("checked", "");

        individualCustomerForm.setAttribute("hidden", "");
        legalEntityCustomerForm.removeAttribute("hidden", "");
    });
    individualRadio.addEventListener("click", () => {
        individualRadio.setAttribute("checked", "");
        legalEntityRadio.removeAttribute("checked", "");

        legalEntityCustomerForm.setAttribute("hidden", "");
        individualCustomerForm.removeAttribute("hidden", "");
    });
}
