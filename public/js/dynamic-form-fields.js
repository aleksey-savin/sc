const addCode = document.querySelector("#addCode");
const removeCode = document.querySelector("#removeCode");
const container = document.getElementById("codeFormGroup");
let j = container.childElementCount - 1;
let paymentsQuantity = document.getElementById("paymentFormGroup").childElementCount;
const initialDefectQueryLength = document.getElementsByClassName("defect-code").length;
const sectionCodeOptions = [];
const defectCodeOptions = [];

document.querySelectorAll("#sectionCodeTitle0 > option").forEach(option => {
    sectionCodeOptions.push(option.cloneNode(true));
});
document.querySelectorAll("#defectCodeTitle0 > option").forEach(option => {
    defectCodeOptions.push(option.cloneNode(true));
});

addCode.addEventListener("click", () => {
    const row = addElement("div", "row");

    const col1 = addElement("div", "col-4");
    const col2 = addElement("div", "col-4");
    const col3 = addElement("div", "col-4");  

    const sectionCode = document.createElement("select");
    sectionCode.name = "sectionCodeTitle" + j;
    sectionCode.id = "sectionCodeTitle" + j;
    sectionCode.dataset.liveSearch = true;
    sectionCode.dataset.noneSelectedText = "Поиск...";

    sectionCodeOptions.forEach(option => {
        sectionCode.appendChild(option);
    });
    sectionCode.selectedIndex = 0;

    const defectCode = document.createElement("select");
    defectCode.name = "defectCodeTitle" + j;
    defectCode.id = "defectCodeTitle" + j;
    defectCode.dataset.liveSearch = true;
    defectCode.dataset.noneSelectedText = "Поиск...";

    defectCodeOptions.forEach(option => {
        defectCode.appendChild(option);
    });
    defectCode.selectedIndex = 0;

    const sparePart = addElement(
        "input",
        "form-control spare-code",
        "sparePart" + j,
        "Поиск...",
        "margin-bottom: 1rem;"
    );  

    row.appendChild(col1).appendChild(sectionCode);
    row.appendChild(col2).appendChild(defectCode);   
    row.appendChild(col3).appendChild(sparePart);    

    container.appendChild(row);

    $("#sectionCodeTitle" + j).selectpicker();
    $("#defectCodeTitle" + j).selectpicker();

    j++;
});

removeCode.addEventListener("click", () => {
    if (j > initialDefectQueryLength) {
        const containers = document.querySelectorAll("#codeFormGroup");
        containers.forEach(container => {
            container.removeChild(container.lastElementChild);
        });
        j--;
    }
});

document.getElementById("addPayment").addEventListener("click", () => {
    const row = addElement("div", "row");

    const col1 = addElement("div", "col-4");
    const col2 = addElement("div", "col-4");
    const col3 = addElement("div", "col-4");

    const paymentAmount = addElement(
        "input",
        "form-control",
        "paymentAmount" + paymentsQuantity,
        "",
        "margin-bottom: 1rem;"
    );

    const paymentMethod = addElement(
        "select",
        "custom-select",
        "paymentMethod" + paymentsQuantity,
        "",
        "margin-bottom: 1rem;"
    );

    const paymentDate = addElement(
        "input",
        "form-control",
        "paymentDate" + paymentsQuantity,
        "",
        "margin-bottom: 1rem;",
        "date",
    );

    for (let i = 0; i < document.getElementById("paymentMethod0").childElementCount; i++) {
        let opt = document.createElement("option");
        opt.text = document.getElementById("paymentMethod0").options[i].text;
        paymentMethod.add(opt);
    }

    row.appendChild(col1).appendChild(paymentAmount);
    row.appendChild(col2).appendChild(paymentMethod);
    row.appendChild(col3).appendChild(paymentDate);
    
    document.getElementById("paymentFormGroup").appendChild(row);

    paymentsQuantity++;
});

document.getElementById("removePayment").addEventListener("click", () => {
    let container = document.getElementById("paymentFormGroup");
    container.removeChild(container.lastChild);
    paymentsQuantity--;
});

function addElement(element, className, name = "", placeholder = "", style = "", type = ""){
    //Create an element, set its type, class, name, placeholder and style (last three optional) attributes
    let el = document.createElement(element);
    el.className = className;
    el.id = name;
    el.name = name;
    el.placeholder = placeholder;
    el.style = style;
    el.type = type;
    return el;
}