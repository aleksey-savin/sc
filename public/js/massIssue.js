let items = document.getElementById("items");
let checkAll = document.getElementById("checkAll");
let checks = document.getElementById("order-table").querySelectorAll("tbody>tr>td>input");
let checked = [];

checks.forEach(el => {
    el.addEventListener("change", () => {
        if (el.checked) {
            checked[el.id.substring(5)] = document.querySelectorAll(
                `#order-table > tbody > tr:nth-child(${parseInt(el.id.substring(5)) + 1}) > 
                td.text-center.align-middle > div > div > a:nth-child(1)`
            )[0].href.split("/").pop();
            
            let newItems = "";
            checked.forEach(el => {
                newItems += el + "|";
            });
            items.value = newItems;
        } else {
            checked[el.id.substring(5)] = undefined;
            
            let newItems = "";
            checked.forEach((el) => {
                newItems += el + "|";
            });
            items.value = newItems;
        }
    });
});

checkAll.addEventListener("change", () => {
    if (checkAll.checked) {
        for (let i = 0; i < checks.length; i++) {
            checks[i].checked = true;
            checked[i] = document.querySelectorAll(
                `#order-table > tbody > tr:nth-child(${ i + 1 }) > 
                td.text-center.align-middle > div > div > a:nth-child(1)`
            )[0].href.split("/").pop();
        }

        let newItems = "";
        checked.forEach((el) => {
            newItems += el + "|";
        });
        items.value = newItems;
    } else {
        checks.forEach((el) => {
            el.checked = false;
        });

        checked = [];
        items.value = "";
    }
});