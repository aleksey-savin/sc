const express = require("express");

const errorController = require("../controllers/errorController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/403-forbidden", isAuth, errorController.get403Forbidden);


module.exports = router;