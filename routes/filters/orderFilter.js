const express = require("express");
const { body } = require("express-validator");

const orderFilterController = require("../../controllers/filterControllers/orderFilterController");
const orderController = require("../../controllers/orderController");

const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../../middleware/validators");

const router = express.Router();

router.get("/my-order-filters", isAuth, orderFilterController.getIndex);
router.get(
    "/my-order-filters/edit/:orderFilterId",
    isAuth,
    orderFilterController.getEdit
);
router.post(
    "/my-order-filters/edit",
    isAuth,
    [
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    orderFilterController.postEdit
);

router.post(
    "/my-order-filters/delete",
    isAuth,
    orderFilterController.postDelete
);

router.get("/orders/filtered", isAuth, orderController.getIndex);
router.post("/orders/filtered", orderFilterController.postFilteredContent);
router.get(
    "/orders/filtered/:filterId",
    orderFilterController.getSavedFilterContent
);
router.post(
    "/orders/delete-order-filter",
    isAuth,
    orderFilterController.postDeleteOrderFilter
);
router.post(
    "/orders/fast-search",
    isAuth,
    orderFilterController.postFastOrderSearch
);

router.get(
    "/filtered/autocomplete",
    isAuth,
    orderFilterController.getAutoCompleteSearch
);

module.exports = router;
