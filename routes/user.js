const express = require("express");
const { check, body } = require("express-validator");

const User = require('../models/user');
const userController = require("../controllers/userController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/users", isAuth, isAdmin, userController.getIndex);

router.get("/users/add", isAdmin, userController.getAdd);
router.post(
    "/users/add",
    isAdmin,
    [
        check("email")
            .isEmail()
            .withMessage("Пожалуйста, введите корректный email.")
            .custom((value, { req }) => {
                return User.findOne({
                    email: value,
                }).then((userDoc) => {
                    if (userDoc) {
                        return Promise.reject(
                            "Пользователь с указанным email уже зарегистрирован. Пожалуйста, укажите другой адрес."
                        );
                    }
                });
            }),
        body("password", "Минимальная длина пароля 6 символов").isLength({
            min: 6,
        }),
        body("confirmPassword").custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error("Пароли должны совпадать!");
            }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value != "") {
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            }
        }),
        body("lastName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Фамилия обязательна.");
            }
            return true;
        }),
        body("firstName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Имя обязательно.");
            }
            return true;
        }),
        body("role").custom((value, { req }) => {
            if (!value) {
                throw new Error("Роль не определена.");
            }
            return true;
        }),
        body("company").custom((value, { req }) => {
            if (!value) {
                throw new Error("Организация не определена.");
            }
            return true;
        }),
    ],
    userController.postAdd
);

router.get("/my-account", isAuth, userController.getMyAccount);
router.post(
    "/my-account",
    isAuth,
    [
        check("email")
            .isEmail()
            .withMessage("Пожалуйста, введите корректный email."),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value != ""){
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error(
                        "Пожалуйста, введите корректный номер телефона."
                    );
                }
            }
            return true;
        }),
    ],
    userController.postMyAccount
);

router.get("/users/edit/:userId", isAdmin, userController.getEdit);
router.post(
    "/users/edit",
    isAuth,
    isAdmin,
    [
        check("email")
            .isEmail()
            .withMessage("Пожалуйста, введите корректный email."),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value != ""){
                if (/^((\+7|8)+([0-9]){10})$/.test(value)) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            }
            return true;
        }),
        body("lastName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Фамилия обязательна.");
            }
            return true;
        }),
        body("firstName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Имя обязательно.");
            }
            return true;
        }),
        body("role").custom((value, { req }) => {
            if (!value) {
                throw new Error("Роль не определена.");
            }
            return true;
        }),
        body("company").custom((value, { req }) => {
            if (!value) {
                throw new Error("Организация не определена.");
            }
            return true;
        }),
    ],
    userController.postEdit
);

router.post('/users/delete', isAuth, userController.postDelete);


module.exports = router;
