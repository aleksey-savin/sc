const express = require("express");
const { check, body } = require("express-validator");

const Manufacturer = require("../models/manufacturer");
const DeviceType = require("../models/deviceType");

const requestController = require("../controllers/requestController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/requests", isAuth, requestController.getIndex);

router.get("/requests/add", isAuth, requestController.getAdd);
router.post(
    "/requests/add",
    isAuth,
    [
        body("manufacturer").custom((value, { req }) => {
            return Manufacturer.findOne({
                name: value,
            }).then((manufacturer) => {
                if (!manufacturer) {
                    return Promise.reject(
                        "Производитель не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("deviceType").custom((value, { req }) => {
            return DeviceType.findOne({
                name: value,
            }).then((deviceType) => {
                if (!deviceType) {
                    return Promise.reject(
                        "Тип устройства не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value != "")
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                }
            throw new Error("Введён некорректный номер телефона.");
        }),
        body("sparePartAvailability").custom((value, { req }) => {
            if (!value) {
                throw new Error('Пункт "откуда запчасти" не определён.');
            }
            return true;
        }),
        body("requestState").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус запроса не определён.");
            }
            return true;
        }),
    ],
    requestController.postAdd
);

router.get("/requests/edit/:requestId", isAuth, requestController.getEdit);
router.post(
    "/requests/edit",
    isAuth,
    [
        body("manufacturer").custom((value, { req }) => {
            return Manufacturer.findOne({
                name: value,
            }).then((manufacturer) => {
                if (!manufacturer) {
                    return Promise.reject(
                        "Производитель не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("deviceType").custom((value, { req }) => {
            return DeviceType.findOne({
                name: value,
            }).then((deviceType) => {
                if (!deviceType) {
                    return Promise.reject(
                        "Тип устройства не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value != "")
                if (/^((\+7|8)+([0-9]){10})$/.test(value)) {
                    return true;
                }
            throw new Error("Введён некорректный номер телефона.");
        }),
        body("sparePartAvailability").custom((value, { req }) => {
            if (!value) {
                throw new Error('Пункт "откуда запчасти" не определён.');
            }
            return true;
        }),
        body("requestState").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус запроса не определён.");
            }
            return true;
        }),
    ],
    requestController.postEdit
);

router.get("/requests/view/:requestId", isAuth, requestController.getView);

router.post("/requests/delete", isAuth, requestController.postDelete);

module.exports = router;
