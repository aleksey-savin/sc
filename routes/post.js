const express = require("express");

const postController = require("../controllers/postController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/", isAuth, postController.getIndex);

router.get("/news/add", isAuth, postController.getAdd);
router.post("/news/add", isAuth, postController.postAdd);

router.get("/news/edit/postId", isAuth, postController.getEdit);
router.post("/news/edit", isAuth, postController.postEdit);

router.post("/news/delete", isAuth, postController.postDelete);

module.exports = router;
