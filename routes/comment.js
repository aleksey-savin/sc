const express = require("express");

const commentController = require("../controllers/commentController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.post(
    "/comments/add",
    isAuth,    
    commentController.postAdd
);

router.post(
    "/comments/delete",
    isAuth,
    isAdmin,
    commentController.postDelete
);

module.exports = router;
