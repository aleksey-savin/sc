const express = require("express");
const { body } = require("express-validator");

const Customer = require("../models/customer");
const LegalEntity = require("../models/legalEntity");
const Manufacturer = require("../models/manufacturer");
const DeviceType = require("../models/deviceType");

const deviceController = require("../controllers/deviceController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/devices", isAuth, deviceController.getIndex);

router.get("/devices/replacement-fund", isAuth, isAdmin, isManager, deviceController.getReplacementFund);

router.get("/devices/add", isAuth, deviceController.getAddDevice);
router.post(
    "/devices/add",
    isAuth,
    [
        body("individualOwner").custom((value, { req }) => {
            if (!value && !req.body.legalEntityOwner) {
                throw new Error("Конечный пользователь не определён.");
            }
            return true;
        }),
        body("individualOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "individualOwner") {
                if (req.body.individualOwnerId) {
                    return Customer.findById(req.body.individualOwnerId).then(
                        (customer) => {
                            if (!customer) {
                                return Promise.reject(
                                    "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                                );
                            }
                        }
                    );
                } else {
                    return Customer.findOne({
                        name: {
                            first: value.split(" ")[1],
                            middle: value.split(" ")[2],
                            last: value.split(" ")[0],
                        },
                    }).then((customer) => {
                        if (!customer) {
                            return Promise.reject(
                                "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                }
            }
            return true;
        }),
        body("legalEntityOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "legalEntityOwner") {
                if (req.body.legalEntityOwnerId) {
                    return LegalEntity.findById(
                        req.body.legalEntityOwnerId
                    ).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                } else {
                    return LegalEntity.findOne({
                        fullName: value,
                    }).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                }
            }
            return true;
        }),
        body("manufacturer").custom((value, { req }) => {
            return Manufacturer.findOne({
                title: value,
            }).then((manufacturer) => {
                if (!manufacturer) {
                    return Promise.reject(
                        "Производитель не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("type").custom((value, { req }) => {
            return DeviceType.findOne({
                name: value,
            }).then((deviceType) => {
                if (!deviceType) {
                    return Promise.reject(
                        "Тип устройства не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("state").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус устройства не определён.");
            }
            return true;
        }),
    ],
    deviceController.postAddDevice
);

router.get("/devices/edit/:deviceId", isAuth, deviceController.getEditDevice);
router.post(
    "/devices/edit",
    isAuth,
    [
        body("individualOwner").custom((value, { req }) => {
            if (!value && !req.body.legalEntityOwner) {
                throw new Error("Конечный пользователь не определён.");
            }
            return true;
        }),
        body("individualOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "individualOwner") {
                if (req.body.individualOwnerId) {
                    return Customer.findById(req.body.individualOwnerId).then(
                        (customer) => {
                            if (!customer) {
                                return Promise.reject(
                                    "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                                );
                            }
                        }
                    );
                } else {
                    return Customer.findOne({
                        name: {
                            first: value.split(" ")[1],
                            middle: value.split(" ")[2],
                            last: value.split(" ")[0],
                        },
                    }).then((customer) => {
                        if (!customer) {
                            return Promise.reject(
                                "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                }
            }
            return true;
        }),
        body("legalEntityOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "legalEntityOwner") {
                if (req.body.legalEntityOwnerId) {
                    return LegalEntity.findById(
                        req.body.legalEntityOwnerId
                    ).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                } else {
                    return LegalEntity.findOne({
                        fullName: value,
                    }).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                }
            }
            return true;
        }),
        body("manufacturer").custom((value, { req }) => {
            return Manufacturer.findOne({
                title: value,
            }).then((manufacturer) => {
                if (!manufacturer) {
                    return Promise.reject(
                        "Производитель не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("type").custom((value, { req }) => {
            return DeviceType.findOne({
                name: value,
            }).then((deviceType) => {
                if (!deviceType) {
                    return Promise.reject(
                        "Тип устройства не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("state").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус устройства не определён.");
            }
            return true;
        }),
    ],
    deviceController.postEditDevice
);

router.get("/devices/view/:deviceId", isAuth, deviceController.getViewDevice);

router.post(
    "/devices/add-customer",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            return true;
        }),        
        body("lastName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Фамилия обязательна.");
            }
            return true;
        }),
        body("firstName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Имя обязательно.");
            }
            return true;
        }),
    ],
    deviceController.postAddCustomer
);
router.post(
    "/devices/add-legal-entity",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Пожалуйста, введите корректный email.");
                }
            return true;
        }),        
        body("shortName").custom((value, { req }) => {
            if (!value) {
                throw new Error(
                    "Краткое наименование обязательно для заполнения."
                );
            }
            return true;
        }),
        body("fullName").custom((value, { req }) => {
            if (!value) {
                throw new Error(
                    "Полное наименование обязательно для заполнения."
                );
            }
            return true;
        }),
    ],
    deviceController.postAddLegalEntity
);

router.get("/devices/print-contract-replacement-fund/:deviceId", isAuth, isAdmin, isManager, deviceController.getPrintContractReplacementFund);
router.post("/devices/print-contract-replacement-fund/", isAuth, isAdmin, isManager, deviceController.postPrintContractReplacementFund);

router.post("/devices/delete", isAdmin, isManager, deviceController.postDeleteDevice);

router.get(
    "/devices/autocomplete",
    isAuth,
    deviceController.getAutoCompleteDevice
);

module.exports = router;
