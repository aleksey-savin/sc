const express = require('express');

const dashboardController = require('../controllers/dashboardController');
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require('../middleware/validators');

const router = express.Router();

router.get(
    '/dashboard',
    isAuth,
    isManager,
    dashboardController.getIndex
);

module.exports = router;
