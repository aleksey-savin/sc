const express = require("express");
const { body } = require("express-validator");

const sparePartController = require("../controllers/sparePartController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/spareParts", isAuth, isAdmin, sparePartController.getIndex);

router.get(
    "/spareParts/add",
    isAuth,
    isAdmin,
    sparePartController.getAddSparePart
);
router.post(
    "/spareParts/add",
    isAuth,
    isAdmin,
    [
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Название не определено.");
            }
            return true;
        }),
        body("vendor").custom((value, { req }) => {
            if (!value) {
                throw new Error("Вендор не определён.");
            }
            return true;
        }),
    ],
    sparePartController.postAddSparePart
);

router.get(
    "/spareParts/edit/:sparePartId",
    isAuth,
    isAdmin,
    sparePartController.getEditSparePart
);
router.post(
    "/spareParts/edit",
    isAuth,
    isAdmin,
    [
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Название не определено.");
            }
            return true;
        }),
        body("vendor").custom((value, { req }) => {
            if (!value) {
                throw new Error("Вендор не определён.");
            }
            return true;
        }),
    ],
    sparePartController.postEditSparePart
);

router.get(
    "/spareParts/view/:sparePartId",
    isAuth,
    isAdmin,
    sparePartController.getViewSparePart
);

router.post(
    "/spareParts/delete",
    isAdmin,
    sparePartController.postDeleteSparePart
);

module.exports = router;
