const express = require("express");
const { body } = require("express-validator");

const requestStateController = require("../controllers/requestStateController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get(
    "/request-states",
    isAuth,
    isAdmin,
    requestStateController.getIndex
);
router.get(
    "/request-states/add",
    isAuth,
    isAdmin,
    requestStateController.getAdd
);

router.post(
    "/request-states/add",
    isAuth,
    isAdmin,
    [
        body("name").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование обязательно для заполнения.");
            }
            return true;
        }),
    ],
    requestStateController.postAdd
);

router.get(
    "/request-states/edit/:requestStateId",
    isAuth,
    isAdmin,
    requestStateController.getEdit
);
router.post(
    "/request-states/edit",
    isAuth,
    isAdmin,
    [
        body("name").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование обязательно для заполнения.");
            }
            return true;
        }),
        body("lowTimeLimit")
            .isInt()
            .withMessage("Уведомления, часы - только цифры."),
        body("mediumTimeLimit")
            .isInt()
            .withMessage("Уведомления, часы - только цифры."),
        body("highTimeLimit")
            .isInt()
            .withMessage("Уведомления, часы - только цифры."),
        body("priority")
            .isInt({ min: 1, max: 100 })
            .withMessage("Приоритет - только цифры от 1 до 100."),
    ],
    requestStateController.postEdit
);

router.post(
    "/request-states/delete",
    isAuth,
    isAdmin,
    requestStateController.postDelete
);

module.exports = router;
