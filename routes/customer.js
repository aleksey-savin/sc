const express = require("express");
const { body } = require("express-validator");

const customerController = require("../controllers/customerController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/customers", isAuth, customerController.getIndex);

router.get("/customers/add", isAuth, customerController.getAddCustomer);
router.post(
    "/customers/add",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (
                /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(value)
            ) {
                return true;
            }
            if (req.body.emptyPhone) {
                return true;
            }
            throw new Error("Введён некорректный номер телефона.");
        }),
        body("lastName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Фамилия обязательна.");
            }
            return true;
        }),
        body("firstName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Имя обязательно.");
            }
            return true;
        }),
    ],
    customerController.postAddCustomer
);

router.get(
    "/customers/edit/:customerId",
    isAuth,
    customerController.getEditCustomer
);
router.post(
    "/customers/edit",
    isAuth,
    [
        [
            body("email").custom((value, { req }) => {
                if (value != "")
                    if (
                        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                            value
                        )
                    ) {
                        return true;
                    } else {
                        throw new Error("Введён некорректный email.");
                    }
                return true;
            }),
            body("phone").custom((value, { req }) => {
                value = value.trim();
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                }
                if (req.body.emptyPhone) {
                    return true;
                }
                throw new Error("Введён некорректный номер телефона.");
            }),
            body("lastName").custom((value, { req }) => {
                if (!value) {
                    throw new Error("Фамилия обязательна.");
                }
                return true;
            }),
            body("firstName").custom((value, { req }) => {
                if (!value) {
                    throw new Error("Имя обязательно.");
                }
                return true;
            }),
        ],
    ],
    customerController.postEditCustomer
);

router.get(
    "/customers/view/:customerId",
    isAuth,
    customerController.getViewCustomer
);

router.post("/customers/delete", isAuth, customerController.postDeleteCustomer);

router.get(
    "/customers/autocomplete",
    isAuth,
    customerController.getAutoCompleteCustomer
);

module.exports = router;
