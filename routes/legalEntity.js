const express = require("express");
const { body } = require("express-validator");

const legalEntityController = require("../controllers/legalEntityController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/legal-entities", isAuth, legalEntityController.getIndex);

router.get("/legal-entities/add", isAuth, legalEntityController.getAddLegalEntity);
router.post(
    "/legal-entities/add",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Пожалуйста, введите корректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (!value) throw new Error("Введён некорректный номер телефона.");
            if (
                /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(value)
            ) {
                return true;
            }
            throw new Error("Введён некорректный номер телефона.");
        }),
        body("shortName").custom((value, { req }) => {
            if (!value) {
                throw new Error(
                    "Краткое наименование обязательно для заполнения."
                );
            }
            return true;
        }),
        body("fullName").custom((value, { req }) => {
            if (!value) {
                throw new Error(
                    "Полное наименование обязательно для заполнения."
                );
            }
            return true;
        }),
    ],
    legalEntityController.postAddLegalEntity
);

router.get(
    "/legal-entities/edit/:id",
    isAuth,
    legalEntityController.getEditLegalEntity
);
router.post(
    "/legal-entities/edit",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (value != "")
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Пожалуйста, введите корректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (!value) throw new Error("Введён некорректный номер телефона.");
            if (
                /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(value)
            ) {
                return true;
            }
            throw new Error("Введён некорректный номер телефона.");
        }),
        body("shortName").custom((value, { req }) => {
            if (!value) {
                throw new Error(
                    "Краткое наименование обязательно для заполнения."
                );
            }
            return true;
        }),
        body("fullName").custom((value, { req }) => {
            if (!value) {
                throw new Error(
                    "Полное наименование обязательно для заполнения."
                );
            }
            return true;
        }),
    ],
    legalEntityController.postEditLegalEntity
);

router.get(
    "/legal-entities/view/:id",
    isAuth,
    legalEntityController.getViewLegalEntity
);

router.post("/legal-entities/delete", isAuth, legalEntityController.postDeleteLegalEntity);

router.get(
    "/legal-entities/add-contact/:id",
    isAuth,
    legalEntityController.getAddContact
);
router.post(
    "/legal-entities/add-contact",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (!value && !req.body.phone) {
                throw new Error(
                    "Email и телефон одновременно не могут быть пустыми."
                );
            }
            return true;
        }),
        body("email").custom((value, { req }) => {
            if (value)
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value)
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value)
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            return true;
        }),
        body("lastName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Фамилия обязательна.");
            }
            return true;
        }),
        body("firstName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Имя обязательно.");
            }
            return true;
        }),
    ],
    legalEntityController.postAddContact
);

router.get(
    "/legal-entities/edit-contact/:id/:contactIndex",
    isAuth,
    legalEntityController.getEditContact
);
router.post(
    "/legal-entities/edit-contact",
    isAuth,
    [
        body("email").custom((value, { req }) => {
            if (!value && !req.body.phone) {
                throw new Error(
                    "Email и телефон одновременно не могут быть пустыми."
                );
            }
            return true;
        }),
        body("email").custom((value, { req }) => {
            if (value)
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value)
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value)
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            return true;
        }),
        body("lastName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Фамилия обязательна.");
            }
            return true;
        }),
        body("firstName").custom((value, { req }) => {
            if (!value) {
                throw new Error("Имя обязательно.");
            }
            return true;
        }),
    ],
    legalEntityController.postEditContact
);

router.post("/legal-entities/delete-contact", isAuth, legalEntityController.postDeleteContact);

router.get(
    "/legal-entities/autocomplete",
    isAuth,
    legalEntityController.getAutoCompleteLegalEntity
);

module.exports = router;
