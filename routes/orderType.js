const express = require('express');
const { body } = require("express-validator");

const orderTypeController = require('../controllers/orderTypeController');
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require('../middleware/validators');

const router = express.Router();

router.get('/order-types', isAuth, isAdmin, orderTypeController.getIndex);
router.get('/order-types/add', isAuth, isAdmin, orderTypeController.getAdd);

router.post(
    "/order-types/add",
    isAuth,
    isAdmin,
    [
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    orderTypeController.postAdd
);

router.get(
    '/order-types/edit/:orderTypeId',
    isAuth,
    isAdmin,
    orderTypeController.getEdit
);
router.post(
    "/order-types/edit",
    isAuth,
    isAdmin,
    [
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
        body("priority")
            .isInt({ min: 1, max: 100 })
            .withMessage("Приоритет - только цифры от 1 до 100."),
    ],
    orderTypeController.postEdit
);

router.post(
    '/order-types/delete',
    isAuth,
    isAdmin,
    orderTypeController.postDelete
);

module.exports = router;
