const express = require("express");
const { body } = require("express-validator");

const sectionCodeController = require("../../controllers/codeControllers/sectionCodeController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../../middleware/validators");

const router = express.Router();

router.get("/section-codes", isAuth, isManager, sectionCodeController.getIndex);
router.get("/section-codes/add", isAuth, isManager, sectionCodeController.getAdd);

router.post(
    "/section-codes/add",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    sectionCodeController.postAdd
);

router.get(
    "/section-codes/edit/:sectionCodeId",
    isAuth,
    isManager,
    sectionCodeController.getEdit
);
router.post(
    "/section-codes/edit",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    sectionCodeController.postEdit
);

router.post(
    "/section-codes/delete",
    isAuth,
    isAdmin,
    sectionCodeController.postDelete
);

router.get(
    "/section-codes/autocomplete",
    isAuth,    
    sectionCodeController.getAutoComplete
);

module.exports = router;
