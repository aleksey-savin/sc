const express = require("express");
const { body } = require("express-validator");

const repairCodeController = require("../../controllers/codeControllers/repairCodeController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../../middleware/validators");

const router = express.Router();

router.get("/repair-codes", isAuth, isManager, repairCodeController.getIndex);
router.get("/repair-codes/add", isAuth, isManager, repairCodeController.getAdd);

router.post(
    "/repair-codes/add",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    repairCodeController.postAdd
);

router.get(
    "/repair-codes/edit/:repairCodeId",
    isAuth,
    isManager,
    repairCodeController.getEdit
);
router.post(
    "/repair-codes/edit",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    repairCodeController.postEdit
);

router.post(
    "/repair-codes/delete",
    isAuth,
    isAdmin,
    repairCodeController.postDelete
);

router.get(
    "/repair-codes/autocomplete",
    isAuth,    
    repairCodeController.getAutoComplete
);

module.exports = router;
