const express = require("express");
const { body } = require("express-validator");

const conditionCodeController = require("../../controllers/codeControllers/conditionCodeController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../../middleware/validators");

const router = express.Router();

router.get("/condition-codes", isAuth, isManager, conditionCodeController.getIndex);
router.get("/condition-codes/add", isAuth, isManager, conditionCodeController.getAdd);

router.post(
    "/condition-codes/add",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    conditionCodeController.postAdd
);

router.get(
    "/condition-codes/edit/:conditionCodeId",
    isAuth,
    isManager,
    conditionCodeController.getEdit
);
router.post(
    "/condition-codes/edit",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    conditionCodeController.postEdit
);

router.post(
    "/condition-codes/delete",
    isAuth,
    isAdmin,
    conditionCodeController.postDelete
);

router.get(
    "/condition-codes/autocomplete",
    isAuth,    
    conditionCodeController.getAutoComplete
);

module.exports = router;
