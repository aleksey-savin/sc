const express = require("express");
const { body } = require("express-validator");

const symptomCodeController = require("../../controllers/codeControllers/symptomCodeController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../../middleware/validators");

const router = express.Router();

router.get("/symptom-codes", isAuth, isManager, symptomCodeController.getIndex);
router.get("/symptom-codes/add", isAuth, isManager, symptomCodeController.getAdd);

router.post(
    "/symptom-codes/add",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    symptomCodeController.postAdd
);

router.get(
    "/symptom-codes/edit/:symptomCodeId",
    isAuth,
    isManager,
    symptomCodeController.getEdit
);
router.post(
    "/symptom-codes/edit",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    symptomCodeController.postEdit
);

router.post(
    "/symptom-codes/delete",
    isAuth,
    isAdmin,
    symptomCodeController.postDelete
);

router.get(
    "/symptom-codes/autocomplete",
    isAuth,    
    symptomCodeController.getAutoComplete
);

module.exports = router;
