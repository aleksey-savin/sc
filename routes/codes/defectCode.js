const express = require("express");
const { body } = require("express-validator");

const defectCodeController = require("../../controllers/codeControllers/defectCodeController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../../middleware/validators");

const router = express.Router();

router.get("/defect-codes", isAuth, isManager, defectCodeController.getIndex);
router.get("/defect-codes/add", isAuth, isManager, defectCodeController.getAdd);

router.post(
    "/defect-codes/add",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    defectCodeController.postAdd
);

router.get(
    "/defect-codes/edit/:defectCodeId",
    isAuth,
    isManager,
    defectCodeController.getEdit
);
router.post(
    "/defect-codes/edit",
    isAuth,
    isManager,
    [
        body("code").custom((value, { req }) => {
            if (!value) {
                throw new Error("Код не определён.");
            }
            return true;
        }),
        body("title").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    defectCodeController.postEdit
);

router.post(
    "/defect-codes/delete",
    isAuth,
    isAdmin,
    defectCodeController.postDelete
);

router.get(
    "/defect-codes/autocomplete",
    isAuth,    
    defectCodeController.getAutoComplete
);

module.exports = router;
