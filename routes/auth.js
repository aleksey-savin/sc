const express = require('express');
const { check, body } = require('express-validator');

const authController = require('../controllers/authController');
const User = require('../models/user');
const Customer = require('../models/customer');

const router = express.Router();

router.get('/login', authController.getLogin);

router.get('/signup', authController.getSignup);

router.post(
    '/login',
    [
        check('email')
            .isEmail()
            .withMessage('Пожалуйста, введите корректный email.'),
    ],
    authController.postLogin
);

router.post(
    '/signup',
    [
        check('email')
            .isEmail()
            .withMessage('Пожалуйста, введите корректный email.')
            .custom((value, { req }) => {
                return User.findOne({
                    email: value,
                }).then((userDoc) => {
                    if (userDoc) {
                        return Promise.reject(
                            'Пользователь с указанным email уже зарегистрирован. Пожалуйста, укажите другой адрес.'
                        );
                    }
                });
            })
            // если это в системе есть хоть один пользователь и email нет среди customers, то регистрация запрещена
            /* .custom((value, { req }) => {
                const users = User.find().count();
                return Customer.findOne({
                    'contacts.email': value,
                }).then((userDoc) => {
                    if (!userDoc || users) {
                        return Promise.reject(
                            'К сожалению, данный email не зарегистрирован в Клиентской базе.'
                        );
                    }
                });
            }) */,
        body('password', 'Минимальная длина пароля 6 символов').isLength({
            min: 6,
        }),
        body('confirmPassword').custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Пароли должны совпадать!');
            }
            return true;
        }),
    ],
    authController.postSignup
);

router.post('/logout', authController.postLogout);

router.get('/reset', authController.getReset);

router.post(
    '/reset',
    [
        check('email')
            .isEmail()
            .withMessage('Пожалуйста, введите корректный email.'),
    ],
    authController.postReset
);

router.get('/reset/:token', authController.getNewPassword);
router.get('/activate/:token', authController.getActivateUser);

router.post(
    '/new-password',
    [
        body('password', 'Минимальная длина пароля 6 символов').isLength({
            min: 6,
        }),
        body('confirmPassword').custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Пароли должны совпадать!');
            }
            return true;
        }),
    ],
    authController.postNewPassword
);

module.exports = router;
