const express = require("express");

const manufacturerController = require("../controllers/manufacturerController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get(
    "/manufacturers",
    isAuth,
    isAdmin,
    isManager,
    manufacturerController.getIndex
);

router.get(
    "/manufacturers/add",
    isAuth,
    isAdmin,
    isManager,
    manufacturerController.getAdd
);
router.post(
    "/manufacturers/add",
    isAuth,
    isAdmin,
    isManager,
    manufacturerController.postAdd
);

router.get(
    "/manufacturers/edit/:manufacturerId",
    isAuth,
    isAdmin,
    isManager,
    manufacturerController.getEdit
);
router.post("/manufacturers/edit", isAuth,  isAdmin, isManager, manufacturerController.postEdit);

router.post("/manufacturers/delete", isAuth, isAdmin, manufacturerController.postDelete);

router.get(
    "/manufacturers/autocomplete",
    isAuth,
    manufacturerController.getAutoComplete
);

module.exports = router;
