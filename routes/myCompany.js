const express = require("express");
const { body } = require("express-validator");

const myCompanyController = require("../controllers/myCompanyController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/my-companies", isAuth, isAdmin, myCompanyController.getIndex);

router.get("/my-companies/add", isAuth, isAdmin, myCompanyController.getAdd);
router.post(
    "/my-companies/add",
    isAuth,
    isAdmin,
    [
        body("fullTitle").custom((value, { req }) => {
            if (!value) {
                throw new Error("Полное наименование обязательно.");
            }
            return true;
        }),
        body("shortTitle").custom((value, { req }) => {
            if (!value) {
                throw new Error("Сокращённое наименование обязательно.");
            }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value !== "") {
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            } else {
                return true;
            }
        }),
        body("email").custom((value, { req }) => {
            if (value !== "") {
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            } else {
                return true;
            }
        }),
    ],
    myCompanyController.postAdd
);

router.get(
    "/my-companies/edit/:companyId",
    isAuth,
    isAdmin,
    myCompanyController.getEdit
);
router.post(
    "/my-companies/edit",
    isAuth,
    isAdmin,
    [
        body("fullTitle").custom((value, { req }) => {
            if (!value) {
                throw new Error("Полное наименование обязательно.");
            }
            return true;
        }),
        body("shortTitle").custom((value, { req }) => {
            if (!value) {
                throw new Error("Сокращённое наименование обязательно.");
            }
            return true;
        }),
        body("phone").custom((value, { req }) => {
            value = value.trim();
            if (value !== "") {
                if (
                    /^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный номер телефона.");
                }
            } else {
                return true;
            }
        }),
        body("email").custom((value, { req }) => {
            if (value !== "") {
                if (
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                        value
                    )
                ) {
                    return true;
                } else {
                    throw new Error("Введён некорректный email.");
                }
            } else {
                return true;
            }
        }),
    ],
    myCompanyController.postEdit
);

router.get("/my-company", isAuth, isAdmin, myCompanyController.getEditMyCompany);

router.post("/my-companies/delete", isAuth, isAdmin, myCompanyController.postDelete);

module.exports = router;
