const express = require("express");
const { body } = require("express-validator");

const deviceTypeController = require("../controllers/deviceTypeController");
const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/device-types", isAuth, isAdmin, isManager, deviceTypeController.getIndex);
router.get("/device-types/add", isAuth, isAdmin, isManager, deviceTypeController.getAdd);

router.post(
    "/device-types/add",
    isAuth,
    isAdmin,
    isManager,
    [
        body("name").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    deviceTypeController.postAdd
);

router.get(
    "/device-types/edit/:deviceTypeId",
    isAuth,
    isAdmin,
    isManager,
    deviceTypeController.getEdit
);
router.post(
    "/device-types/edit",
    isAuth,
    isAdmin,
    isManager,
    [
        body("name").custom((value, { req }) => {
            if (!value) {
                throw new Error("Наименование не определено.");
            }
            return true;
        }),
    ],
    deviceTypeController.postEdit
);

router.get(
    "/device-types/view/:deviceTypeId",
    isAuth,
    isAdmin,
    isManager,
    deviceTypeController.getView
);

router.post(
    "/device-types/delete",
    isAuth,
    isAdmin,
    deviceTypeController.postDelete
);

module.exports = router;
