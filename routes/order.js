const express = require("express");
const { body } = require("express-validator");

const Customer = require("../models/customer");
const LegalEntity = require("../models/legalEntity");
const Device = require("../models/device");
const DeviceType = require("../models/deviceType");

orderController = require("../controllers/orderController");
exportController = require("../controllers/exportController");
deviceController = require("../controllers/deviceController");

const {
    isAuth,
    isAdmin,
    isManager,
    isEngineer,
    isUser,
} = require("../middleware/validators");

const router = express.Router();

router.get("/orders", isAuth, orderController.getIndex);

router.post("/orders/mass-issue", isAuth, orderController.postMassIssue);

router.get("/orders/add", isAuth, orderController.getAddOrder);
router.post(
    "/orders/add",
    isAuth,
    [
        body("individualOwner").custom((value, { req }) => {
            if (!value && !req.body.legalEntityOwner) {
                throw new Error("Сдавший устройство не определён.");
            }
            return true;
        }),
        body("individualOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "individualOwner") {
                if (req.body.individualOwnerId) {
                    return Customer.findById(req.body.individualOwnerId).then(
                        (customer) => {
                            if (!customer) {
                                return Promise.reject(
                                    "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                                );
                            }
                        }
                    );
                } else {
                    throw new Error(
                        "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                    );
                }
            }
            return true;
        }),
        body("legalEntityOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "legalEntityOwner") {
                if (req.body.legalEntityOwnerId) {
                    return LegalEntity.findById(
                        req.body.legalEntityOwnerId
                    ).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                } else {
                    throw new Error(
                        "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                    );
                }
            }
            return true;
        }),
        body("deviceId").custom((value, { req }) => {
            if (value) {
                return Device.findOne({
                    _id: value,
                }).then((device) => {
                    if (!device) {
                        return Promise.reject(
                            "Устройство не найдено. Пожалуйста, выберите из списка или добавьте новое."
                        );
                    } else {
                        return true;
                    }
                });
            } else {
                throw new Error(
                    "Устройство не найдено. Пожалуйста, выберите из списка или добавьте новое."
                );
            }
        }),
        body("state").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус ремонта не определён.");
            }
            return true;
        }),
        body("orderType").custom((value, { req }) => {
            if (!value) {
                throw new Error("Тип ремонта не определён.");
            }
            return true;
        }),
    ],
    orderController.postAddOrder
);

router.get("/orders/edit/:orderId", isAuth, orderController.getEditOrder);

router.get(
    "/orders/print/work-order/:orderId",
    isAuth,
    orderController.getPrintWorkOrder
);
router.get(
    "/orders/print/act-of-acceptance/:orderId",
    isAuth,
    orderController.getPrintActOfAcceptance
);

router.get(
    "/orders/print/nrp-act/:orderId",
    isAuth,
    orderController.getPrintNRPAct
);

router.post(
    "/orders/edit",
    isAuth,
    [
        body("individualOwner").custom((value, { req }) => {
            if (!value && !req.body.legalEntityOwner) {
                throw new Error("Сдавший устройство не определён.");
            }
            return true;
        }),
        body("individualOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "individualOwner") {
                if (req.body.individualOwnerId) {
                    return Customer.findById(req.body.individualOwnerId).then(
                        (customer) => {
                            if (!customer) {
                                return Promise.reject(
                                    "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                                );
                            }
                        }
                    );
                } else {
                    throw new Error(
                        "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                    );
                }
            }
            return true;
        }),
        body("legalEntityOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "legalEntityOwner") {
                if (req.body.legalEntityOwnerId) {
                    return LegalEntity.findById(
                        req.body.legalEntityOwnerId
                    ).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                } else {
                    throw new Error(
                        "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                    );
                }
            }
            return true;
        }),
        body("deviceId").custom((value, { req }) => {
            if (value) {
                return Device.findOne({
                    _id: value,
                }).then((device) => {
                    if (!device) {
                        return Promise.reject(
                            "Устройство не найдено. Пожалуйста, выберите из списка или добавьте новое."
                        );
                    } else {
                        return true;
                    }
                });
            } else {
                throw new Error(
                    "Устройство не найдено. Пожалуйста, выберите из списка или добавьте новое."
                );
            }
        }),
        body("state").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус ремонта не определён.");
            }
            return true;
        }),
        body("orderType").custom((value, { req }) => {
            if (!value) {
                throw new Error("Тип ремонта не определён.");
            }
            return true;
        }),
    ],
    orderController.postEditOrder
);

router.post(
    "/orders/new-device-identity",
    isAuth,
    orderController.postNewDeviceIdentity
);

router.post("/orders/add-device", isAuth, [
        body("individualOwner").custom((value, { req }) => {
            if (!value && !req.body.legalEntityOwner) {
                throw new Error("Конечный пользователь не определён.");
            }
            return true;
        }),
        body("individualOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "individualOwner") {
                if (req.body.individualOwnerId) {
                    return Customer.findById(req.body.individualOwnerId).then(
                        (customer) => {
                            if (!customer) {
                                return Promise.reject(
                                    "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                                );
                            }
                        }
                    );
                } else {
                    return Customer.findOne({
                        name: {
                            first: value.split(" ")[1],
                            middle: value.split(" ")[2],
                            last: value.split(" ")[0],
                        },
                    }).then((customer) => {
                        if (!customer) {
                            return Promise.reject(
                                "Физическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                }
            }
            return true;
        }),
        body("legalEntityOwner").custom((value, { req }) => {
            if (req.body.ownerRadios === "legalEntityOwner") {
                if (req.body.legalEntityOwnerId) {
                    return LegalEntity.findById(
                        req.body.legalEntityOwnerId
                    ).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                } else {
                    return LegalEntity.findOne({
                        fullName: value,
                    }).then((legalEntity) => {
                        if (!legalEntity) {
                            return Promise.reject(
                                "Юридическое лицо не найдено. Пожалуйста, выберите из списка или добавьте новое."
                            );
                        }
                    });
                }
            }
            return true;
        }),
        body("manufacturer").custom((value, { req }) => {
            return Manufacturer.findOne({
                name: value,
            }).then((manufacturer) => {
                if (!manufacturer) {
                    return Promise.reject(
                        "Производитель не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("type").custom((value, { req }) => {
            return DeviceType.findOne({
                name: value,
            }).then((deviceType) => {
                if (!deviceType) {
                    return Promise.reject(
                        "Тип устройства не найден. Пожалуйста, выберите из списка или добавьте новый."
                    );
                }
            });
        }),
        body("state").custom((value, { req }) => {
            if (!value) {
                throw new Error("Статус устройства не определён.");
            }
            return true;
        }),
    ], deviceController.postAddDevice);

router.get("/orders/view/:orderId", isAuth, orderController.getViewOrder);

router.get("/orders/uploads/:fileTitle", isAuth, orderController.getViewAttachement);

router.post("/orders/add-file", isAuth, orderController.postAddFileToOrder);

router.post("/orders/delete", isAuth, isAdmin, orderController.postDeleteOrder);

router.post("/orders/export-lenovo", isAuth, isAdmin, isManager, exportController.postExportLenovoOrders);

router.post("/orders/export-philips-aoc", isAuth, isManager, exportController.postExportPhilipsAOCOrders);

router.post("/orders/send-email", isAuth, orderController.postSendEmail);
router.post("/orders/send-sms", isAuth, orderController.postSendSMS);

router.post("/orders/issue-note-submit", isAuth, orderController.postIssueNoteSubmit);

module.exports = router;
