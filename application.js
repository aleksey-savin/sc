const path = require('path');

require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');

const User = require('./models/user');
const OrderFilter = require('./models/orderFilter');

const MONGODB_URI = `mongodb://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@mongodb:27017/${process.env.MONGODB_DATABASE}?authSource=admin`;

const app = express();
const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'sessions',
});
const csrfProtection = csrf();

app.set('view engine', 'ejs');
app.set('views', 'views');

const authRoutes = require('./routes/auth');
const postRoutes = require('./routes/post');
const orderFilterRoutes = require('./routes/filters/orderFilter');
const myCompanyRoutes = require('./routes/myCompany');
const errorRoutes = require('./routes/error');
const deviceRoutes = require('./routes/device');
const orderTypeRoutes = require('./routes/orderType');
const orderRoutes = require('./routes/order');
const requestRoutes = require('./routes/request');
const customerRoutes = require('./routes/customer');
const legalEntityRoutes = require('./routes/legalEntity');
const manufacturerRoutes = require('./routes/manufacturer');
const deviceTypeRoutes = require('./routes/deviceType');
const requestStateRoutes = require('./routes/requestState');
const userRoutes = require('./routes/user');
const dashboardRoutes = require('./routes/dashboard');
const conditionCodeRoutes = require('./routes/codes/conditionCode');
const defectCodeRoutes = require('./routes/codes/defectCode');
const repairCodeRoutes = require('./routes/codes/repairCode');
const sectionCodeRoutes = require('./routes/codes/sectionCode');
const symptomCodeRoutes = require('./routes/codes/symptomCode');
const commentRoutes = require('./routes/comment');
const sparePartRoutes = require('./routes/sparePart');

const dateOptions = {
    day: 'numeric',
    month: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
};

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/orderFiles');
    },
    filename: (req, file, cb) => {
        cb(
            null,
            new Date()
                .toLocaleDateString('ru-RU', dateOptions)
                .replace(/\s/g, '') +
                '-' +
                file.originalname
        );
    },
});

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'application/pdf' ||
        file.mimetype ===
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
        file.mimetype === 'application/msword' ||
        file.mimetype === 'text/plain' ||
        file.mimetype === 'application/vnd.ms-excel' ||
        file.mimetype ===
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

app.use(bodyParser.urlencoded({ extended: false }));
app.use(
    multer({ storage: fileStorage, fileFilter: fileFilter }).array('addFile')
);
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    session({
        secret: 'sc-secret',
        resave: false,
        saveUninitialized: false,
        store: store,
        useUnifiedTopology: true,
    })
);
app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
    if (!req.session.user) {
        return next();
    }
    User.findById(req.session.user._id)
        .then((user) => {
            req.user = user;

            next();
        })
        .catch((err) => console.log(err));
});

app.use(async (req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.userData = req.session.user; //Must be combined with userCompany
    res.locals.myOrderFilters = req.session.user
        ? await OrderFilter.find({
              $or: [{ isShared: true }, { createdBy: req.session.user._id }],
          })
        : null;
    try {
        res.locals.userCompany = req.user.company; //Can be rewrote
    } catch (err) {}
    res.locals.csrfToken = req.csrfToken();
    next();
});

app.use(authRoutes);
app.use(postRoutes);
app.use(orderFilterRoutes);
app.use(myCompanyRoutes);
app.use(errorRoutes);
app.use(deviceRoutes);
app.use(orderTypeRoutes);
app.use(orderRoutes);
app.use(requestRoutes);
app.use(customerRoutes);
app.use(legalEntityRoutes);
app.use(manufacturerRoutes);
app.use(deviceTypeRoutes);
app.use(requestStateRoutes);
app.use(userRoutes);
app.use(dashboardRoutes);
app.use(conditionCodeRoutes);
app.use(defectCodeRoutes);
app.use(repairCodeRoutes);
app.use(sectionCodeRoutes);
app.use(symptomCodeRoutes);
app.use(commentRoutes);
app.use(sparePartRoutes);

mongoose
    .connect(MONGODB_URI)
    .then((result) => {
        app.listen(8090);
    })
    .catch((err) => {
        console.log(err);
    });
