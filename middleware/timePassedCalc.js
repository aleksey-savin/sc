exports.timePassedVerbose = (from, to) => {
    // today date and time in milliseconds
    const today = to;

    // parsing post date and time into milliseconds format
    const timeData = Date.parse(from);

    const seconds = (today - timeData) / 1000;
    const minutes = Math.floor((seconds / 60) % 60);
    const hours = Math.floor((seconds / 3600) % 24);
    const days = Math.floor(seconds / (3600 * 24));

    const daysWord =
        days === 1
            ? "день"
            : [
                  2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54, 62,
                  63, 64,
              ].includes(days)
            ? "дня"
            : "дней";    

    return `${days}дн ${hours}ч ${minutes}мин`;
};

exports.timePassedArray = (from, to) => {
    // today date and time in milliseconds
    const today = to;

    // parsing post date and time into milliseconds format
    const timeData = Date.parse(from);

    const seconds = Math.floor((today - timeData) / 1000);
    const minutes = Math.floor((seconds / 60) % 60);
    const hours = Math.floor((seconds / 3600) % 24);
    const days = Math.floor(seconds / (3600 * 24));
    
    return [days, hours, minutes];
};