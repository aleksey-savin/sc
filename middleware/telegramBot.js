const Telegraf = require("telegraf");
const MyCompany = require("../models/myCompany");

const notificator = {
    send: async (msg, user) => {
        const myCompany = await MyCompany.findById(user.company._id);
        const bot = new Telegraf(myCompany.apiKeys.telegram);
        bot.command("id", (ctx) => ctx.reply(ctx.chat.id));
        bot.launch();
        await bot.telegram.sendMessage(myCompany.contacts.telegramId, msg, {
            parse_mode: "HTML",
        });
    },
};

module.exports = notificator;
