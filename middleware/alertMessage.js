exports.alertMessage = (message) => {
    return message.length > 0 ? message[0] : null;
};
