exports.isAuth = (req, res, next) => {
    if (!req.session.isLoggedIn) {
        return res.redirect("/login");
    }
    next();
};

exports.isAdmin = (req, res, next) => {
    req.session.user.role !== "Admin" ? res.redirect("/403-forbidden") : next();
};

exports.isManager = (req, res, next) => {
    req.session.user.role === "Engineer" || req.session.user.role === "User"
        ? res.redirect("/403-forbidden")
        : next();
};

exports.isEngineer = (req, res, next) => {
    req.session.user.role !== "Admin" || req.session.user.role !== "Engineer"
        ? res.redirect("/403-forbidden")
        : next();
};

exports.isUser = (req, res, next) => {
    req.session.user.role !== "User" ? res.redirect("/403-forbidden") : next();
};
